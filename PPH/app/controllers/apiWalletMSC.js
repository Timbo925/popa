var _ = require('lodash'),
    log = require('log4js').getLogger("API Wallet MSC"),
    types = require('../../module/constants'),
    m = require('./middleware'),
    parser = require('../../module/messageParser');

module.exports = {
    getBalance: getBalance,
    makeTransaction: makeTransaction,
    getHdPrivkey: getHdPrivkey,
    receiveHdPrivkey: receiveHdPrivkey,
    getCurrentAddress: getCurrentAddress,
    creditRequest: creditRequest,
    getNotifications: getNotifications,
    getCredit: getCredit,
    getTransactions: getTransactions
};

function getBalance() {
    return function (req, res, next) {
        if(m.toSkip(req, types.accountTypes.WalletMSC)) return next();
        req.account.sendMessageToAllEncrypted({msgType: types.messageTypes.MSW_GETBALANCE}, {query: req.query}, function (err, data) {
            if (err)  return m.returnErrorJson(err)(req, res, next);
            req.retMsg = data;
            req.done = true;
            next()
        })
    }
}

function getNotifications() {
    return function (req, res, next) {
        if(m.toSkip(req, types.accountTypes.WalletMSC)) return next();
        req.account.sendMessageToAllEncrypted(
            {msgType: types.messageTypes.ACCOUNT_GETNOT}, {}, function (err, data) {
            if (err)  return m.returnErrorJson(err)(req, res, next);
            req.retMsg = data;
            req.done = true;
            next()
        })
    }
}

function makeTransaction() {
    return function(req, res, next){
        if(m.toSkip(req, types.accountTypes.WalletMSC)) return next();

        //Send request to the MSW and sign this input
        req.account.sendMessageToAllEncrypted(req.body, {}, function (err, tData) {
            if (err) return m.returnError(err)(req, res, next);
            var transaction = req.account.signTransaction(tData);
            transaction.value_CC = req.body.msgData.value_CC;
            transaction.CC = req.body.msgData.CC;

            //Send the signed transaction to MSW
            if (req.query.submit) {
                req.account.sendMessageToAllEncrypted({
                    msgType: types.messageTypes.MSW_SUBMITTRANSACTION,
                    msgData: transaction
                }, {}, function (err, response) {
                    if (err) return m.returnError(err)(req, res, next);
                    req.retMsg = response;
                    req.done = true;
                    next();
                })
            } else {
                req.retMsg = transaction;
                req.done = true;
                next();
            }
        })
    }
}

function receiveHdPrivkey () {
    return function (req, res, next) {
        if(m.toSkip(req, types.accountTypes.WalletMSC)) return next();
        parser[types.messageTypes.ACCOUNT_REG_CLIENT_MSC_HDPRIV](req.am, req.pm, req.body, req.account, function (err) {
            if (err) return m.returnErrorJson(err)(req, res);
            req.done = true;
            next();
        });
    }
}

function getHdPrivkey () {
    return function (req, res, next) {
        if(m.toSkip(req, types.accountTypes.WalletMSC)) return next();
        req.retMsg = req.account.getMessage(types.messageTypes.ACCOUNT_REG_CLIENT_MSC_HDPRIVGET);
        req.done = true;
        next();
    }
}

function getCurrentAddress () {
    return function (req, res, next) {
        if(m.toSkip(req, types.accountTypes.WalletMSC)) return next();
        req.account.sendMessageToAllEncrypted({
            msgType: types.messageTypes.MSW_GETCURRENTADDRESS
        }, {query: req.query}, function (err, retMsg) {
            if (err)  return m.returnErrorJson(err)(req, res, next);
            req.retMsg = retMsg;
            req.done = true;
            next()
        });
    }
}

function creditRequest() {
    return function (req, res, next) {
        if(m.toSkip(req, types.accountTypes.WalletMSC)) return next();
        req.account.sendMessageToAllEncrypted({
            msgType: types.messageTypes.MSW_CREDITREQUEST,
            msgData: {
                value: req.body.msgData.value,
                value_CC: req.body.msgData.value_CC,
                CC: req.body.msgData.CC
            }
        }, {}, function (err, msg) {
            if (err) return m.returnError(err)(req, res, next);
            req.retMsg = msg;
            req.done = true;
            next();
        })
    }
}

function getCredit() {
    return function (req, res, next) {
        if(m.toSkip(req, types.accountTypes.WalletMSC)) return next();
        req.account.sendMessageToAllEncrypted({msgType: types.messageTypes.ACCOUNT_GETCREDIT}, {}, function (err, msg) {
            if (err) return m.returnError(err)(req, res, next);
            req.retMsg = msg;
            req.done = true;
            next();
        })
    }
}

function getTransactions() {
    return function (req, res, next) {
        if(m.toSkip(req, types.accountTypes.WalletMSC)) return next();
        req.account.sendMessageToAllEncrypted({msgType: types.messageTypes.MSW_GETTRANSACTIONS}, {}, function (err, msg) {
            if (err) return m.returnError(err)(req, res, next);
            req.retMsg = msg;
            req.done = true;
            next();
        })
    }
}