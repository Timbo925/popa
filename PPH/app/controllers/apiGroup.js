var _ = require('lodash'),
    log = require('log4js').getLogger("API Group"),
    types = require('../../module/constants'),
    m = require('./middleware');

module.exports = {
    getGroups: function (req, res) {
        var groups = req.user.getPolicyManager().getGroupsArray();
        res.status(200).json(groups);
    },

    newGroup: [
        m.checkBody({name: {notEmpty: true, isAlphanumeric: true}}),
        m.attachPolicyManager(),
        function (req, res, next) {
            var group = req.pm.addGroup(req.body.name);
            next()
        },
        m.savePolicyManager(),
        m.returnOk()
    ],

    updateGroupPolicy: [
        m.checkBody({
            'name': {notEmpty: true, isAlphanumeric: true},
            'type': {notEmpty: true, isAlphanumeric: true},
            'lowerBound': {optional: true, isNumeric: true},
            'upperBound': {optional: true, isNumeric: true},
            'threshold': {optional: true, isNumeric: true},
            'groups': {optional: true, isGroups: true}
        }),
        m.attachPolicyManager(),
        function (req, res, next) {
            var vars = {};
            _.forEach(req.body, function (value, key) {
                if (_.indexOf(types.policyActionsVars[req.body.type], key) >= 0) vars[key] = value
            });

            var group = req.pm.getGroup(req.body.name);
            group.addOrChangePolicyAction(req.body.type, vars);
            log.debug("New Edited Group:", JSON.stringify(group));
            next()
        },
        m.savePolicyManager(),
        m.returnOk()
    ],

    updateGroup: [
        m.checkBody({
            'name': {notEmpty: true, isString: {errorMessage: 'Not a string'}},
            'displayName': {optional: true, isString: {errorMessage: 'Not a string'}},
            'members': {optional: true, isMember: true},
            'policyList': {optional:true}
        }),
        m.attachPolicyManager(),
        function (req, res, next) {
            log.debug("Update Group: body =", req.body);
            var group = req.pm.getGroup(req.body.name);
            if (req.body.displayName) group.displayName = req.body.displayName;
            if (req.body.members) {
                var members = (typeof req.body.members == 'string') ? [req.body.members] : req.body.members;
                log.debug("Members of group to update:", members);
                log.debug(req.pm.getMemberList());
                req.pm.updateMembers(req.body.name, members);
            }
            if (req.body.policyList) {
                try {
                    JSON.parse(req.body.policyList);
                    group.policyList = req.body.policyList;
                } catch (e) {
                    m.returnError('Not Good Format')(req,res,next);
                }
            }
            next()
        },
        m.savePolicyManager(),
        m.returnOk()
    ],

    addPolicyToGroup: [
        m.checkBody({
            'name': {notEmpty: true, isAlphanumeric: true},
            'type': {notEmpty: true, isPolicyAction: true}
        }),
        m.attachPolicyManager(),
        function (req, res, next) {
            var group = req.pm.getGroup(req.body.name);
            group.addOrChangePolicyAction(req.body.type, {}, true);
            next();
        },
        m.savePolicyManager(),
        m.returnOk()
    ],

    removePolicyFromGroup: [
        m.checkBody({
            'name': {notEmpty: true, isAlphanumeric: true},
            'type': {notEmpty: true, isPolicyAction: true}
        }),
        m.attachPolicyManager(),
        function (req, res, next) {
            var group = req.pm.getGroup(req.body.name);
            group.removePolicyAction(req.body.type);
            next()
        },
        m.savePolicyManager(),
        m.returnOk()
    ],

    removeGroup: [
        m.checkBody({'name': {notEmpty: true, isAlphanumeric: true}}),
        m.attachPolicyManager(),
        function (req, res, next) {

            log.debug("Removing group:", req.body.name);
            req.pm.removeGroup(req.body.name);
            next()
        },
        m.savePolicyManager(),
        m.returnOk()
    ]
};