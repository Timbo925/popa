var uid = require('uid-safe'),
    t = require('../../module/constants'),
    _ = require('lodash');

module.exports = {
    allAccounts: function (req, res) {
        var accounts = req.user.getWalletManager().getAccountListFlat();
        if (accounts.length == 1 && req.query.show != 't') {
            res.redirect('/account/' + accounts[0].id)
        } else {
            
            var groups = req.user.getPolicyManager().getGroupsArray();
            res.render('accounts', {
                uid: uid,
                types: t.accountTypes,
                accounts: accounts,
                groups: groups,
                _:require('lodash')
            })
        }
    },

    getAccount: function (req, res) {
        var am = req.user.getWalletManager();
        var account = am.getWalletById(req.params.accountId);
        var groups = req.user.getPolicyManager().getGroupsArray();
        res.render('account',
            {
                uid: uid,
                types: t.accountTypes,
                account: account,
                clients: account.getMessenger().getClientList(),
                query : req.query,
                groups: groups,
                notifications: account.getMessenger().getNotifications(),
                _:_
            })
    }
};
