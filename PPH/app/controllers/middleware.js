var Account = require('../models/account'),
    async = require('async'),
    _ = require('lodash'),
    log = require('log4js').getLogger('middleware');

function attachManagers() {
    return function (req, res, next) {
        attachWalletManager()(req, res, function(err) {
            if (err) return returnErrorJson(err);
            attachPolicyManager()(req, res, function (err) {
                if (err) return returnErrorJson(err);
                next();
            })
        })
    }
}

function attachWalletManager() {
    return function (req, res, next) {
        req.am = req.user.getWalletManager();
        next();
    }
}

function attachPolicyManager() {
    return function (req, res, next) {
        req.pm = req.user.getPolicyManager();
        next();
    }
}

function saveManagers(message) {
    return function (req, res, next) {
        saveWalletManager()(req, res, function(err) {
            if (err) return returnErrorJson(err)(req,res);
            savePolicyManager()(req, res, function (err) {
                if (err) return returnErrorJson(err)(req,res);
                if (message) return returnOk(message)(req, res);
                next();
            })
        })
    }
}

function savePolicyManager(policyManager) {
    return function (req, res, next) {
        var policyManager = policyManager || req.pm;
        if (!policyManager) res.status(500).json({err: "No accountmanager to be saved"});
        req.user.savePolicyManager(policyManager, function (err) {
            if (err) return res.status(500).json({error: 'Problem saving account manager'});
            next();
        })
    }
}

function saveWalletManager(accountManager) {
    return function (req, res, next) {
        var accountManager = accountManager || req.am;
        if (!accountManager) res.status(500).json({err: "No accountmanager to be saved"});
        req.user.saveWalletManager(accountManager, function (err) {
            if (err) return res.status(500).json({error: 'Problem saving account manager'});
            next();
        })
    }
}

function checkBody(schema) {
    return function (req, res, next) {
        req.checkBody(schema);
        //log.debug(req.body, req.params);
        var err = req.validationErrors();
        if (err) {
            log.error(err);
            returnErrorJson(err)(req, res)
        } else {
            next();
        }
    }
}

function attachIdentity(req, account, message) {
    var msg = _.clone(message);
    delete msg.commData;
    delete msg.commType;
    msg.commType = "SERVER";
    msg.commData = {
        url: 'http://' + req.get('host') + '/api/message/' + req.user._id + '/' + account.id,
        senderPubkey: account.getAuthPub()
    };
    return msg;
}

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        log.trace("Is Authenticated");
        return next();
    }
    passport.authenticate('basic', {session: false})(req, res, next)
}

function findAccount() {
    return function (req, res, next) {
        Account.findOne({_id: req.params.userId}, function (err, user) {
            if (err || !user) res.status(500).json({status: 'Account not found with id'})
            req.user = user;
            next()
        })
    }
}

function executeMiddlewareList(middlewareList, req, res, next) {
    async.eachSeries(middlewareList, function(middleware,next) {
       // log.debug('execute middleware', middleware);
        middleware.bind(null,req,res,next)()
    }, function(err) {
        if (err) return res.status(500).json({error: err});
        //log.debug('async finished')
        next();
    })
}

function returnErrorJson(message) {
    return function(req, res, next) {
        var ret  = message || req.returnMessage || req.retMsg ||'';
        res.status(500).json({status: 'error' , msg: ret})
    }
}

function returnFailed(message) {
    return function(req, res, next) {
        var ret  = message || req.returnMessage || req.retMsg ||'';
        res.status(200).json({status: 'failed' , msg: ret})
    }
}

function returnOk(message) {
    return function(req, res, next) {
        var ret  = message || req.returnMessage || req.retMsg ||'';
        res.status(200).json({status: 'success', msg:ret})
    }
}

function returnIfNotDone(msg) {
    return function(req, res, next) {
        if (!req.done) return res.status(500).json({status: 'error', msg:msg})
        else return next();
    }
}

function testMid(number) {
    return function (req, res, next) {
        log.debug('testMid was', req.test, " to ", number);
        req.test=number;
        next();
    }
}

function findWallet(id) {
    return function(req, res, next) {
        var accountId = id || req.params.accountId;
        if (!req.am) {
            attachManagers()(req,res,function () {
                req.account = req.am.getWalletById(accountId);
                next();
            })
        } else {
            req.account = req.am.getWalletById(accountId);
            next();
        }
    }
}

function toSkip (req, type) {
    return (req.done || req.account.type != type)
}

module.exports = {
    //parseMessage: parseMessage,
    findWallet: findWallet,
    attachManagers: attachManagers,
    saveManagers: saveManagers,
    attachWalletManager: attachWalletManager,
    attachPolicyManager: attachPolicyManager,
    savePolicyManager: savePolicyManager,
    saveWalletManager: saveWalletManager,
    checkBody: checkBody,
    attachIdentity: attachIdentity,
    isLoggedIn: isLoggedIn,
    findAccount: findAccount,
    executeMiddlewareList:executeMiddlewareList,
    returnErrorJson: returnErrorJson,
    returnOk: returnOk,
    returnIfNotDone: returnIfNotDone,
    returnError: returnErrorJson,
    returnFailed: returnFailed,
    testMid: testMid,
    toSkip: toSkip
};