var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    flash = require('connect-flash'),
    log = require('log4js').getLogger("Express Route"),
    con = require('../../module/constants');
var Messenger = require('../../module/messenger');
var passport = require('passport');
var log = require('log4js').getLogger("API");
var PPH = require('../../module/index');
var _ = require('lodash');
var User = require('../models/account');
var comm = PPH.commUtil;
var async = require('async');
var gzip = require('gzip-js');
var apiGroups = require('./apiGroup');
var apiWallet = require('./apiWallet');
var apiResponse = require('./apiResponse');
var m = require('./middleware');

module.exports = function (app, passport) {
    app.use('/api', router);

    router.get ('/groups', m.isLoggedIn, apiGroups.getGroups);
    router.post('/group/new', m.isLoggedIn, apiGroups.newGroup);
    router.post('/group/update', m.isLoggedIn, apiGroups.updateGroup);
    router.post('/group/remove', m.isLoggedIn, apiGroups.removeGroup);
    router.post('/group/policy/update', m.isLoggedIn, apiGroups.updateGroupPolicy);
    router.post('/group/policy/add', m.isLoggedIn, apiGroups.addPolicyToGroup);
    router.post('/group/policy/delete', m.isLoggedIn, apiGroups.removePolicyFromGroup);

    router.post('/account/new', m.isLoggedIn, apiWallet.createWallet);
    router.post('/account/delete', m.isLoggedIn, apiWallet.deleteWallet);
    router.post('/account/:accountId/sign', m.isLoggedIn, apiWallet.signWallet);
    router.post('/account/:accountId/edit', m.isLoggedIn, apiWallet.editWallet);

    router.get ('/account/:accountId/clients', m.isLoggedIn, apiWallet.getClients);
    router.get ('/account/:accountId/notifications', m.isLoggedIn, apiWallet.getNotifications);
    router.get ('/account/:accountId/clients/register', m.isLoggedIn, apiWallet.registerClientMessage);
    router.post('/account/:accountId/clients/register', m.isLoggedIn, apiWallet.registerClient);
    router.post('/account/:accountId/clients/HdPrivkey', m.isLoggedIn, apiWallet.postHdPrivkey);
    router.get ('/account/:accountId/clients/HdPrivkey', m.isLoggedIn, apiWallet.getHdPrivkey);
    
    router.get ('/account/:accountId/currentAddress', m.isLoggedIn, apiWallet.getCurrentAddress);
    router.get ('/account/:accountId/balance', m.isLoggedIn, apiWallet.getBalance);
    router.get ('/account/:accountId/backup', m.isLoggedIn, apiWallet.getBackupString);

    router.post('/account/:accountId/makeTransaction', m.isLoggedIn, apiWallet.makeTransaction);
    router.post('/account/:accountId/submitTransaction', m.isLoggedIn, apiWallet.submitTransaction);
    router.post('/account/:accountId/creditRequest', m.isLoggedIn, apiWallet.creditRequest);

    router.post('/account/:accountId/not/update', m.isLoggedIn, apiWallet.updateNotification);

    //Message from someone else, needs to be secured
    router.post('/message/:userId/:accountId', apiResponse.secure);
    //Message manual parsing, user responsible for security of content
    router.post('/message/:accountId', m.isLoggedIn, apiResponse.response);
    router.get('/test', function(req, res) {
        res.status(200).json({test:'ok'});
    })
};