var _ = require('lodash'),
    log = require('log4js').getLogger("API Wallet"),
    types = require('../../module/constants'),
    m = require('./middleware'),
    parser = require('../../module/messageParser');

module.exports = {
    getBalance: getBalance,
    makeTransaction: makeTransaction,
    receiveTransactionToSign: receiveTransactionToSign,
    getCurrentAddress: getCurrentAddress,
    creditRequest: creditRequest,
    getNotifications: getNotifications,
    getCredit: getCredit,
    getTransactions: getTransactions
};

function makeTransaction() {
    return function (req, res, next) {
        if(m.toSkip(req, types.accountTypes.WalletMSS)) return next();
        
        //Return a transaction that is unsigned with the depthlist
        req.account.makeTransaction(req.body.msgData.uri, function (err, failed, transactionData) {
            if (err) return m.returnError(err)(req, res, next);
            if (failed) return m.returnError(failed)(req, res, next);
            log.debug(transactionData);
            req.retMsg = transactionData;
            req.done = true;
            next();
        })
    }

}


/**
 * Retrieve all transactions from wallet, stored on the Bitcoin Network
 */
function getTransactions() {
    return function (req, res, next) {
        if(m.toSkip(req, types.accountTypes.WalletMSS)) return next();
        req.account.getTransactions(true, function (err, list) {
            if (err) return m.returnError(err)(req, res, next);
            req.retMsg = list;
            req.done = true;
            next();
        })
    }
}

function getBalance() {
    return function (req, res, next) {
        if(m.toSkip(req, types.accountTypes.WalletMSS)) return next();

        req.account.getBalance(req.query.testnet, function (err, satoshis) {
            if (err) return m.returnErrorJson(err)(req, res);
            req.retMsg = {
                satoshis: satoshis
            };
            req.done = true;
            next()
        })
    }
}

function receiveTransactionToSign() {
    return function (req, res, next) {
        if(m.toSkip(req, types.accountTypes.WalletMSS)) return next();

        parser[types.messageTypes.MSW_SUBMITTRANSACTION](req.am, req.pm, req.account, req.secInfo, req.body.msgData, function (err, transaction, decision) {
            if (err)  return m.returnErrorJson(err)(req, res, next);
            req.retMsg = {decision: decision, txid: transaction};
            req.done = true;
            if (decision.status == 'YES' || decision.status == 'MORE') {
                log.debug("Saving");
                return m.saveManagers()(req, res, next)
            }
            else {
                next();
            }
        });
    }
}

function getCurrentAddress () {
    return function (req, res, next) {
        if(m.toSkip(req, types.accountTypes.WalletMSS)) return next();
        var address = req.account.getAddressCurrentIndex(req.query.change, req.query.testnet, req.query.next);
        if (req.query.uri) {
            req.returnMessage = 'bitcoin:' + address.toString();
        } else {
            req.returnMessage = {
                address: address.toString(),
                change: req.query.change ? true : false,
                network: address.network.toString(),
                uri: 'bitcoin:' + address.toString()
            };
        }
        req.done = true;
        next();
    }
}

function creditRequest () {
    return function(req, res, next) {
        if(m.toSkip(req, types.accountTypes.WalletMSS)) return next();
        parser[types.messageTypes.MSW_CREDITREQUEST](req.am, req.pm, req.account,req.secInfo, req.body, function(err, msg) {
            if (err) return m.returnError(err)(req, res, next);
            req.retMsg = msg;
            req.done = true;
            next();
        })
    }
}

function getCredit() {
    return  function(req, res, next) {
        if(m.toSkip(req, types.accountTypes.WalletMSS)) return next();

        //Encrypted Message from client asking for his credit
        if (req.secInfo.isFrom) {
            req.retMsg = req.pm.getGroup(req.secInfo.isFrom).getCredit();
        } else {
            //
            req.retMsg = 0;
        }
        req.done = true;
        next();
    }
}

function getNotifications() {
    return function (req, res, next) {
        if(m.toSkip(req, types.accountTypes.WalletMSS)) return next();
        parser[types.messageTypes.ACCOUNT_GETNOT](req.account, req.secInfo, function(err, notList) {
            if (err) return m.returnError(err)(req, res, next);
            req.retMsg = notList;
            req.done = true;
            next();
        })
    }
}

