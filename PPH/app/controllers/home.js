var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    flash = require('connect-flash'),
    log = require('log4js').getLogger("Express Route"),
    AccountManager = require('../../module/walletManager'),
    con = require('../../module/constants'),
    _ = require('lodash'),
    uid = require('uid-safe'),
    m = require('./middleware'),
    accountController = require('./accountController');

module.exports = function (app, passport) {
    app.use('/', router);

    /*
     * ACCOUNTS PAGES
     */
    router.get('/account/all', isLoggedIn, accountController.allAccounts);
    router.get('/account/:accountId',isLoggedIn, accountController.getAccount );

    /*
     * POLICY PAGES
     */
    router.get('/policy', isLoggedIn, function (req, res) {
        var pm = req.user.getPolicyManager();
        var groups = pm.getGroupsArray();
        var clients = pm.clientLookup;
        log.debug(pm.getMemberList());
        res.render('policy', {
            moment : require('moment'),
            groups: _.clone(groups),
            query: req.query,
            clients: _.clone(clients),
            memberList: pm.getMemberList(),
            _:_,
            uid: uid,
            policyActions: con.policyActions})
    });
    
    router.get('/policy/graph/:groupId', isLoggedIn, function(req,res) {
        var group = req.user.getPolicyManager().getGroup(req.params.groupId);
        res.render('policyGraph', {
            group: group, 
            uid: uid,
            idListText: require('../../module/policyFunctions').idListText});
    });

    /*
     *
     *      USER MANAGEMENT
     *
     */
    router.get('/', function (req, res, next) {
        res.render('index')
    });

    router.get('/login', function (req, res) {
        // render the page and pass in any flash data if it exists
        res.render('login', {message: req.flash('loginMessage')});
    });

// show the signup form
    router.get('/signup', function (req, res) {
        log.debug("Serving Signup");
        res.render('signup', {message: req.flash('signupMessage'), accountType: req.query.accountType});
    });

    router.get('/profile', isLoggedIn, function (req, res) {
        var amUser = req.user.getWalletManager();
        log.debug("amUser" , amUser.toString());

        var pmUser = req.user.getPolicyManager();
        log.debug("pmUser" , pmUser.toString());

        res.render('profile', {
            uid: uid,
            accountList: amUser.getAccountListFlat(),
            user: req.user,
            accountManager: amUser.toString(),
            policyManager: pmUser.toString()
        });
    });

    router.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    router.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/account/all',
        failureRedirect: '/',
        failureFlash: true
    }));

    router.post('/login', passport.authenticate('local-login', {
        successRedirect: '/profile',
        failureRedirect: '/login',
        failureFlash: true
    }));
    
    
    router.get('/account/admin/register', isLoggedIn, function (req, res) {
        var am = req.user.getWalletManager();
        var aa = am.getAccountAdmin();
        var pm = req.user.getPolicyManager();
        var ch = pm.getChallenge("ADMIN_ACCOUNT_REGISTER");

        res.render('adminRegister', {
            qrcode: {
                url: "http://localhost:3000/message/57446ba38065f9ab919b6b47",
                pubkey: aa.getAuthPub(),
                type:"ADMIN_ACCOUNT_REGISTER",
                challenge: ch
            }
        })
    });

    router.post('/account/enable2FA', isLoggedIn, function(req,res,next) {
        req.user.enabled2FA(function(err) {
            if (err) return m.returnError('2FA not enabled')(req,res,next);
            return m.returnOk('2FA enabled')(req,res,next);
        })
    });

    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated()) {
            log.trace("Is Authenticated");
            return next();
        }
        passport.authenticate('basic', {session: false})(req, res, next)
    }

};