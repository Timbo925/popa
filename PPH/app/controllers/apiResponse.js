var _ = require('lodash'),
    log = require('log4js').getLogger("API Response"),
    t = require('../../module/constants'),
    m = require('./middleware'),
    parser = require('../../module/messageParser'),
    apiWallet = require('./apiWallet');


function responseDecrypted  () {
    return function(req, res, next) {
        log.debug('Received Message', req.body);
        log.debug('Received Message Secure:', req.secInfo);
        switch (req.body.msgType) {
            case t.messageTypes.ACCOUNT_REG_CLIENT:
                m.executeMiddlewareList(apiWallet.registerClient, req, res, next);
                break;
            case t.messageTypes.ACCOUNT_REG_CLIENT_MSC:
                m.executeMiddlewareList(apiWallet.registerClient, req, res, next);
                break;
            case t.messageTypes.ACCOUNT_REG_CLIENT_MSW:
                m.executeMiddlewareList(apiWallet.registerClient, req, res, next);
                break;
            case t.messageTypes.ACCOUNT_REG_CLIENT_MSC_HDPRIV:
                m.executeMiddlewareList(apiWallet.postHdPrivkey, req, res, next);
                break;
            case t.messageTypes.MSW_GETCURRENTADDRESS:
                m.executeMiddlewareList(apiWallet.getCurrentAddress, req, res, next);
                break;
            case t.messageTypes.MSW_MAKETRANSACTION:
                m.executeMiddlewareList(apiWallet.makeTransaction, req, res, next);
                break;
            case t.messageTypes.MSW_GETBALANCE:
                m.executeMiddlewareList(apiWallet.getBalance, req, res, next);
                break;
            case t.messageTypes.MSW_SUBMITTRANSACTION:
                m.executeMiddlewareList(apiWallet.submitTransaction, req, res, next);
                break;
            case t.messageTypes.N_NEW:
                m.executeMiddlewareList(apiWallet.newNotification, req, res, next);
                break;
            case t.messageTypes.N_UPDATE:
                m.executeMiddlewareList(apiWallet.updateNotification, req, res, next);
                break;
            case t.messageTypes.MSW_CREDITREQUEST:
                m.executeMiddlewareList(apiWallet.creditRequest, req, res, next);
                break;
            case t.messageTypes.ACCOUNT_GETNOT:
                m.executeMiddlewareList(apiWallet.getNotifications, req, res ,next);
                break;
            case t.messageTypes.ACCOUNT_GETCREDIT:
                m.executeMiddlewareList(apiWallet.getCredit, req, res, next);
                break;
            case t.messageTypes.MSW_GETTRANSACTIONS:
                m.executeMiddlewareList(apiWallet.getTransactions, req, res, next);
                break;
            default:
                m.returnErrorJson(['Message Type not recognized', req.body.msgType])(req, res);
        }
    }
}

module.exports = {
    response: [
        // Messages here come from somewhere else, don't give out sensitive information!!
        //TODO Check if encrypted, known, challenge restore/make
        responseDecrypted()
    ],

    secure: [
        m.checkBody({
            'userId': {isEmpty: false, in: 'param'},
            'accountId': {isEmpty: false, in: 'param'}
        }),
        m.findAccount(),
        m.findWallet(),
        function(req,res,next) {
            var secure = {
                isEncrypted: false,
                isKnown: false,
                isFrom: ''
            };
            
            var decrypted = req.account.decryptMessage(req.body);
            log.debug('Decrypted Body',JSON.stringify(decrypted));

            secure.isFrom = req.account.getMessenger().getPubKeyMessage(req.body).toString();
            secure.isTo = req.params.accountId;
            secure.isEncrypted = true;
            secure.isKnown = req.account.getMessenger().getClientByPubKey(secure.isFrom) ? true : false;
            log.debug('Secured:', JSON.stringify(secure));
            
            //req.secure = {};
            req.secInfo = secure;
            req.body = decrypted;
            next();
        },
        responseDecrypted(),
        m.saveManagers(),
        m.returnOk()
    ]
};