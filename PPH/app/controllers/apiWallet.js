var _ = require('lodash'),
    log = require('log4js').getLogger("API Wallet"),
    types = require('../../module/constants'),
    m = require('./middleware'),
    parser = require('../../module/messageParser'),
    apiWalletMSS = require('./apiWalletMSS'),
    apiWalletMSC = require('./apiWalletMSC');

module.exports = {
//##################################
//##########   Wallet   ###########
//##################################    
    signWallet: [
        m.checkBody({
            'commData.senderPubkey': {isPubkey: {errorMessage: 'Not a valid pubkey'}},
            'msgType': {notEmpty: true},
            'accountId': {notEmpty: true, in: 'params'}
        }),
        function (req, res) {
            log.debug('Signing');
            var account = req.user.getWalletManager().getWalletById(req.params.accountId);
            var senderPubkey = req.body.commData.senderPubkey;
            var msg = m.attachIdentity(req, account, req.body);
            var encrypted = account.signMessageJson(senderPubkey, msg);
            res.status(200).json({decrypted: req.body, encrypted: encrypted.toJSON()})
        }],

    editWallet: [
        m.checkBody({
            'name': {notEmpty: true},
            'accountId': {notEmpty: true, in: 'params'},
            'value': {notEmpty: true}
        }),
        m.attachWalletManager(),
        function (req, res, next) {
            log.debug("editWallet:", req.body);
            var account = req.am.getWalletById(req.params.accountId);

            switch (req.body.name) {
                case "name":
                    account.name = req.body.value;
                    break;
                default:
                    break
            }
            next();
        },
        m.saveWalletManager(),
        m.returnOk('Wallet Edited')
    ],

    getClients: [
        m.checkBody({'accountId': {notEmpty: true, in: 'params'}}),
        m.attachWalletManager(),
        function (req, res, next) {
            var clients = req.am.getWalletById(req.params.accountId).getMessenger().getClientList();
            res.status(200).json({clients: clients});
        }
    ],

    registerClientMessage: [
        m.attachWalletManager(),
        function (req, res, next) {
            var account = req.am.getWalletById(req.params.accountId);
            var msg = account.getMessage(types.messageTypes.ACCOUNT_REG_CLIENT);
            msg = m.attachIdentity(req, account, msg);
            req.retMsg = msg;
            next()
        },
        m.saveWalletManager(),
        m.returnOk()
    ],

    getBackupString: [
        m.findWallet(),
        function (req, res, next) {
            req.retMsg = req.account.getBackupString();
            next()
        },
        m.returnOk()
    ],
    
    registerClient: [
        m.checkBody({
            'accountId': {notEmpty: true, in: 'params'},
            'msgType': {notEmpty: true, 'contains': {options: [types.messageTypes.ACCOUNT_REG_CLIENT]}},
            'msgData.accountId': {notEmpty: true},
            'commType': {notEmpty: true},
            'commData.senderPubkey': {isPubkey: {errorMessage: 'Not a valid pubkey'}},
            'commData.url': {optional: true, isURL: true}
        }),
        m.attachManagers(),
        function (req, res, next) {
            var account = req.am.getWalletById(req.params.accountId);
            parser[req.body.msgType](req.am, req.pm, req.body, account, function (err, retMsg) {
                if (err) return m.returnErrorJson(err)(req, res, next);
                if (retMsg) retMsg = m.attachIdentity(req, account, retMsg);
                req.retMsg = retMsg;

                if (account.type == 'WalletMSC') {
                    account.sendMessageEncrypted(req.body.msgData.senderPubkey, retMsg, {}, function (err, retMsg) {
                        if (err) return m.returnErrorJson(err)(req, res, next);
                        if (retMsg) req.retMsg = retMsg;
                        next();
                    });
                } else {
                    next();
                }
            });
        },
        m.saveManagers(),
        m.returnOk()
    ],

    createWallet: [
        m.checkBody({
            'msgType': {notEmpty: true, 'equals': {options: [types.messageTypes.ACCOUNT_NEW]}},
            'msgData.name': {notEmpty: true},
            'msgData.accountType': {notEmpty: true}
        }),
        m.attachManagers(),
        function (req, res, next) {
            parser[types.messageTypes.ACCOUNT_NEW](req.am, req.pm, req.body, function (err, account) {
                log.debug('0', err);
                if (err) return m.returnErrorJson(err)(req, res, next);
                req.returnMessage = account;
                next();
            })
        },
        m.saveManagers(),
        m.returnOk()
    ],

    deleteWallet: [
        m.checkBody({
            'msgType': {notEmpty: true, 'equals': {options: [types.messageTypes.ACCOUNT_REMOVE]}},
            'msgData.pubkey': {notEmpty: true, isPubkey: true}
        }),
        m.attachManagers(),
        function (req, res, next) {
            parser[types.messageTypes.ACCOUNT_REMOVE](req.am, req.pm, req.body, function (err) {
                if (err) return m.returnErrorJson(err)(req, res);
                req.returnMessage = 'Wallet has been deleted';
                next()
            })
        },
        m.saveManagers(),
        m.returnOk()
    ],

//##################################
//#######   Notifications   ########
//##################################
    newNotification: [
        m.checkBody({
            'accountId': {notEmpty: true, in: 'params'},
            'msgData.id': {notEmpty: true, in: 'body'},
            'msgData.type': {notEmpty: true, in: 'body'},
            'msgData.data': {notEmpty: true, in: 'body'}
        }),
        m.attachManagers(),
        m.findWallet(),
        function (req, res, next) {
            parser[types.messageTypes.N_NEW](req.account, req.body.msgData, function (err, msg) {
                if (err) return m.returnError(err)(req, res, next);
                req.retMsg = msg;
                next()
            })
        },
        m.saveManagers(),
        m.returnOk()
    ],

    updateNotification: [
        m.checkBody({
            'accountId': {notEmpty: true, in: 'params'},
            'msgData.id': {notEmpty: true, in: 'body'},
            'msgData.action': {notEmpty: true, in: 'body'}
        }),
        m.attachManagers(),
        m.findWallet(),
        function (req, res, next) {
            log.debug('updateNotification', req.account.name);
            var notReceived = req.account.getMessenger().getNotifications();
            var notReceivedIndex = _.findIndex(notReceived, {id: req.body.msgData.id});
            var notSend = req.account.getMessenger().getNotificationsSend();
            var notSendIndex = _.findIndex(notSend, {id: req.body.msgData.id});

            //This message was received and needs to be send to the origin.
            if (notReceivedIndex > -1) {
                log.debug('Updating Received Notification');
                if (req.body.msgData.action == 'NACK') {
                    notReceived.splice(notReceivedIndex, 1);
                    next();
                } else {
                    req.account.getMessenger().sendMessageToAllEncrypted({
                        msgType: types.messageTypes.N_UPDATE,
                        msgData: {
                            id: req.body.msgData.id,
                            action: req.body.msgData.action
                        }
                    }, {}, function (err, retMsg) {
                        if (err) return m.returnError(err)(req, res, next)
                        notReceived.splice(notReceivedIndex, 1);
                        req.retMsg = retMsg;
                        next();
                    })
                }
            }
            //
            else if (notSendIndex > -1) {
                var notification = notSend[notSendIndex];
                log.debug('Updating Send Notification', notification);
                parser[types.messageTypes.N_UPDATE](req.am, req.pm, req.account, req.secInfo, req.body.msgData, notification, function (err, status, info, done) {
                    if (err) return m.returnError(err)(req, res, next);
                    //notReceived.splice(notReceivedIndex, 1);
                    req.retMsg = {status: status, info: info};
                    next();
                })
            }
            //Wallet doens't know about this notification
            else {
                log.debug('unknown notification', req.body);
                return m.returnOk('Unknown notification')(req, res, next);
            }
        },
        m.saveManagers(),
        m.returnOk()
    ],
    
//##################################
//#######   WalletMSS/MSC   #######
//##################################

    //Receive new Xpriv from someone else
    postHdPrivkey: [
        m.checkBody({
            'accountId': {notEmpty: true, in: 'params'},
            'msgType': {notEmpty: true, 'equals': {options: [types.messageTypes.ACCOUNT_REG_CLIENT_MSC_HDPRIV]}},
            'msgData.hdPrivkey': {notEmpty: true, isHdPrivkey: true}
        }),
        m.attachManagers(),
        m.findWallet(),
        apiWalletMSC.receiveHdPrivkey(),
        m.saveManagers(),
        m.returnIfNotDone('AccountType not supported: MSC'),
        m.returnOk()
    ],

    getHdPrivkey: [
        m.checkBody({
            'accountId': {notEmpty: true, in: 'params'}
        }),
        m.attachWalletManager(),
        m.findWallet(),
        apiWalletMSC.getHdPrivkey(),
        m.returnIfNotDone('AccountType not supported: MSC only'),
        m.returnOk()
    ],

    getCurrentAddress: [
        m.checkBody({
            'accountId': {notEmpty: true, in: 'params'}
        }),
        m.findWallet(),
        apiWalletMSS.getCurrentAddress(),
        apiWalletMSC.getCurrentAddress(),
        m.returnIfNotDone('Cant get currentAddress of non MSW/MSC account'),
        m.saveWalletManager(),
        m.returnOk()
    ],

    getBalance: [
        m.checkBody({
            'accountId': {notEmpty: true, in: 'params'}
        }),
        m.findWallet(),
        apiWalletMSC.getBalance(),
        apiWalletMSS.getBalance(),
        m.returnIfNotDone('Wallet not a MSW/MSC'),
        m.returnOk()
    ],

    getCredit: [
        m.checkBody({
            'accountId': {notEmpty: true, in: 'params'}
        }),
        m.attachPolicyManager(),
        m.findWallet(),
        apiWalletMSS.getCredit(),
        apiWalletMSC.getCredit(),
        m.returnIfNotDone('Wallet not a MSW/MSC'),
        m.returnOk()
    ],

    makeTransaction: [
        m.checkBody({
            'accountId': {notEmpty: true, in: 'params'},
            'msgData.uri': {notEmpty: true, in: 'body'},
            'submit': {optional: true, in: 'query'}
        }),
        m.findWallet(),
        m.attachManagers(),
        apiWalletMSC.makeTransaction(),
        apiWalletMSS.makeTransaction(),
        m.returnIfNotDone('Wallet Type Not Supported'),
        m.returnOk()
    ],
    
    submitTransaction: [
        m.checkBody({
            'accountId': {notEmpty: true, in: 'params'},
            'msgData.transaction': {notEmpty: true, in: 'body'},
            'msgData.satoshis': {notEmpty: true, in: 'body'},
            'msgData.depthList': {notEmpty: true, in: 'body'}
        }),
        m.attachManagers(),
        m.findWallet(),
        apiWalletMSS.receiveTransactionToSign(),
        m.returnOk()
    ],

    creditRequest: [
        m.checkBody({
            'accountId': {notEmpty: true, in: 'params'},
            'msgData.value': {notEmpty: true, in: 'body'}
        }),
        m.attachManagers(),
        m.findWallet(),
        apiWalletMSC.creditRequest(),
        apiWalletMSS.creditRequest(),
        m.returnIfNotDone('Not a supported type for this type of request'),
        m.saveManagers(),
        m.returnOk()
    ],

    getNotifications: [
        m.checkBody({
            'accountId': {notEmpty: true, in: 'params'}
        }),
        m.findWallet(),
        apiWalletMSC.getNotifications(),
        apiWalletMSS.getNotifications(),
        m.returnIfNotDone('Wallet not a MSW/MSC'),
        m.returnOk()
    ],

    getTransactions: [
        m.checkBody({
            'accountId': {notEmpty: true, in: 'params'}
        }),
        m.findWallet(),
        apiWalletMSC.getTransactions(),
        apiWalletMSS.getTransactions(),
        m.returnIfNotDone('Wallet not a MSW/MSC'),
        m.returnOk()
    ]
    
    
};