// Credit user schema: scotch.io

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var PPH = require('../../module/index');
var twoFactor = require('node-2fa');

var userSchema = mongoose.Schema({
    local: {
        email: String,
        password: String,
    },
    facebook: {
        id: String,
        token: String,
        email: String,
        name: String
    },
    twitter: {
        id: String,
        token: String,
        displayName: String,
        username: String
    },
    google: {
        id: String,
        token: String,
        email: String,
        name: String
    },
    manager: {
        account: {type: String, default: "{}"},
        policy: {type: String, default: "{}"}
    },
    fa: {
        enabled: {type:Boolean, default: false},
        secret: {type: String},
        qr: {type: String},
        uri: {type:String}
    }
});

userSchema.pre('init', function(next, data) {
    console.log(data);
    if (!data.fa || !data.fa.secret) {
        data.fa = {}
        var newSecret = twoFactor.generateSecret({name: 'PersonalPaymentHub', account: data._id});
        data.fa.secret = newSecret.secret;
        data.fa.uri = newSecret.uri;
        data.fa.qr = newSecret.qr;
    }
    next()
});

userSchema.methods.enabled2FA = function(cb) {
    this.fa.enabled = true;
    this.save(cb);
};

userSchema.methods.disable2FA = function(cb) {
    this.fa.enabled = false;
    this.save(cb);
};

/**
 *
 * @return {AccountManager}
 */
userSchema.methods.getWalletManager = function() {
    var amOptions = JSON.parse(this.manager.account);
    if (!amOptions.id) amOptions.id = this._id;
    return new PPH.AccountManager(amOptions)
};

/**
 *
 * @return {PolicyManager}
 */
userSchema.methods.getPolicyManager = function () {
    var pmOptions = JSON.parse(this.manager.policy);
    if (!pmOptions.id) pmOptions.id = this._id;
    return new PPH.PolicyManager(pmOptions)
};

userSchema.methods.saveWalletManager = function(am, cb) {
    this.manager.account = am.toString();
    this.save(cb)
};

userSchema.methods.savePolicyManager = function(pm, cb) {
    this.manager.policy = pm.toString();
    this.save(cb)
};

userSchema.methods.saveManagers = function(walletManager, policyManager, cb) {
    this.manager.policy = policyManager.toString() || this.manager.policy;
    this.manager.account = walletManager.toString() || this.manager.account;
    this.save(cb);
};

// generating a hash
userSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.local.password);
};

userSchema.methods.valid2FA = function(token) {
    if (this.fa.enabled) {
        var fa = twoFactor.verifyToken(this.fa.secret, token);
        return !(fa == null || fa.delta != 0)
    } else {
        return true;
    }
};

module.exports = mongoose.model('User', userSchema);

