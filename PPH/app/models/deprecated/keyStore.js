var bitcore = require('bitcore-lib');
var Mnemonic = require('bitcore-mnemonic');
var jf = require('jsonfile');
var fs = require('fs')
var log = require('log4js').getLogger()

//Optional id to load a keyStore from memory
function keyStore(options) {
    var wordList,hdPrivMaster,index,id;
    options = options || {};

    if (Mnemonic.isValid(options.wordList)) {
        log.debug('Creating wallet with wordList ', options)
        createKeyStore(options)
    } else {
        try {
            data = jf.readFileSync(options.id + '.json');
            data.id = options.id;
            log.debug('Loaded wallet with id:', data.id);
            createKeyStore(data);
        } catch (e) {
            log.debug('Creating new wallet with id:' , options.id)
            createKeyStore(options)
        }
    }

    //Constructor
    function createKeyStore(options) {
        wordList = (Mnemonic.isValid(options.wordList)) ? new Mnemonic(options.wordList) : new Mnemonic();
        hdPrivMaster = wordList.toHDPrivateKey();
        index = options.index || 0;
        id = options.id || 0;
        log.debug("Wallet Created: ", wordList, index, id)
    }

    //Functions
    this.getId = function () {return id}
    this.getWordList = function() {return wordList};
    this.getIndex = function () {return index};
    this.getAuthPub = function() {return hdPrivMaster.derive("m/0/0'").publicKey}

    this.getAddressindex = function(d) {
        return new bitcore.Address(hdPrivMaster.derive("m/1/0'").derive(d).publicKey, bitcore.Networks.livenet)
    };

    this.getAddressCurrent = function () {
        return this.getAddressindex(index)
    };

    this.getAddressNext = function() {
        index++;
        return this.getAddressindex(index);
    };

    this.writeFs = function () {
        var data = {
            wordList: wordList.toString(),
            index: index
        };

        jf.writeFileSync(id+".json", data)
    }

    this.removeFs = function () {
        fs.unlinkSync(id+'.json')
        log.debug("Removed wallet with id: " , id)
    }
}

module.exports = keyStore;