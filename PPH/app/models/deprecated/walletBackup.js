/**
 * Created by Tim on 04/05/16.
 */
var bitcore = require('bitcore-lib');
var Mnemonic = require('bitcore-mnemonic');
var jf = require('jsonfile')
var log = require('log4js').getLogger()

var Wallet = (function () {

    // Instance stores a reference to the Singleton
    var instance;

    function init(options) {

        log.debug("Load wallet with options: " , options);
        // Private methods and variables
        var wordList = (Mnemonic.isValid(options.wordList)) ? new Mnemonic(options.wordList) : new Mnemonic();
        var hdPrivMaster = wordList.toHDPrivateKey();
        var authPriv = hdPrivMaster.derive("m/0/0").privateKey; //Keys used for authentication/encrypting
        var authPub  = hdPrivMaster.derive('m/0/0').publicKey;
        var hdPriv = hdPrivMaster.derive("m/1/0"); //Keys used for local payment hb
        var hdPub = hdPriv.hdPublicKey;
        var depth = options. depth || 0;

        function getNextAddress() {
            depth += 1;
            return new bitcore.Address(hdPriv.derive(depth).publicKey, bitcore.Networks.livenet)
        }

        function writeWalletFs() {
            var data = {
                wordList: wordList.toString(),
                depth: depth
            };

            jf.writeFile('walletInfo.dat', data, function(err) {
                if (err) {console.log("Write Error ", err)}
            })
        }

        //Public
        return {
            getHdPub: function () {return hdPub},
            getAuthPub: function() {return authPub},
            getNextAddress: getNextAddress,
            getWordList: function() {return wordList.toString()},
            writeWalletFs: writeWalletFs
        };
    };

    return {
        // Get the Singleton instance if one exists or create one if it doesn't
        getInstance: function () {

            if ( !instance ) {
                var options;
                try {
                    options = jf.readFileSync('walletInfo.dat');
                    log.debug("Read: ", options)
                } catch (e) {
                    options = {}
                }
                instance = init(options);
                if(options !== {}) {instance.writeWalletFs();}

                return instance;
            } else {
                return instance;
            }
        }
    };
})();


module.exports = Wallet;