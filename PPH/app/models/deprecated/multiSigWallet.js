var bitcore = require('bitcore-lib');
var jf = require('jsonfile');
var log = require('log4js').getLogger()

function multiSigWallet(options) {
    // Private vars and functions
    options = options || {};
    var treshold = options.treshold || 2;
    var xpubs = options.xpubs || [];
    var index = options.index || 0;
    var indexChange = options.indexChange || 0;
    
    //Add Xpub String
    this.addXpub = function(xpub) {
        if (bitcore.HDPublicKey(xpub)) {
            xpubs.push(new bitcore.HDPublicKey(xpub))
        } else {
            throw new Error("Not valid xpub")
        }
    };

    this.getXpubs = function () {
        return xpubs;
    };
    

    this.getAddressIndex = function (d, change) {
        if (treshold <= xpubs.length) {
            var pubKeys = [];
            change = change ? 1 : 0;
            //log.debug(xpubs);
            for (var i =0; i<xpubs.length; i++) {
                pubKeys.push(xpubs[i].derive(change).derive(d).publicKey)
            }
            return new bitcore.Address.createMultisig(pubKeys, treshold, 'livenet')
        } else {
            return null;
        }
    }
}

module.exports = multiSigWallet;