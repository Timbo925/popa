var enums = require("./../../../module/constants");
var bitcore = require('bitcore-lib');
var fs = require('fs')
var log = require('log4js').getLogger("Device")

function Device(id, pubkey, type, typeInfo) {
    if (!bitcore.PublicKey(pubkey)) throw new Error("Not a valid pubkey:", pubkey);
    if (!enums.deviceTypes[type]) throw new Error("Not a recognized device type:" + type);

    this.id = id;
    this.pubkey = bitcore.PublicKey(pubkey);
    this.type = type;
    this.typeInfo = typeInfo; //TODO check if typeInfo is correct
}

Device.prototype.toJSON = function () {
  return {
      id: this.id,
      pubkey: this.pubkey.toString(),
      type: this.type,
      typeInfo: this.typeInfo
  }
};

module.exports = Device;
