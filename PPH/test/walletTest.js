var assert = require('chai').assert;
var KeyStore = require('../app/models/deprecated/keyStore');
var log = require('log4js').getLogger("Mocha");
var bitcore = require('bitcore-lib');
var msWallet = require('../app/models/deprecated/multiSigWallet');
var Account = require('../module/wallet');
var MSClient = require('../module/walletMSClient');
var MSWallet = require('../module/walletMSServer');
var AccountManager = require('../module/walletManager');
var types = require('../module/constants');
var Cleint = require('../module/client');
var ECIES = require('bitcore-ecies');
var PolicyManager = require('../module/policyManager');
var PolicyActoin = require('../module/policyActions');
var Group = require('../module/group');
var _ = require('lodash');
var con = require('../module/constants');
var gzip = require('gzip-js');
var Mnemonic = require('bitcore-mnemonic');

describe('Wallet Test', function () {
    describe('Bitcore', function () {
        it("Test", function () {
            var master = bitcore.HDPrivateKey();
            var xpriv = master.derive("m/0/33");
            var xpub = xpriv.hdPublicKey;
            log.trace(xpub.derive(1));
            log.trace("xpriv", xpriv);
            log.trace("xpub", xpub.toObject().childIndex)
        });
    });

    describe('MNemonic', function () {
        it.only('1', function () {
            var master = bitcore.HDPrivateKey();
            var m = new Mnemonic();
            log.debug('Mnemonic', m.toString());
            var m2 = new Mnemonic(master)
        })
    });

    describe("Wallet Test", function () {
        it("Simple Test", function () {
            var a1 = new Account();
            var a2 = new Account();
            assert(a1.getId() != a2.getId(), "ID's are equal");

            a1.saveFS();
            var a3 = new Account();
            a3.loadFs(a1.getId());
            a1.removeFS();
            assert.equal(a1.toJSON().hdPrivMaster, a3.toJSON().hdPrivMaster, "loaded account isn't the same");

        });
    });

    describe("MSClient", function () {
        var ms1, ms2, ms3;

        before(function () {
            ms1 = new MSClient();
            ms1.saveFS();
            ms2 = new MSClient();
            ms3 = new MSClient();
            ms3.loadFs(ms1.getId());
        });
        //Test correct loading objects

        after(function () {
            ms1.removeFS();
        });

        it("Save Wallets", function () {
            log.debug("A1", ms1.toJSON());
            log.debug("A2", ms2.toJSON());
            log.debug("A3", ms3.toJSON());
            assert.equal(ms1.toJSON().hdPrivMaster, ms3.toJSON().hdPrivMaster, 'Should be equals but are not');
        });
        //log.debug(ms1._hdPriv, ms2.hdPriv);


        it("Unique Accounts", function () {
            assert.notEqual(ms1.toJSON().hdPrivMaster, ms2.toJSON().hdPrivMaster, "Should be unequal")
        });

        //Test derivation addresses
        it("Test Derivations", function () {
            var ms4 = new MSClient("xprv9s21ZrQH143K3djMKXmncfuYx7op79ZTAjGXpcfBCmKfVxgUDzT31sRzuHMXSCNKpp7Q8xpnXq4cdn7MHRsKxN8tZogrXEjuU3quRkrn4Z2");
            assert.equal(ms4.getAddressIndex(0, false).toString(), "1HSuy1rgBEPBosnHXQr2sXfTpNxsP2sVL7", "Address is not correct"); // m/0/1'/0/0
            assert.equal(ms4.getAddressIndex(0, true).toString(), "12ACiFgbF95gehxTV6SaVNjfJVCqAVxJYG", "Change Address is not correct"); // m/0/1'/1/0
            ms4.getAddressNext(true);
            ms4.getAddressNext(true);
            assert.notEqual(ms3._indexChange, ms4._indexChange, "Linked objects!!");
            assert.isAbove(ms4._indexChange, 1, "Index Change not growing");
        });
    });
    
    describe("MSWallet", function () {
        var w1, c1, c2, c3;

        before(function () {
            log.trace('-- Before --');
            w1 = new MSWallet("xprv9s21ZrQH143K2jFcnZyVXqt5MQdRfMfXRkhcqCwUqiY3vfDtGEQj2ETtZjZKWA1B51jmiDvUJyNgi9XuCgBbtmGg7shP54PHHNpkVRizJdm");
            log.trace('---');
            c1 = new MSClient("xprv9s21ZrQH143K3yNTUGhKHSLyZn6bNFJzpUEySXf325S37NGb3nUWj7gVx34s5G2TFH2qZ7HopyXbFtQWYZQTjxaynF5AS8J4Pk4gP1L1f7w");
            log.trace('_');
            c2 = new MSClient("xprv9s21ZrQH143K2bN9HVwn3qF1qcKMVoRkVazmpLDHmxyRrZoFAxoeYVLkmTLdCZ1ofmoJnXno54B5oH8YcHxGp4GzFY6BKqdoFPFJiLe8o5Y");
            log.trace('_');
            c3 = new MSClient("xprv9s21ZrQH143K2Mz9G6weF5NU1d7HkYzPUBH9VwDEnnCJoqYWHAGBJcnGcyEM7eXNT3Z1anYiGuTZ9zXfpa6xKBTwwoi5tgz73ZemQZ9ARZq");
            log.trace('-- Before Done --')
        });

        it("No Address", function () {
            assert.equal(w1.getAddressIndex(0), null);
        });

        it("Add Xpubs", function () {
            log.debug('w1 xpbuMaster', w1._client.getXpubMaster());
            w1.addXpubMasters(c1.getXpubMaster());
            assert.throws(function () {
                w1.addXpubMasters(c2.getXpubMaster());
            }, 'Max Keys reached', 'Should trow Max Keys Reached Error');
            
            assert.equal(w1.getPubKeys(0, false).length, 2);
            var pubkeys = [new bitcore.PublicKey("033936b9e7ef221cc7f7d44493aa4668395fede6c12cc7d1d20d63132598b0ff77"), new bitcore.PublicKey("036f7f873475e2fddfcbd926066ad852202d75c61017354e44cdda66c1e92487cb")];
            var address  = new bitcore.Address(pubkeys, 2);
            var address2 = new bitcore.Address.createMultisig(pubkeys, 2);
            assert.equal(address.toString(), address2.toString(), "Both address generations are different");
            assert.equal(address2.toString(), "33rZcv3dyypHLaN5Nr7hD7NiMaCTZ58kDP", 'Multisig Address Should be the same');
            assert.equal(w1.getAddressIndex(0,false,false).toString(), address.toString());
        });

        it("Save Wallet", function () {
            w1.saveFS();
            w1.removeFS();
        });

    });

    describe("ECIES", function () {
        var alicePriv, bobPriv, charliePriv, cypher1, cypher2, cypher3
        before(function () {
            alicePriv = new bitcore.PrivateKey();
            bobPriv = new bitcore.PrivateKey();
            charliePriv = new bitcore.PrivateKey();
            cypher1 = new ECIES().privateKey(alicePriv).publicKey(bobPriv.publicKey);
            cypher2 = new ECIES().privateKey(bobPriv);
            cypher3 = new ECIES().privateKey(charliePriv);
        });

        it("Decryption witout pubkey", function () {
            var data = "This is a buffer";
            var encryptedMessage = cypher1.encrypt(data);
            var decyptedMessage = cypher2.decrypt(encryptedMessage);
            assert.equal(decyptedMessage, data, "Both should be the same")
        });

        it("Message not for me", function () {
            var data = "Data here";
            var encryptedMessage = cypher1.encrypt(data);

            assert.throws(function () {
                cypher3.decrypt(encryptedMessage)
            }, 'Invalid checksum', "Should trow error");
        });

        it("Get Public Key form Message", function () {
            var data = new Buffer("Some buffer here")
            var encryptedMessage = cypher1.encrypt(data);
            var publicKey = bitcore.PublicKey.fromDER(encryptedMessage.slice(0, 33));
            assert.equal(alicePriv.publicKey.toString(), publicKey.toString(), "Retieved Pubkey not the same");
        });
    });

    describe("Message Encryption", function () {
        var c1, c2, c3;

        before(function () {
            c1 = new MSClient("xprv9s21ZrQH143K3yNTUGhKHSLyZn6bNFJzpUEySXf325S37NGb3nUWj7gVx34s5G2TFH2qZ7HopyXbFtQWYZQTjxaynF5AS8J4Pk4gP1L1f7w");
            c2 = new MSClient("xprv9s21ZrQH143K2bN9HVwn3qF1qcKMVoRkVazmpLDHmxyRrZoFAxoeYVLkmTLdCZ1ofmoJnXno54B5oH8YcHxGp4GzFY6BKqdoFPFJiLe8o5Y");
            c3 = new MSClient("xprv9s21ZrQH143K2Mz9G6weF5NU1d7HkYzPUBH9VwDEnnCJoqYWHAGBJcnGcyEM7eXNT3Z1anYiGuTZ9zXfpa6xKBTwwoi5tgz73ZemQZ9ARZq");
        });

        it("Decrypt/Encrypte", function () {
            var messageJson = {id: "Test", status: "success"};
            var encrypted = c1._messenger.encryptJson(messageJson, c2.getAuthPub());
            log.debug('Encrypted', encrypted);
            var string = JSON.stringify(encrypted);
            log.debug('string', string);
            var stringParsed = JSON.parse(string);
            var parsed = new Buffer(stringParsed.data);
            var decryptedJson = c2._messenger.decryptJson(stringParsed, c1.getAuthPub());
            assert.equal(messageJson.id, decryptedJson.id);
        });

        it("Decrypte/Encrypt String", function () {
            encrypted = c1.getMessenger().encrypt("ELLO", c2.getAuthPub());
            decrypted = c2.getMessenger().decrypt(encrypted);
            //log.debug("Messages", encrypted, decrypted, "ELLO");
            assert.equal("ELLO", decrypted, "String encryption not equal");

        });

        it("Sending to messenger", function () {

            //Register Devices with eachother
            c2.getMessenger().registerClient(c1.getAuthPub(), "SERVER", {});
            c1.getMessenger().registerClient(c2.getAuthPub(), "SERVER", {});

            //var em1 = c2.getMessenger().sendMessage(c1.getId(), "Unencrypted Message");
            var em2 = c2.getMessenger().sendMessageEncrypted(c1.getAuthPub(), {status: "Encrypted Json"}, {});
            var em3 = c2.getMessenger().sendMessageEncrypted(c1.getAuthPub(), "Encrpyted Message String", {});

            //var emr1 = c1.getMessenger().receiveMessageEncrypted(c2.getId(), em1);
            var emr2 = c1.getMessenger().receiveMessageEncrypted(em2);
            var emr3 = c1.getMessenger().receiveMessageEncrypted(em3);

            //log.debug(emr2)
            //assert.equal("Unencrypted Message",emr1.message);
            //assert(emr1.encrypted == false);
            assert.equal("Encrypted Json", emr2.message.status);
            assert(emr2.encrypted == true);
            assert.equal("Encrpyted Message String", emr3.message);
            assert(emr3.encrypted == true);
        });
    });

    describe("AccountManager", function () {
        var am1;

        before(function () {
            am1 = new AccountManager()
        });

        it("toString", function () {
            var am2 = new AccountManager();
            am2.createWallet(types.accountTypes.WalletMSC);
            am2.createWallet(types.accountTypes.WalletMSS);
            var saved = JSON.parse(am2.toString());
            var am3 = new AccountManager("Test");
            am3.init(saved);

            assert.equal(
                am2.toJSON().accountList[0]._hdClientMaster.xprivkey,
                am3.toJSON().accountList[0]._hdClientMaster.xprivkey);
        });
    });

    describe('PolicyManager', function () {

        var policyManager;
        var c1;
        /** c1 @type Wallet */
        var c2;
        /** c2 @type Wallet */
        var g;

        before(function () {
            policyManager = new PolicyManager();
            c1 = new Account();
            c2 = new Account();
        });

        it("Adding Clients", function () {
            policyManager.addClient(c1.getAuthPub());
            policyManager.addClient(c2.getAuthPub());
        });

        it("Adding Group", function () {
            var actions = [];
            actions.push({type: con.policyActions.SEND, vars: {lowerBound: 0, upperBound: 10}});
            actions.push({
                type: con.policyActions.AUTH, vars: {
                    upperBound: 100,
                    lowerBound: 10,
                    threshold: 1,
                    groups: [c2.getAuthPub()]
                }
            });

            g = policyManager.addGroup("AuthGroup", actions);
            log.debug("AuthGroup: ", g.toString());

            assert.throws(function () {
                g.addOrChangePolicyAction("", {lowerBound: 0, upperBound: 10})
            }, "Non existing type");

            assert.throws(function () {
                g.addOrChangePolicyAction(con.policyActions.AUTH, {lowerBoundd: 0, upperBound: 10})
            }, "Made with not the right vars");
        });

        it("Group Decisions", function () {
            var d1 = g.decide("SEND", {value: 5});
            var d2 = g.decide("SEND", {value: 111});
            var d3 = g.decide("SEND", {value: 50});

            log.debug("\n", d1, "\n", d2, "\n", d3);

            assert.equal(d1.status, 'YES');
            assert.equal(d2.status, 'NO');
            assert.equal(d3.status, 'AUTH');
        });

        it("Policy Decisions", function () {
            policyManager.addMembers(c1.getAuthPub(), ["AuthGroup"]);
            var d1 = policyManager.decide(c1.getAuthPub(), "SEND", {value: 5});
            var d2 = policyManager.decide(c1.getAuthPub(), "SEND", {value: 55});
            var d3 = policyManager.decide(c1.getAuthPub(), "SEND", {value: 555});

            log.debug("\n", d1, "\n", d2, "\n", d3);
            assert.equal(d1.status, "YES");
            assert.equal(d2.status, "MORE");
            assert.equal(d3.status, "NO");
        });

        it("Updating Members", function () {
            //log.debug("Member Lookup", '\n', policyManager.memberLookup);
            policyManager.updateMembers('AuthGroup', [c2.getAuthPub()]);
            policyManager.addMembers('AuthGroup', [c1.getAuthPub()]);
            //log.debug("Member Lookup",'\n', policyManager.memberLookup)

        });

        it("Adding Groups and save/loading", function () {
            var policyManager = new PolicyManager();
            policyManager.addClient(c1.getAuthPub());
            policyManager.addClient(c2.getAuthPub());
            var group = policyManager.addGroup("TestGroup");
            group.addOrChangePolicyAction(con.policyActions.ROOT);
            //group.addOrChangePolicyAction(con.policyActions.AUTH, {upperBound: 100, lowerBound: 0});
            policyManager.addMembers(c2.getAuthPub(), ["TestGroup"]);

            policyManager.saveFS();
            var before = policyManager.toJSON();

            var pm2 = new PolicyManager();
            pm2.loadFS();

            var after = pm2.toJSON();

            assert.isOk(_.isEqual(before, after), "Not the same after loading");

            assert(policyManager.getGroup("TestGroup") instanceof Group);
            assert(policyManager.getGroup("TestGroup").policyActions[0] instanceof PolicyActoin);
        });
    });
    
    describe("Random", function () {
        it("Remove from list" , function () {
            var list = JSON.parse('[{"type":"AUTH","vars":{"lowerBound":0,"upperBound":0,"threshold":0,"groups":[]}},{"type":"ROOT","vars":{}},{"type":"SEND","vars":{"lowerBound":"0","upperBound":"1"}}]')
            _.remove(list, function(o) {return o.type == 'AUTH'})
        })
    })

});