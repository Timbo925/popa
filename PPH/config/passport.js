//Implementation based on Scotch.io

var LocalStrategy   = require('passport-local').Strategy;
var User            = require('../app/models/account');
var log             = require('log4js').getLogger("PassportJS");
var BasicStrategy   = require('passport-http').BasicStrategy;
var parser = require('../module/messageParser');
var types = require('../module/constants');
var PM = require('../module/policyManager');
var AM = require('../module/walletManager');
var twoFactor = require('node-2fa');

module.exports = function(passport) {
    passport.serializeUser(function (user, done) {
        done(null, user.id)
    });

    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user)
        });
    });

    passport.use('local-signup', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, email, password, done) {
            // asynchronous
            // Account.findOne wont fire unless data is sent back
            log.debug("Entering local-signup before tick");
            process.nextTick(function () {

                log.debug("Entering local-signup");
                // find a user whose email is the same as the forms email
                // we are checking to see if the user trying to login already exists
                User.findOne({'local.email': email}, function (err, user) {
                    // if there are any errors, return the error
                    if (err) {
                        console.log('1');
                        log.debug("Local Singup Find Account Error", err);
                        return done(err);
                    }
                    if (user) {
                        console.log('2');
                        log.debug("Found Account Signup", user);
                        return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                    } else {
                        console.log('3');
                        log.debug("Creating New Account")
                        // if there is no user with that email
                        // create the user
                        var newUser = new User();

                        // set the user's local credentials
                        newUser.local.email = email;
                        newUser.local.password = newUser.generateHash(password);

                        // save the user
                        newUser.save(function (err) {
                            if (err) throw err;
                            log.debug("New user saved", newUser);

                            var am = new AM();
                            var pm  = new PM();
                            var account = am.createWallet(req.body.accountType, {name: email});
                            var group = pm.addClient(account.getAuthPub());
                            group.displayName = email;

                            newUser.manager.account = am.toString();
                            newUser.manager.policy = pm.toString();
                            var newSecret = twoFactor.generateSecret({name: 'PersonalPaymentHub', account: newUser._id});
                            newUser.fa.secret = newSecret.secret;
                            newUser.fa.uri = newSecret.uri;
                            newUser.fa.qr = newSecret.qr;

                            newUser.save(function(err){
                                if (err) throw err;
                                return done(null, newUser);
                            });
                        });
                    }
                });

            });

        })
    );

    passport.use('local-login', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'email',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, email, password, done) { // callback with email and password from our form
            User.findOne({ 'local.email' :  email }, function(err, user) {
                // if there are any errors, return the error before anything else
                if (err) return done(err);
                if (!user) return done(null, false, req.flash('loginMessage', 'Account not found'));
                if (!user.validPassword(password)) return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
                if (!user.valid2FA(req.body.fa)) return done(null, false, req.flash('loginMessage', '2 Factor key was wrong'));
                return done(null, user);
            });

        })
    );

    passport.use('basic', new BasicStrategy(
        function(username, password, done) {
            User.findOne({ 'local.email' :  username }, function(err, user) {
                // if there are any errors, return the error before anything else
                if (err) return done(err);
                if (!user) return done(null, false);
                if (!user.validPassword(password)) return done(null, false);
                return done(null, user);
            });
        }
    ))
};