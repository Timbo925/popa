var _ = require('lodash'),
    types = require('../module/constants'),
    bitcore = require('bitcore-lib');

module.exports = {
    customValidators: {
        isMember: function(input) {
            return (_.isString(input) || _.isArray(input))
        },
        isGroups: function(input) {
            return (_.isString(input) || _.isArray(input))
        },
        isPolicyAction: function(input) {
            return _.has(types.policyActions, input);
        },
        isString: function(input) {
            return (typeof input == 'string')
        },
        isPubkey: function(input) {
           try {
               new bitcore.PublicKey(input);
               return true;
           } catch(e) {
               return false;
           }
        },
        isHdPrivkey: function(input) {
            try {
                new bitcore.HDPrivateKey(input);
                return true;
            } catch(e) {
                return false;
            }
        }

    }
};