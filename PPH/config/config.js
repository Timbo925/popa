var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'pph'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/pph-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'pph'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/pph-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'pph'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/pph-production'
  }
};

module.exports = config[env];
