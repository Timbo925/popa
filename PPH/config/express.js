var express = require('express');
var glob = require('glob');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compress = require('compression');
var methodOverride = require('method-override');
var passport = require('passport');
var flash = require('connect-flash');
var session = require('express-session');
var morgan = require('morgan');
const MongoStore = require('connect-mongo/es5')(session);
var expressValidator = require('express-validator');
var log = require('log4js').getLogger("Express");
var validatorOptions = require('./validator');

module.exports = function (app, db, config) {
    var env = process.env.NODE_ENV || 'development';
    app.locals.ENV = env;
    app.locals.ENV_DEVELOPMENT = env == 'development';

    app.set('views', config.root + '/app/views');
    app.set('view engine', 'jade');

    //Configure passport
    require('./passport')(passport);
    //app.use(morgan('dev'));
    // app.use(favicon(config.root + '/public/img/favicon.ico'));
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(expressValidator(validatorOptions));
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.use(cookieParser());
    app.use(compress());
    app.use(express.static(config.root + '/public'));
    app.use(methodOverride());
    app.use(session({
        secret: "This will be a secret",
        maxAge: new Date(Date.now() + 3600000),
        store: new MongoStore({mongooseConnection: db}),
        resave: false,
        saveUninitialized: true
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(flash());


    //var controllers = glob.sync(config.root + '/app/controllers/*.js');

    require('../app/controllers/home')(app, passport);
    require('../app/controllers/api')(app, passport);

    // controllers.forEach(function (controller) {
    //     require(controller)(app, passport);
    // });

    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    if (app.get('env') === 'development') {
        app.use(function (err, req, res, next) {
            res.status(err.status || 500);
            res.render('error', {
                message: err.message,
                error: err,
                title: 'error'
            });
        });
    }

    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {},
            title: 'error'
        });
    });
};
