var baseurl = 'https://test-insight.bitpay.com/api',
    request = require('request'),
    _ = require('lodash'),
    log = require('log4js').getLogger("BtcNetwork");
var fx = require('money');

module.exports.btcBalance = btcBalance;
module.exports.getUtxoSet = getUtxoSet;
module.exports.broadcast = broadcast;
module.exports.getTransactions = getTransactions;

module.exports.fx = fx;

fx.base = "USD";
fx.rates = {"BTC":"0.0015365349600117", "SAT": "153653.49600117","EUR":"0.903179","HKD":"7.757587","CNY":"6.690480","CAD":"1.305275","BRL":"3.298139","AUD":"1.311145","TRY":"2.894418","RUB":"63.801120","HRK":"6.767702","NOK":"8.412663","CHF":"0.985278","SEK":"8.523121","RON":"4.056539","PLN":"3.978956","HUF":"283.047327","GBP":"0.753233","DKK":"6.717666","CZK":"24.415643","BGN":"1.766438","JPY":"104.606214","USD":"1.000000"}
fx.settings = {from: "BTC", to: "USD"};

function btcBalance(addrs, cb) {
    getUtxoSet(addrs, function (err, utxo) {
        if (err) return cb(err);
        var satoshis = 0;
        _.each(utxo, function(ut) {
            satoshis += parseInt(ut.satoshis)
        });
        cb(null, satoshis);
    })
}

function getTransactions(addrs, cb) {
    var url = baseurl + '/addrs/txs';
    var options = {
        method: 'post',
        url : url,
        form: {addrs: addrs.toString()},
        json: true
    };
    request(options, function(err, res, body) {
        if (err) return cb(err);
        if (res.statusCode !=200) return cb(res);
        log.debug('Retrieved Transactions from:' , addrs.length, 'Body',  body);
        cb(null, body.items);
    })
}

/**
 * Get the UTXO set of all addresses
 * @param addrs {[Address||String]} List of addresses
 * @param cb {Function} Callback
 */
function getUtxoSet(addrs, cb) {
    var url = baseurl + '/addrs/utxo';
    var options = {
        method: 'post',
        url: url,
        form: {addrs: addrs.toString()},
        json: true
    };
    log.trace('Posting Options', options);
    request(options, function(err, res, body) {
        if (err) return cb(err);
        if (res.statusCode !=200) return cb(res);
        log.debug('UTXO Set returned #addrs:' , addrs.length, '#utxos',  body);
        cb(null, body);
    })
}

function broadcast(tx, cb) {
    var url = baseurl + '/tx/send';
    var options = {
        method: 'post',
        url: url,
        form: {rawtx: tx.toString()}
    };
    log.debug('Broadcasting', tx.toString());
    request(options, function(err, res, body) {
        if (err) return cb(err);
        if (res.statusCode !=200) return cb(res);
        log.debug('Transaction Submitted', body);
        cb(null, JSON.parse(body));
    });
}