var bitcore = require('bitcore-lib');
var Mnemonic = require('bitcore-mnemonic');
var log = require('log4js').getLogger("WalletAdmin");
var uid = require('uid-safe');
var Account = require('./wallet');
var types = require('./constants');


/**
 *
 * @param identifier
 * @constructor
 * @type{Account}
 */
function WalletAdmin(identifier) {
    Account.apply(this, [identifier]);
    this.type = types.accountTypes.WalletAdmin;
}

WalletAdmin.prototype = new Account();
WalletAdmin.prototype.constructor = WalletAdmin;


/**
 * Init gets called by constructor Wallet
 * @param options
 */
WalletAdmin.prototype.init = function (options) {
    Account.prototype.init.call(this, options);
};

//Override Functions
WalletAdmin.prototype.toJSON =  function () {
    return Account.prototype.toJSON.call(this);
};


module.exports = WalletAdmin;