var AccountManager = require('./walletManager'),
    PolicyManager = require('./policyManager'),
    commUtil = require('./communication');

/**
 * @type {{AccountManager: AccountManager, PolicyManager: PolicyManager, comUtil: commUtil}}
 */
module.exports = {
    AccountManager: AccountManager,
    PolicyManager: PolicyManager,
    commUtil: commUtil
};