var t = require('./constants'),
    log = require('log4js').getLogger('messageParser'),
    _ = require('lodash'),
    async = require('async')
apiBtc = require('./apiBtcNetwork');


var exporter = {};

/**
 * Register with Wallet to @param account
 * @param am {AccountManager}
 * @param pm {PolicyManager}
 * @param msg {object}
 * @param account {Wallet}
 * @param cb {function} callback
 */
exporter[t.messageTypes.ACCOUNT_REG_CLIENT] = function (am, pm, msg, account, cb) {
    log.debug('Parsing', t.messageTypes.REG_CLIENT_ACCOUNT);
    if (!checkMsg(msg)) return cb(new Error('Missing comm or msg data'), msg);
    am.getWalletById(msg.msgData.accountId).registerClient(msg.commData.senderPubkey, msg.commType, msg.commData);
    pm.registerClient(msg.commData.senderPubkey);
    cb(null)
};

/**
 * Register Account_MSW to MSC account
 * @param am {AccountManager}
 * @param pm {PolicyManager}
 * @param msg {object}
 * @param account {WalletMSC}
 * @param cb {function} callback
 */
exporter[t.messageTypes.ACCOUNT_REG_CLIENT_MSW] = function (am, pm, msg, account, cb) {
    log.debug('Registering', t.messageTypes.REG_CLIENT_ACCOUNT_MSW, 'with account', account.id);
    if (!checkMsg(msg)) return cb(['Missing comm or msg data', msg]);
    if (account.type != t.accountTypes.WalletMSC) return cb('Cant register MSW with non MSC');
    account.registerClient(msg.commData.senderPubkey, msg.commType, msg.commData);
    //pm.registerClient(msg.commData.senderPubkey);
    var retMsg = account.getMessage(t.messageTypes.ACCOUNT_REG_CLIENT);
    retMsg.msgData.challenge = msg.msgData.challenge; //Set the challenge from the MSW so he will accept
    cb(null, retMsg)
};

/**
 * Register Wallet MSC to MSW account
 * @param am {AccountManager}
 * @param pm {PolicyManager}
 * @param msg {object}
 * @param account {WalletMSS}
 * @param cb {function} callback
 */
exporter[t.messageTypes.ACCOUNT_REG_CLIENT_MSC] = function (am, pm, msg, account, cb) {
    log.debug('Registering', t.messageTypes.ACCOUNT_REG_CLIENT_MSC, 'with account', account.id);
    if (!checkMsg(msg)) return cb(['Missing comm or msg data', msg]);
    if (account.type != t.accountTypes.WalletMSS) return cb('Cant register MSC with non MSW');
    try {
        if (!account.isValidChallenge(msg.msgData.challenge)) {
            log.debug('Not a valid Challenge');
            return cb('Not a valid challenge');
        } else {
            log.debug('Valid Challenge')
        }
        account.registerClient(msg.commData.senderPubkey, msg.commType, msg.commData);
        pm.registerClient(msg.commData.senderPubkey);
        account.addXpubMasters(msg.msgData.hdPubkey);
        log.debug('Registered successfully');
        cb(null, 'Registered successfully to MSW as first client')
    } catch (e) {
        log.debug(e);
        var retMsg = account.getMessage(t.messageTypes.ACCOUNT_REG_CLIENT_MSC);
        cb(null, retMsg)
    }
};

exporter[t.messageTypes.ACCOUNT_REG_CLIENT_MSC_HDPRIV] = function (am, pm, msg, account, cb) {
    log.debug('Parsing', t.messageTypes.ACCOUNT_REG_CLIENT_MSC_HDPRIV, 'with account', account.id);
    try {
        if (!checkMsg(msg)) return cb(['Missing comm or msg data', msg]);
        if (account.type != t.accountTypes.WalletMSC) return cb('NEW XPRIV only for a Client');
        account.updateHdPriv(msg.msgData.hdPrivkey);
        cb(null, 'XPriv Updated successfully');
    } catch (e) {
        cb(e);
    }
};

/**
 * Parsing a signed transaction coming from a MSC to MSW based on the Policy
 * @param am {AccountManager}
 * @param pm {PolicyManager}
 * @param account {WalletMSServer}
 * @param secInfo {Object}
 * @param tData {Object}
 * @param cb {function}
 * @return {*}
 */
exporter[t.messageTypes.MSW_SUBMITTRANSACTION] = function (am, pm, account, secInfo, tData, cb) {
    log.debug('Parsing', t.messageTypes.MSW_SUBMITTRANSACTION, 'with account', account.id, tData);

    //Messages need to encrypted and from a know source (aka Registered source)
    if (!secInfo.isEncrypted || !secInfo.isKnown) return cb('Info not send secure' + JSON.stringify(secInfo));
    pm.decide(secInfo.isFrom, t.policyActions.SEND, {value: tData.satoshis, address: tData.address}, function(err, decision) {
        log.info('Decision:', decision);

        if (decision.status == 'YES') {
            tData = account.signTransaction(tData);
            //TODO submit to network
            apiBtc.broadcast(tData.transaction, function (err, data) {
                if (err) return cb(err);
                return cb(null, data.txid, decision);
            });
        }

        else if (decision.status == 'NO') {
            return cb(null, null, decision);
        }

        else if (decision.status == 'MORE') {
            //decision.policies[0].vars.groups[0]
            var groupName = pm.getGroup(secInfo.isFrom).getDisplayName()
            async.each(decision.policies, function (policy, callback) {
                if (policy.type == t.policyActions.AUTH) {
                    var data = {
                        msg: 'Authentication needed: ' + groupName + ' wants to send ' + tData.satoshis + ' SAT / ' +  tData.value_CC + ' ' + tData.CC,
                        tData: tData,
                        clients: policy.vars.groups,
                        threshold: policy.vars.threshold
                    };
                    account.getMessenger().newNotification(t.policyActions.AUTH, policy.vars.groups, data, function (err, msg) {
                        log.debug('NewNotification', msg);
                        callback(null)
                    });
                }
            }, function (err) {
                if (err) return cb(err, null, decision);
                return cb(null, null, decision);
            });
        }

        else {
            log.error('Unknown decision', decision);
            return cb('Unknown decision');
        }
    });

};

/**
 * Parsing a new received notification
 * @param account {Wallet}
 * @param notification {Object}
 * @param cb {function}
 */
exporter[t.messageTypes.N_NEW] = function (account, notification, cb) {
    log.debug('Parsing', t.messageTypes.N_NEW, notification);
    account.getMessenger().receiveNotification(notification);
    cb(null, 'Added Notification');
};

/**
 * Parsing an update to a certain notification
 * @param account {Wallet|WalletMSS|WalletMSC}
 * @param updateData {Object}
 * @param notification {Object}
 * @param cb {function} err, status, info
 */
exporter[t.messageTypes.N_UPDATE] = function (am, pm, account, secInfo, updateData, notification, cb) {
    //Check if still exist

    if (updateData.action == "ACK" && notification.type == 'AUTH' && account.type == t.accountTypes.WalletMSS) {
        var data = notification.data;
        //From client is allowed to act on this notification
        if (notification.data.clients.indexOf(secInfo.isFrom) > -1) {
            _.pull(notification.data.clients, secInfo.isFrom); //Remove clients so it won't double authenticate
            notification.data.threshold--;
            //Sign and broadcast
            if (notification.data.threshold <= 0) {
                var tData = account.signTransaction(notification.data.tData);
                apiBtc.broadcast(tData.transaction, function (err, data) {
                    if (err) return cb(err);
                    var notSend = account.getMessenger().getNotificationsSend();
                    notSend.splice(_.findIndex(notSend, {id: notification.id}), 1);
                    return cb(null, 'Transaction Broadcasted', data.txid, true);
                });
            }
            //More authentications needed
            else {
                account.getMessenger().updateNotification(notification);
                return cb(null, 'More Authentication needed', null);
            }
        } else {
            return cb('Client not allowed to perform this action')
        }
    }

    else if (updateData.action == 'ACK' && notification.type == 'CREDIT' && account.type == t.accountTypes.WalletMSS) {
        if (notification.data.clients.indexOf(secInfo.isFrom) > -1) {
            _.pull(notification.data.clients, secInfo.isFrom); //Remove clients so it won't double authenticate
            notification.data.threshold--;
            if (notification.data.threshold <= 0) {
                var policy = pm.getGroup(notification.data.groupName).getPolicy(t.policyActions.CREDIT);
                policy.vars.credit = parseInt(policy.vars.credit) + parseInt(notification.data.value);
                var notSend = account.getMessenger().getNotificationsSend();
                notSend.splice(_.findIndex(notSend, {id: notification.id}), 1);
                return cb(null, notification.data.value + ' credit added.');
            }
            else {
                account.getMessenger().updateNotification(notification);
                return cb(null, 'More Authentication needed', null);
            }
        } else {
            return cb('Client not allowed to perform this action')
        }
    }

    else if (updateData.action == 'NACK' && account.type == t.accountTypes.WalletMSS) {
        if (notification.data.clients.indexOf(secInfo.isFrom) > -1) {
            _.pull(notification.data.clients, secInfo.isFrom); //Remove clients so it won't double authenticate
            _.pull(notification.clients, secInfo.isFrom); //Remove clients so it won't double authenticate
            cb(null, 'Notification Rejected')
        } else {
            return cb('Client not allowed to perform this action')
        }
    }

    else {
        log.error(updateData.action, notification.type, accoutn.type);
        return cb('Something went wrong')
    }
};

/**
 * Create an new account and add to policy
 * @param am {AccountManager}
 * @param pm {PolicyManager}
 * @param msg {object}
 * @param cb {function} callback
 */
exporter[t.messageTypes.ACCOUNT_NEW] = function (am, pm, msg, cb) {
    log.debug('Parsing', t.messageTypes.REG_CLIENT_ACCOUNT, msg);
    if (!checkRequiredMsgData(msg)) return cb('Not all needed msgData Present');
    var account = am.createWallet(msg.msgData.accountType, msg.msgData);
    var group = pm.addClient(account.getAuthPub());
    group.displayName = msg.msgData.name;
    cb(null, account)
};

/**
 * Delete Wallet based on the pubkey
 * @param am {AccountManager}
 * @param pm {PolicyManager}
 * @param msg {object}
 * @param cb {function} callback
 */
exporter[t.messageTypes.ACCOUNT_REMOVE] = function (am, pm, msg, cb) {
    log.debug('Parsing', t.messageTypes.ACCOUNT_REMOVE, msg);
    try {
        if (!checkRequiredMsgData(msg)) return cb('Not all needed msgData Present');
        am.deleteAccountByPubkey(msg.msgData.pubkey);
        pm.removeGroup(msg.msgData.pubkey);
        cb(null)
    } catch (e) {
        cb(e)
    }
};

exporter[t.messageTypes.MSW_CREDITREQUEST] = function (am, pm, account, secInfo, msg, cb) {
    log.debug('Parsing', t.messageTypes.MSW_CREDITREQUEST, 'with msg', msg);
    if (!secInfo.isEncrypted || !secInfo.isKnown) return cb('Info not send secure' + JSON.stringify(secInfo));

    var group = pm.getGroup(secInfo.isFrom);
    pm.decide(secInfo.isFrom, t.policyActions.CREDIT, {value: msg.msgData.value}, function(err, decision) {
        //Allowed to ask for this much credit, send out notifications
        if (decision.status == 'YES') {
            var groupName = group.getDisplayName();
            async.eachSeries(pm.memberLookup[secInfo.isFrom], function (groupName, cb) {
                var group = pm.getGroup(groupName);
                var credit = group.getPolicy(t.policyActions.CREDIT);
                if (!credit) return cb(null);

                var msgNot = {
                    msg: 'Credit Request: ' + group.getDisplayName() + ' requesting ' + msg.msgData.value_CC + ' ' + msg.msgData.CC,
                    groupName: groupName,
                    value: Math.round(msg.msgData.value),
                    clients: credit.vars.groups,
                    threshold: credit.vars.threshold
                };
                account.getMessenger().newNotification(t.policyActions.CREDIT, credit.vars.groups, msgNot, cb)
            }, function (err) {
                if (err) return cb(err);
                cb(null, 'Credit requests are send to the various devices');
            })
        }
        //Not allowed to ask for credit
        else {
            cb(decision.msg)
        }
    });
};

exporter[t.messageTypes.ACCOUNT_GETNOT] = function(wallet, secInfo, cb) {
    if (!secInfo.isEncrypted || !secInfo.isKnown) return cb('Info not send secure' + JSON.stringify(secInfo));
    cb(null, wallet.getMessenger().getNotificationsSend(secInfo.isFrom))
};

module.exports = exporter;

function checkRequiredCommData(msg) {
    return (_.every(t.commTypesData[msg.commType], function (o) {
        return _.has(msg, o)
    }))
}
function checkRequiredMsgData(msg) {
    return (_.every(t.messageData[msg.msgType], function (o) {
        return _.has(msg, o)
    }))
}
function checkMsg(msg) {
    return (checkRequiredCommData(msg) && checkRequiredMsgData(msg))
}