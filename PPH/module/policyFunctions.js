var _ = require('lodash');
var moment = require('moment');
var con = require('./constants');
var types = require('./constants');
var uid = require('uid-safe');
var log = require('log4js').getLogger("policyFunction");
var async = require('async');
var PolicyAction = require("./policyActions");

var decisionFalse = {status: 'NO', msg: 'Action not allowed'};
var decisionTrue = {status: 'YES', msg: 'Action allowed'};

function hasAllCommand(type, vars) {
    vars = vars || [];
    if (!_.has(con.policyActionCommand, type)) return false;
    return (_.every(con.policyActionCommand[type], _.partial(_.has, vars)))
}
function isAllowedRateLimit(policy, value) {
//['upperBound', 'timeAmount', 'timeScale', 'timeHistory']
    var fromTimeSeconds = moment().subtract(policy.vars.timeAmount, policy.vars.timeScale).unix();
    var toCount = _.filter(policy.vars.timeHistory, function (o) {
        return o[0] >= fromTimeSeconds;
    });
    var spendAmount = _.reduce(toCount, function (count, o) {
        return count + parseInt(o[1]);
    }, 0);

    return parseInt(policy.vars.upperBound) >= (spendAmount + value);
}
function addRatelimitPolicy(ratelimitPolicy, value) {
    ratelimitPolicy.vars.timeHistory.push([moment().unix(), value])
}
function addRatelimitPolicySend(policyActions, value) {
    var ratelimitPolicy = _.find(policyActions, {type: types.policyActions.RATELIMIT});
    if (ratelimitPolicy) addRatelimitPolicy(ratelimitPolicy, value);
    return this;
}

function hasAllVars(action,vars) {
    return function() {
        if (!hasAllCommand(action, vars)) {log.info("Doesn't have all required commands", types.policyActionCommand[action]);
            return decisionFalse;
        } else {
            return null
        }
    }
}

function isRoot(policyActions) {
    return function() {
        if (_.find(policyActions, {type: "ROOT"})) {
            decisionTrue['policies'] = [_.find(this.policyActions, {type: "ROOT"})];
            return decisionTrue;
        } else {
            return null
        }
    }
}

function canStop(policyActions) {
    return function() {
        var stopPolicy = _.find(policyActions, {type: types.policyActions.STOP});
        if (stopPolicy) {
            var now = moment();
            var before = moment(stopPolicy.vars.fromDate, 'DD/MM/YY');
            var after = moment(stopPolicy.vars.toDate, 'DD/MM,YY');

            if (now.isBetween(before,after)
                && stopPolicy.vars.enable =='on') {
                decisionFalse.msg = 'STOP is activated between ' + before.format() +' and' + after.format() + '. ';
                return decisionFalse;
            } else {
                return null
            }
        } else {
            return null
        }
    }
}

function hasAction(policyActions, action) {
    return function() {
        var policyAction = _.find(policyActions, {type: action});
        if (!policyAction) {
            decisionFalse.msg ='Action not configured for this device';
            return decisionFalse;
        } else {
            return null;
        }
    }
}

function executeDecisionList(list, next) {
    log.debug(list);
    var decision = null;
    _.forEach(list, function(l) {
        decision = l();
        if (decision) {return false}
    });
    next(null, decision);
}

function executeDecisionObject(splitList,list, cb) {
    var decision = null;
    async.eachSeries(list, function(l, callback) {
        if (l.length) {
            executeDecisionList(l, function(err, dec) {
                if (err) return callback(err);
                if (dec) {
                    decision = dec;
                    callback('Exit', dec);
                } else {
                    callback(null);

                }
            })
        } else {
            executeDecisionObject(_.tail(splitList), [l[_.head(splitList)]],function(err, dec) {
                if (err) return callback(err);
                if (dec) {
                    decision = dec;
                    callback('Exit', dec);
                } else {
                    callback(null);

                }
            })
        }
    }, function(err) {
        decision ? cb(null,decision) : cb(err);
    });
}

function canSend(policyActions, action, vars) {
    return function() {
        var sendPolicy = _.find(policyActions, {type: types.policyActions.SEND});
        if (sendPolicy.vars.upperBound >= vars.value && sendPolicy.vars.lowerBound < vars.value) {
            //Update the ratelimit
            addRatelimitPolicySend(policyActions, vars.value);

            //Allowed to straight send the payment
            decisionTrue['policies'] = [sendPolicy];
            return decisionTrue;
        } else {
            return false
        }
    }
}

function canSendCredit(policyActions, action, vars) {
    return function() {
        //If we want to send and we have enough credit on our account,
        var credit = _.find(policyActions, {type: types.policyActions.CREDIT});
        if (credit && credit.vars.credit > vars.value) {
            credit.vars.credit = credit.vars.credit - vars.value; //Deduct from credit
            decisionTrue['policies'] = [credit];
            return decisionTrue
        } else {
            return null;
        }
    }
}

function canSendBlacklist(policyActions, action, vars) {
    return function() {
        var blacklist = _.find(policyActions, {type: types.policyActions.BLACKLIST});
        if (blacklist && _.indexOf(blacklist.vars.blacklist, vars.address) > -1) {
            decisionFalse['policies'] = [blacklist];
            decisionFalse['msg'] = 'Address is blacklisted';
            return decisionFalse
        } else {
            return null;
        }
    }
}

function canSendWhitelist(policyActions, action, vars) {
    return function() {
        var whitelist = _.find(policyActions, {type: types.policyActions.WHITELIST});
        if (whitelist && _.indexOf(whitelist.vars.blacklist, vars.address) > -1) {
            addRatelimitPolicySend(policyActions, vars.value);
            decisionTrue['policies'] = [blacklist];
            decisionTrue['msg'] = 'Address is on the whitelist';
            return decisionFalse
        } else {
            return null;
        }
    }
}

function canSendAuth(policyActions, action, vars) {
    return function() {
        //Possible that value is allowed with certain authentication rights.
        var auth = _.find(policyActions, {type: types.policyActions.AUTH});
        if (auth && auth.vars.upperBound > vars.value && auth.vars.lowerBound < vars.value) {
            return {
                status: "AUTH",
                msg: "Needs authentication from other clients",
                policies: [auth]
            }
        } else {
            return null;
        }
    }
}

function canSendRate(policyActions, action, vars) {
    return function() {
        var ratelimitPolicy = _.find(policyActions, {type: types.policyActions.RATELIMIT});
        if (ratelimitPolicy && !isAllowedRateLimit(ratelimitPolicy, vars.value)) {
            decisionFalse['msg'] = 'Transaction would put you over your daily limit of ' + vars.value;
            return decisionFalse;
        } else {
            return null
        };
    }
}

function returnNoDecision() {
    return function () {
        return {status: 'NO', msg: 'No limit could take a decision.'};
    }
}

function returnYesDecision() {
    return function () {
        return decisionTrue;
    }
}

function canRequestCredit(policyActions, action,vars) {
    return function () {
        var credit = _.find(policyActions, {type: types.policyActions.CREDIT});
        if (credit.vars.groups.length == 0) {
            decisionFalse.msg = 'No devices configured to ask for permission. Login with your Account Wallet > Policy to add devices';
            return decisionFalse;
        }
        //Allowed to ask for this much credit
        else if (credit.vars.upperBound < Math.round(vars.value)) {
            decisionFalse.msg = "You're not allowed to request this much. Maximum is " + credit.vars.upperBound + ' SAT';
            return decisionFalse;
        } else {
            return null
        }
    }
}

//var numList = [[1,2,3],{'SEND':[7,4,5,6,9], 'CREDIT':[8,10]}];

var idListText = {
    1: 'Root',
    2: 'Stop Limitation',
    3: 'Requires All Actions',
    4: 'Standard Send',
    5: 'Credit Send',
    6: 'Authenticated Send',
    7: 'Rate Limitation',
    8: 'Allowed to Requesting Credit',
    9: 'Return No Decision',
    10: 'Return Yes Decision',
    11: 'Send Blacklist',
    12: 'Send Whitelist'
};

function buildGraph(toBuild,policyActions,action,vars) {
    var idList = {
        1: isRoot,
        2: canStop,
        3: hasAction,
        4: canSend,
        5: canSendCredit,
        6: canSendAuth,
        7: canSendRate,
        8: canRequestCredit,
        9: returnNoDecision,
        10: returnYesDecision,
        11: canSendBlacklist,
        12: canSendWhitelist,
    };
    var list = [];
    list.push([hasAllVars(action,vars)]);
    
    toBuild =_.map(toBuild,function(x) {
        return mapFunc(x, policyActions, action ,vars, idList)
    });
    return _.concat(list, toBuild);
}

function mapFunc(i, policyActions, actions,vars, idList) {
    if(typeof i == 'number') {
        return idList[i](policyActions, actions,vars)
    } else if (i.length) {
        return _.map(i, function(value) {
            return mapFunc(value, policyActions, actions,vars, idList)
        })
    } else {
        console.log('item',i)
        return _.mapValues(i, function(value) {
            return mapFunc(value, policyActions, actions,vars, idList)
        })
    }
}

module.exports = {
    executeDecisionList: executeDecisionList,
    executeDecisionObject:executeDecisionObject,
    hasAllVars: hasAllVars,
    isRoot: isRoot,
    canStop: canStop,
    hasAction : hasAction,
    canSend : canSend,
    canSendCredit: canSendCredit,
    canSendAuth: canSendAuth,
    canSendRate: canSendRate,
    canSendBlacklist: canSendBlacklist,
    canSendWhitelist: canSendWhitelist,
    canRequestCredit: canRequestCredit,
    returnNoDecision: returnNoDecision,
    returnYesDecision: returnYesDecision,
    decisionTrue: decisionTrue,
    decisionFalse: decisionFalse,
    buildGraph: buildGraph,
    idListText: idListText
};

