var bitcore = require('bitcore-lib');
var Mnemonic = require('bitcore-mnemonic');
var jf = require('jsonfile');
var fs = require('fs')
var log = require('log4js').getLogger("Wallet");
var uid = require('uid-safe');
var Messenger = require('./messenger');
var types = require('./constants');
var Client = require('./client');

function Wallet(identifier) {
    this._options = {};
    this.type = types.accountTypes.Wallet;
    log.trace("Wallet Construct");

    //Check if we are creating with Mnemonic and/or Private key as identifier
    if (Mnemonic.isValid(identifier)) {
        this._options = {mnemonic: new Mnemonic(identifier).toString()}
    } else if (bitcore.HDPrivateKey.isValidSerialized(identifier)) {
        this._options = {hdPrivMaster: identifier}
    } else if (identifier) {
        this._options = identifier;
    } else {
        this._options = {mnemonic: new Mnemonic().toString()}
    }

    this.init(this._options);
}

Wallet.prototype.init = function (options) {
    //log.debug("Creating Wallet with options: ", JSON.stringify(options))
    log.trace("Wallet Init", options);
    this.version = 1;
    this.id = options.id || uid.sync(18);
    this.name = options.name || "";
    this._mnemonic = options.mnemonic ? new Mnemonic(options.mnemonic) : new Mnemonic();
    this._hdPrivMaster = options.hdPrivMaster ? new bitcore.HDPrivateKey(options.hdPrivMaster) : this._mnemonic.toHDPrivateKey()

    //Loading Messenger
    var messengerOptions = options.messenger || {client: {privateKey: this._hdPrivMaster.derive("m/0/0'").privateKey}};
    //log.debug("Messenger Options", messengerOptions);
    this._messenger = new Messenger(messengerOptions);
    return this
};

/**
 * Override this to make sure important inherited objects are also saved correctly
 * @returns JSON critical info of the object
 */
Wallet.prototype.toJSON = function () {
    return {
        type: this.type,
        version: this.version,
        name: this.name,
        id: this.id,
        mnemonic: this._mnemonic.toString(),
        hdPrivMaster: this._hdPrivMaster.toString(),
        messenger: this._messenger.toJSON()
    }
};

/**
 * Return string used for backup
 * @return {String}
 */
Wallet.prototype.getBackupString = function () {
    return this._mnemonic.toString()
};

Wallet.prototype.getType = function () {
    return this.type
};

Wallet.prototype.toString = function () {
    return JSON.stringify(this.toJSON());
};

Wallet.prototype.getId = function () {
    return this.id;
};

Wallet.prototype.getAuthPub = function () {
    return this._messenger.getClient().getPublicKey().toString()
};

Wallet.prototype.loadFs = function (id) {
    try {
        var data = jf.readFileSync(id + '.json');
        log.debug(this.type);
        this.init(data)
    } catch (err) {
        log.error("Wallet Creation with given/unknown identifier:", id);
    }
    return true;
};

Wallet.prototype.saveFS = function () {
    return jf.writeFileSync(this.id + '.json', this.toJSON());

};

Wallet.prototype.removeFS = function () {
    fs.unlinkSync(this.id + '.json');
    log.debug("Removed wallet with id: ", this.id)
    return null;
};

/**
 *
 * @param a1 {Wallet}
 * @returns {boolean}
 */
Wallet.prototype.isEqualTo = function (a1) {
    var a2 = this;
    return a1.type == a2.type &&
        a1.id == a2.id &&
        a1._hdPrivMaster.toString() == a2._hdPrivMaster.toString();
};

Wallet.prototype.getMessage = function (type) {
    switch (type) {
        // case types.messageTypes.REG_NEW_CLIENT_R:
        //     return {msgType: types.messageTypes.REG_NEW_CLIENT_R};
        case types.messageTypes.ACCOUNT_REG_CLIENT:
            return {
                msgType: types.messageTypes.ACCOUNT_REG_CLIENT,
                msgData: {accountId: this.id, accountType:types.accountTypes}};
        default:
            return {
                msgType: 'Not supported by account or unknown type'
            }
    }
};

Wallet.prototype.registerClient = function(pubkey, commType, commData) {
    this._messenger.registerClient(pubkey,commType,commData);
    return this;
};

/**
 *
 * @returns {Messenger}
 */
Wallet.prototype.getMessenger = function () {
    return this._messenger;
};

Wallet.prototype.signMessageJson = function (toPubKey, message) {
    return  this._messenger.encryptJson(message,toPubKey)
};

Wallet.prototype.sendMessageToAllEncrypted = function(msg, options, cb) {
    return this._messenger.sendMessageToAllEncrypted(msg, options, cb);
};

Wallet.prototype.sendMessageEncrypted = function(toPubKey, message, options, cb) {
    return this._messenger.sendMessageEncrypted(toPubKey, message, options ,cb);
};

Wallet.prototype.decryptMessage = function(message) {
  return this._messenger.decryptJson(message);  
};

Wallet.prototype.getBalance = function () {
    return 0;
};

module.exports = Wallet;