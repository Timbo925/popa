var log = require('log4js').getLogger("NotificationCenter");
var uid = require('uid-safe');
var _ = require('lodash');
var async = require('async');

function NotificationCenter(options) {
    options = options || {};
    this._sendNotifications = options._sendNotifications || [];
    this._receivedNotifications = options._receivedNotifications || [];
}

NotificationCenter.prototype.toJSON = function () {
    return {
        _sendNotifications: this._sendNotifications,
        _receivedNotifications: this._receivedNotifications
    }
};

NotificationCenter.prototype.toString = function () {
    return JSON.stringify(this.toJSON());
};

/**
 * Create a notification
 * @param type {String} Type of notification eg: NOT,SEND
 * @param data {Object} Object to be saved
 * @return {Object} Notification object
 */
NotificationCenter.prototype.newNotification = function (type, data, clientList) {
    var notification = {
        id: uid.sync(8),
        type: type,
        data: data,
        clients: clientList
    };
    this._sendNotifications.push(notification);
    return notification;
};

NotificationCenter.prototype.receiveNotification = function (notification) {
    if(!_.every(['id','type','data'], _.partial(_.has, notification))) return false;
    this._receivedNotifications.push(notification);
    return this._receivedNotifications;
};

NotificationCenter.prototype.getSendNotification = function (pubkeyFilter) {
    if (pubkeyFilter) {
        return _.filter(this._sendNotifications, function(not) {
            return (_.indexOf(not.clients, pubkeyFilter) > -1)
        });
    } else {
        return this._sendNotifications;
    }
};

NotificationCenter.prototype.getReceiveNotification = function () {
    return this._receivedNotifications;
};

NotificationCenter.prototype.getNotificationById = function (id) {
    var not = _.find(this._receivedNotifications, function(n) {return n.id == id});
    var not2 = _.find(this._sendNotifications, function(n) {return n.id == id});
    if (not) return not;
    if (not2) return not2;
    return false;
};

NotificationCenter.prototype.updateNotification = function (notification) {
    var not = _.find(this._receivedNotifications, function(n) {return n.id == notification.id});
    var not2 = _.find(this._sendNotifications, function(n) {return n.id == notification.id});
    if (not) {not = notification}
    else if (not2) {not2 = notification}
    return this;
};


module.exports = NotificationCenter;