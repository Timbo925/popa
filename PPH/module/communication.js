var bitcore = require('bitcore-lib');
//var AccountManger = require('./accountManager');
//var PolicyManager = require('./policyManager');
var log = require('log4js').getLogger("Communication Util");
var types = require('./constants');
var _ = require('lodash');
var request = require('request');
var uid = require('uid-safe');

exports.sendMessage = function (client, message, opt, cb) {
    opt = opt || {};
    var id = uid.sync(8);
    var url = client.typeInfo.url;
    if(!_.startsWith(url, 'http://')) {url = 'http://' + url}
    log.info(id, 'Sending Message to client:', url);
    var options = {
        method: 'post',
        json: true,
        qs: opt.query,
        body: message,
        url: url
    };
    log.trace('Options to send\n', options);
    request.post(options, function(err, res, body) {
        if (err) {
            log.error(err);
            return cb(err);
        }
        log.info(id, 'Response Message status:', res.statusCode);
        log.info(id, 'Response Body: ', body);
        if (res.statusCode != 200) {
            cb(body)
        } else {
            cb(null, body)
        }
    });
};

/**
 * Returns PubKey from the message Buffer
 * @param message {Buffer}
 * @return {String}
 */
exports.getPubKeyMessage = function (message) {
    return bitcore.PublicKey.fromDER(message.slice(0, 33)).toString();
};

//TODO parse could check if message is allowed...
/**
 *
 * @param accountManager {AccountManager}
 * @param policyManager {PolicyManager}
 * @param msgOriginal {{}}
 */
exports.parseMessage = function (accountManager, policyManager, msgOriginal) {

    //TODO check message is right format
    /*
        STEP 1
        Get the decrypted message based on the the
     */
    var msgBuffer = new Buffer(msgOriginal.data);
    var ret = {msgType: "PARSED_MESSAGE"};

    var msg = accountManager.decryptMessage(msgBuffer);
    //if (!msg) res.status(500).json({error: "Couldn't decrypt the message"});

    /*
     STEP 2
     Add Add standard info to the result
     */
    ret.senderPubkey = this.getPubKeyMessage(msgBuffer);
    ret.receivedMessage = msg;

    log.info("Parsing ", msg.msgType);

    /*
        STEP 3
        Parse the messages
            3.1 check if all required data from message is present
            3.2 (optional) ask for a decision to the policyManager
                3.2.1 Handle decision
            3.3 Parse the message
     */
    switch (msg.msgType) {
        case types.messageTypes.REG_NEW_CLIENT_R:
            // 1. Check if all required field are present
            if (!checkRequiredCommData(msg)) return {status: "failed"};
            if (!checkRequiredMsgData(msg)) return {status: "failed"};

            //TODO: 2. Check challenge OK
            //      3. Register client to admin account
            var account = accountManager.getAccountAdmin();
            var regClient = account.getMessenger().registerClient(ret.senderPubkey, msg.commType, msg.commData);
            log.debug(ret.senderPubkey, "Added to AdminAccount");

            // 4. Register with Policy + Make Admin
            var resAddClient = policyManager.addClient(ret.senderPubkey);
            var resAddGroupRoot = policyManager.addGroupToClient(ret.senderPubkey, "ROOT");
            log.debug(ret.senderPubkey, "Added to group: ROOT");
            log.debug("RegClient:", regClient, "ResAddCleint", resAddClient, "ResAddGroupRoot", resAddGroupRoot)
            ret.status = "ok";
            break;

        case types.messageTypes.TEST_ROOT:
            //var groups = policyManager.getClient(ret.senderPubkey);
            //ret.groups = groups;
            //var allowed = policyManager.isAllowed(ret.senderPubkey, types.policyActions.SEND, {value: 1000})
            //ret.allowed = allowed;
            var decision = policyManager.decide(ret.senderPubkey, types.policyActions.ROOT, {});
            log.debug("TEST_ROOT Decision" , decision);
            break;

        case types.messageTypes.REG_CLIENT_ACCOUNT:
            if (!checkRequiredCommData(msg)) return {status: "failed"};
            if (!checkRequiredMsgData(msg)) return {status: "failed"};

            //Register with the account
            var account = accountManager.getWalletById(msg.commData.accountId);
            account.getMessenger().registerClient(ret.senderPubkey, msg.commType, msg.commData);

            //Register with policy
            policyManager.addClient(ret.senderPubkey)
            break;

        default:
            break;

    }
    return ret
};

function checkRequiredCommData(msg) {
    if (!_.every(types.commTypesData[msg.commType], function (o) {
            return _.has(msg, o)
        })) {
        log.debug("Not all types present")
        return false;
        //return {status: "Failure", msg: msg, needs: types.commTypesData[msg.commType]};
    }
    log.debug("All commData present");
    return true;
}
function checkRequiredMsgData(msg) {
    if (!_.every(types.messageData[msg.msgType], function (o) {
            return _.has(msg, o)
        })) {
        log.debug("Not all MsgData present");
        return false;
        //return {status: "Failure", msg: msg, needs: types.commTypesData[msg.commType]};
    }
    log.debug("All msgData present");
    return true;
}