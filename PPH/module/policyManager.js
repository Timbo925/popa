var _ = require('lodash');
var Group = require('./group');
var con = require('./constants');
var bitcore = require('bitcore-lib');
var log = require('log4js').getLogger("PolicyManager");
var jf = require('jsonfile');
var uid = require('uid-safe');
var async = require('async');

function PolicyManager(options) {
    this.clientLookup = {};
    this.groupLookup = {};
    this.memberLookup = {};
    this.challengeLookup = {};
    this.init(options);
}

PolicyManager.prototype.toJSON = function () {
    return {
        clients: this.clientLookup,
        groups: this.groupLookup,
        challenges: this.challengeLookup,
        memberLookup: this.memberLookup
    }
};

PolicyManager.prototype.init = function (json) {
    log.trace("Init PolicyManager with: ", json);
    json = json || {};
    var that = this;
    this.id = json.id || "";
    this.clientLookup = json.clients || {};
    this.groupLookup = {};
    this.groupLookup["ROOT"] = new Group({name: "ROOT"});
    this.groupLookup["ROOT"].addOrChangePolicyAction(con.policyActions.ROOT, {});

    _.each(json.groups, function (value, key) {
        that.groupLookup[key] = new Group(value)
    });
    this.challengeLookup = json.challenges || {};
    this.memberLookup = json.memberLookup || {}
    return this;
};

PolicyManager.prototype.toString = function () {
    return JSON.stringify(this.toJSON())
};

PolicyManager.prototype.getNewChallenge = function (message) {
    var ch = uid.sync(10);
    this.challengeLookup[ch] = message;
    return ch
};

PolicyManager.prototype.getMessage = function (type, options) {
    var msg = {};
    switch (type) {
        case con.messageTypes.PM_REGISTER_NEW:
            msg = {
                type: con.messageTypes.PM_REGISTER_NEW
            };
            break;
        default:
            msg = {}
    }

    if (msg != {}) {
        msg.ch = this.getNewChallenge(msg)
    }
    return _.clone(msg);
};

PolicyManager.prototype.getChallenge = function (ch) {
    return this.challengeLookup[ch];
};

PolicyManager.prototype.isValidChallenge = function (ch, type) {
    if (!this.challengeLookup[ch]) return false;
    var typeCh = this.challengeLookup[ch];
    delete this.challengeLookup[ch];
    return (typeCh == type);
};

/**
 *
 * @return {{}|*}
 */
PolicyManager.prototype.getGroupsArray = function() {
    var array = [];
    _.forEach(this.groupLookup, function(group,key) {
        array.push(group)
    });
    return array;
};

PolicyManager.prototype.getGroups = function() {
    return this.groupLookup;
};

/**
 * @param name
 * @param options
 * @return {Group}
 */
PolicyManager.prototype.addGroup = function addGroup(name, policyActions) {
    if (this.groupLookup[name]) return this.groupLookup[name];

    var options = {};
    if (policyActions) options.policyActions = policyActions;
    if (name) options['name'] = name;
    this.groupLookup[name] = new Group(options);
    this.updateMembers(name)
    return this.groupLookup[name];
};

PolicyManager.prototype.removeGroup = function (name) {
    var that = this;
    delete this.groupLookup[name];
    var delList = this.memberLookup[name] || [];
    delete this.memberLookup[name];
    _.forEach(delList, function (e) {
        _.pull(that.memberLookup[e], name)
    })
    return this;
};

/**
 *
 * @param name {String}
 * @return {Group}
 */
PolicyManager.prototype.getGroup = function (name) {
    return this.groupLookup[name];
};

PolicyManager.prototype.getMemberList = function () {
    return this.memberLookup;
    // var res = {};
    // _.forEach(this.clientLookup, function(memberArray, key) {
    //     res[key] = memberArray;
    //     _.forEach(memberArray, function (member) {
    //         if (res[member] && _.indexOf(res[member], key) == -1) {
    //             res[member].push(key)
    //         } else if (!res[member]) {
    //             res[member] = [key];
    //         }
    //     })
    // });
    // return res;
};

PolicyManager.prototype.addMembers = function (name, members) {
    return this.updateMembers(name, _.union(this.memberLookup[name], members));
}

PolicyManager.prototype.updateMembers = function(name, members) {
    var that = this;
    members = members || [];
    members = _.union(members, [name]);
    log.debug("Updating members:" ,name, members, this.memberLookup);

    that.memberLookup[name] = members
    _.forEach(that.memberLookup, function (memberList, member) {
        log.trace(member, memberList)
       if (member == name) {
            that.memberLookup[name] = members;
       } else {
           if(_.indexOf(members, member) > -1) {
               log.trace("Add to", member, name);
               that.memberLookup[member] = _.union(memberList, [name])
           } else {
               log.trace('Pull from:', member, name, that.memberLookup[member]);
               _.pull(that.memberLookup[member],name);
           }
       }
    });

    log.debug("Updated members:", this.memberLookup);


    //return this.memberLookup[name] = members;
    // log.debug(this.clientLookup);
    // log.debug(name, members);
    // var that = this;
    // if (this.clientLookup[name]) {
    //     _.pull(members, name); //Make sure he keeps rights to it own group
    //     members.push(name);
    //     this.clientLookup[name] = members;
    // } else {
    //     _.forEach(members, function (member) {
    //         resolve(that.clientLookup, name, member)
    //     })
    // }
    // var that = this;
    // if (this.clientLookup[name]) {
    //     //This is a client who's groups need to be edited
    //     this.clientLookup[name] = members;
    // } else {
    //     _.forEach(memebers, function (member) {
    //         if (that.clientLookup[member]) {
    //             that.clientLookup[member].push(name)
    //         }
    //     });
    // }
};

PolicyManager.prototype.getClient = function (pubkey) {
    return this.clientLookup[pubkey.toString()];
};

/**
 * Adding a new client with group list + add own group to list
 * @param pubkey {String|bitcore.PublicKey}
 * @returns {Group} group of the client
 */
PolicyManager.prototype.addClient = function (pubkey) {
    pubkey = pubkey.toString();
    if (!this.memberLookup.hasOwnProperty(pubkey)) {
        this.memberLookup[pubkey] = [pubkey];
        this.groupLookup[pubkey] = new Group(
            {name: pubkey})
    }
    return this.groupLookup[pubkey];
};

PolicyManager.prototype.registerClient = function(pubkey) {
    return this.addClient(pubkey);
};

/**
 *
 * @param pubkey {String|PublicKey}
 * @param groupname {String}
 * @return {boolean} False if no action is undertaken
 */
PolicyManager.prototype.addGroupToClient = function (pubkey, groupname) {
    if (_.has(this.groupLookup, groupname)
        && _.has(this.memberLookup, pubkey.toString())
        && !_.includes(this.memberLookup[pubkey.toString()], groupname)) {
        this.memberLookup[pubkey.toString()].push(groupname)
        return true;
    } else {
        return false;
    }
};


PolicyManager.prototype.getPolicyByType = function (pubkey, type) {
    var groups = this.memberLookup[pubkey];
    var policyArray = [];
    _.forEach(groups, function (group) {
        policyArray.push(group.getPolicy(type))
    })
    return policyArray;
};

/**
 *
 * @param pubkey {bitcore.PublicKey|String}
 * @param action {con.policyActions|String}
 * @param value
 * @return {{status: string, msg: string, policies: []}}
 */
PolicyManager.prototype.decide = function (pubkey, action, value, cb) {
    var that = this;
    var decisionFalse = {status: 'NO', msg: 'Action not allowed because following reasons: '};
    var decisionTrue = {status: 'YES', msg: 'Action allowed'};
    var decision = {status: 'MORE', msg: 'Needs actions to be approved', policies: []};

    var groups = this.memberLookup[pubkey];

    log.debug("Groups to decide", groups);

    async.eachSeries(groups, function(gName, callback) {
        that.groupLookup[gName].decide(action, value, function(err, gDecision) {
            log.debug("Group Decision", gDecision);
            if (gDecision.status == "YES") {
                decision = gDecision;
                callback('stop');
            } else if (gDecision.status != 'NO') {
                decision.policies = _.union(decision.policies, gDecision.policies);
                callback()
            } else {
                decisionFalse.msg = decisionFalse.msg + gDecision.msg;
                callback()
            }
        });
    }, function(err) {
        log.debug("Done Deciding")
        if (decision.status == 'MORE' && decision.policies.length == 0) {
            cb(null,decisionFalse);
        } else {
            cb(null, decision);
        }
    })
};

PolicyManager.prototype.saveFS = function () {
    try {
        jf.writeFileSync(this.id + "_PolicyManager.json", this.toJSON());
        return true;
    } catch (e) {
        log.error("Saving PolicyManager", e);
        return false
    }
};

PolicyManager.prototype.loadFS = function (id) {
    id = id || "";
    try {
        var json = jf.readFileSync(id + "_PolicyManager.json");
        this.init(json);

    } catch (e) {
        log.error("Error loading PolicyManager", e)
    }
    return this;
};


function isPublicKey(key) {
    try {
        new bitcore.PublicKey(key);
        return true
    } catch (e) {
        return false
    }
}
function resolve(clientLookup, name, member) {
    if (bitcore.PublicKey(member)) {
        clientLookup[member] = _.union(clientLookup[member], name)
    } else {
        var list = this.getMemberList()[member];
        _.forEach(list, function(o) {
            resolve(clientLookup, name, o)
        })
    }
}

module.exports = PolicyManager;