var bitcore = require('bitcore-lib');
var jf = require('jsonfile');
var log = require('log4js').getLogger("WalletMSC");
var Wallet = require('./wallet');
var types = require('./constants');
var _ = require('lodash');


/**
 * Create MultiSig Client Wallet
 * @param identifier
 * @constructor
 * @type{Wallet}
 */
function WalletMSClient(identifier) {
    //Calling constructor of Wallet to initialize correct values
    //this._options contains all options read from disk
    log.trace("WalletMSC Construct");
    Wallet.apply(this, [identifier]);
}

WalletMSClient.prototype = new Wallet(); //Inherit form Wallet
WalletMSClient.prototype.constructor = WalletMSClient; //Set contructor to this one

//Override Functions

/**
 * JSON Object with crytical Wallet info
 * @returns {{type, id, hdPrivMaster}|JSON}
 */
WalletMSClient.prototype.toJSON = function () {
    var json = Wallet.prototype.toJSON.call(this);
    json.hdClientMaster = this._hdClientMaster.toString();
    json.index = this._index;
    json.indexChange = this._indexChange;
    return json;
};

WalletMSClient.prototype.init = function (options) {
    Wallet.prototype.init.call(this, options);
    log.trace("WalletMSS Init before MSC");
    this.type = types.accountTypes.WalletMSC;
    this._hdClientMaster = options.hdClientMaster ?  new bitcore.HDPrivateKey(options.hdClientMaster): this._hdPrivMaster.derive("m/0/1'");
    this._hdPriv = this._hdClientMaster.derive(0);
    this._hdPrivChagne = this._hdClientMaster.derive(1);
    this.hdPub = this._hdPriv.hdPublicKey;
    this._index = options.index || 0;
    this._indexChange = options.indexChange ||0;
    return this;
};

//Extended Functions

/**
 * Get the master HDPublicKey of the client
 * @return {HDPublicKey}
 */
WalletMSClient.prototype.getXpubMaster = function() {
    return this._hdClientMaster.hdPublicKey;
};

WalletMSClient.prototype.updateHdPriv = function (hdPriv) {
    if (bitcore.HDPrivateKey(hdPriv)) {
        this._hdClientMaster = new bitcore.HDPrivateKey(hdPriv);
        this._hdPriv = this._hdClientMaster.derive(0);
        this._hdPrivChagne = this._hdClientMaster.derive(1);
        this.hdPub = this._hdPriv.hdPublicKey;
    } else {
        throw hdPriv + ' not a valid HDPrivateKey'
    }
    return this;
};

/**
 * 
 * @param d {Number} Index depth to get
 * @param change {Boolean} Get change address or not
 * @param testnet {Boolean} Get the testnet
 * @return {PublicKey}
 */
WalletMSClient.prototype.getAddressIndex = function(d, change, testnet) {
    var network = testnet ? 'testnet' : 'livenet';
    log.debug('HDPriv', this._hdPrivMaster ,this._hdClientMaster, this._hdPriv);
    var pubKey = change ? this._hdPrivChagne.derive(d).publicKey : this._hdPriv.derive(d).publicKey;
    return new bitcore.Address(pubKey, network)
};

WalletMSClient.prototype.getAddressCurrent = function (change, testnet) {
    var i = change ? this._indexChange : this._index;
    return this.getAddressIndex(i,change,testnet);
};

/**
 * @returns Address of the next index
 */
WalletMSClient.prototype.getAddressNext = function (change) {
    var i = change ? ++this._indexChange : ++this._index;
    return this.getAddressIndex(i, change);
};

WalletMSClient.prototype.getMessage = function (type) {
    switch(type) {
        case types.messageTypes.ACCOUNT_REG_CLIENT:
            return {
                msgType: types.messageTypes.ACCOUNT_REG_CLIENT_MSC,
                msgData: {
                    accountId: this.id,
                    hdPubkey: this.getXpubMaster(),
                    senderPubkey: this.getAuthPub(),
                    name: this.name
                }
            };
        case types.messageTypes.ACCOUNT_REG_CLIENT_MSW_SENDHDPUB:
            return {
                msgType: types.messageTypes.ACCOUNT_REG_CLIENT_MSC_HDPUB,
                msgData: {hdPubkey: this._hdClientMaster.hdPublicKey.toString()}
            };
        case types.messageTypes.ACCOUNT_REG_CLIENT_MSC_HDPRIVGET:
            return {
                msgType: types.messageTypes.ACCOUNT_REG_CLIENT_MSC_HDPRIV,
                msgData: {hdPrivkey: this._hdClientMaster.toString()}
            };
        default:
            return Wallet.prototype.getMessage.call(this, type);
    }
};

/**
 *
 * @param tData {{transaction: String, depthList: [String]}
 */
WalletMSClient.prototype.signTransaction = function (tData) {
    log.debug('Signing:', tData)
    var that = this;
    var transaction = new bitcore.Transaction(tData.transaction);
    var depthList = tData.depthList || [];
    _.forEach(depthList, function (d) {
        transaction.sign(that._hdClientMaster.derive(d).privateKey)
        log.debug('Signed with: ', that._hdClientMaster.derive(d).privateKey.toString())
    });

    tData.transaction = transaction;
    tData.depthList = depthList;

    return tData;
};


module.exports = WalletMSClient;