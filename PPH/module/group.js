var con = require('./constants');
var types = require('./constants');
var uid = require('uid-safe');
var log = require('log4js').getLogger("Groups");
var _ = require('lodash');
var PolicyAction = require("./policyActions");
var moment = require('moment');
var async = require('async');
var f = require('./policyFunctions');

/**
 * @param options {{ name: !string, actions : {} }}
 * @constructor
 */
function Group(options) {
    log.trace("Create group with options:", options);
    var that = this;
    this.name = options.name || uid.sync(18);
    this.displayName = options.displayName || "";
    this.policyActions = [];
    this.policyList = options.policyList || JSON.stringify([[1,2,3],{'SEND':[11,7,4,5,6,9], 'CREDIT':[8,10]}]);
    this.addOrChangePolicyAction(types.policyActions.SEND, {lowerBound: 0, upperBound: 0});
    //this.addOrChangePolicyAction(types.policyActions.AUTH, {lowerBound: 0 ,upperBound:0, threshold:0, groups:[]});
    this.addOrChangePolicyActions(options.policyActions);
}

Group.prototype.toJSON = function () {
    return {
        name: this.name,
        displayName: this.displayName,
        policyActions: this.policyActions,
        policyList: this.policyList
    }
};

Group.prototype.getDisplayName = function () {
    return this.displayName || this.name;
};

Group.prototype.toString = function () {
    return JSON.stringify(this.toJSON());
};


Group.prototype.getPolicy = function (type) {
    return _.find(this.policyActions, {type: type})
};

Group.prototype.getCredit = function () {
    var creditPolicy = _.find(this.policyActions, {type: types.policyActions.CREDIT});
    if (creditPolicy && creditPolicy.vars.credit) {
            return creditPolicy.vars.credit;
    } else {
        return 0
    }
};

Group.prototype.decide = function (action, vars, cb) {
    var policyActions = this.policyActions;
    log.debug('Deciding for ' , this.getDisplayName());
    
    var jsList = JSON.parse(this.policyList);
    log.debug('PolicyList', this.policyList);
    var policyGraph = f.buildGraph(jsList, policyActions,action,vars);

    f.executeDecisionObject([action],policyGraph,function(err, decision) {
        if (err) return cb(null, f.decisionFalse);
        if (decision) return  cb(null, decision);
        cb(null, t.decisionFalse);
    });
};

/**
 * @param type {String} PolicyActionType
 * @param vars {Object} Object with the correct properties of PolicyActionTypesVar
 * @param setDefault {boolean} If true, default variables will be set for the actions
 * @return {*}
 */
Group.prototype.addOrChangePolicyAction = function (type, vars, setDefault) {
    log.trace("ChangeOrAddPolicyAction ", this.policyActions.length, type, vars);
    vars = vars || {};
    if (typeof vars.groups == 'string') vars.groups = [vars.groups];
    log.trace("New Action", newAction);
    var old = _.find(this.policyActions, function (o) {
        return o.type == type
    });

    if (old) {
        _.forEach(old.vars, function (value, key, oldVars) {
            if (vars[key]) {
                oldVars[key] = vars[key];
            }
        })
    } else {
        var newAction = new PolicyAction(type, vars, setDefault);
        this.policyActions.push(newAction)
    }

    // _.remove(this.policyActions, function (a) {
    //     return a.type == type;
    // });

    //this.policyActions.push(newAction);
    return true;
};

/**
 * @param actions {[{type: String, vars: Array}]}
 */
Group.prototype.addOrChangePolicyActions = function (actions) {
    log.trace("New/Change PolicyActions", actions);
    var that = this;
    _.forEach(actions, function (o) {
        that.addOrChangePolicyAction(o.type, o.vars);
    });
};

Group.prototype.removePolicyAction = function (action) {
    return _.remove(this.policyActions, function (o) {
        return o.type == action
    })
};


function hasRequiredVars(type, vars) {
    vars = vars || {};
    if (!_.has(con.policyActions, type)) {
        log.debug("not true");
        return false
    }
    return (_.every(con.policyActionsVars[type], _.partial(_.has, vars)));
}

module.exports = Group;