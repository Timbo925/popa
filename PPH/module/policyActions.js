var _ = require('lodash');
var con = require('./constants');
var log = require('log4js').getLogger("PolicyAction");

function PolicyAction(type, vars, setDefault) {
    if (!hasType(type)) {throw new Error("Non existing type")}
    //if (!setDefault && !hasRequiredVars(type, vars)) {throw new Error("Made with not the right vars")}
    this.type = type;
    this.vars = vars;

    if (setDefault) {
        this.vars = {};
        for (var i = 0; i<con.policyActionsVars[type].length; i++) {
            this.vars[con.policyActionsVars[type][i]] = con.policyActionsDefaults[type][i];
        }
    }
}

function hasRequiredVars(type, vars)  {
    vars = vars || [];
    if (!_.has(con.policyActions, type)) {log.debug("not true"); return false}
    return (_.every(con.policyActionsVars[type], _.partial(_.has, vars)));
}

function hasType(type) {
    return (_.has(con.policyActions, type))
}

PolicyAction.prototype.toString = function () {
  return JSON.stringify(this.toJSON(), null, 4)
};

PolicyAction.prototype.toJSON = function () {
    return {
        type: this.type,
        vars: this.vars
    }
};

module.exports = PolicyAction;