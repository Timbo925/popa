var express = require('express'),
    config = require('./config/config'),
    glob = require('glob'),
    mongoose = require('mongoose'),
    http = require('http'),
    https = require('https'),
    fs = require('fs'),
    log = require('log4js').getLogger("App");

mongoose.connect(config.db);
mongoose.set('debug', true);
var db = mongoose.connection;
db.on('error', function () {
    throw new Error('unable to connect to database at ' + config.db);
});

//var models = glob.sync(config.root + '/app/models/*.js');

// models.forEach(function (model) {
//   require(model);
// });

var app = express();

require('./config/express')(app, db, config);

var key = fs.readFileSync('key.pem', 'utf8');
var cert = fs.readFileSync('cert.pem', 'utf8');
var credentials = {key: key, cert: cert, passphrase: 'test'};

var httpServer = http.createServer(app);
//var httpsServer = https.createServer(credentials, app);

httpServer.listen(3000, function () {
    console.log('Runnin HTTP on 3000')
});

// httpsServer.listen(3000, function() {
//     console.log('Running HTTPS on 3000')
// });

