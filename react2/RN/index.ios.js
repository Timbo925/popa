/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

require('./shim');
import React, {Component} from 'react';
import {AppRegistry, StyleSheet, Text, View, Alert} from 'react-native';
import {Scene, Router, Modal} from 'react-native-router-flux'
import { createStore , combineReducers} from 'redux';
import { Provider } from 'react-redux';
import walletReducers from './app/reducers/wallet';
import * as storage from 'redux-storage'


//var walletReducers = require('./app/reducers/wallet');
import  WalletHome from './app/components/AppHome'

//Alert.alert('sync random bytes', require('crypto').randomBytes(32).toString('hex'));

let store = createStore(combineReducers({
    walletReducers: walletReducers}));

class App extends Component {
    render() {
        return (
            <WalletHome/>
        );
    }
}

class MyApp extends Component {
    render () {
        return (
            <Provider store={store}>
                <WalletHome/>
            </Provider>
        )
    }
}

AppRegistry.registerComponent('RN', () => MyApp);
