import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import BarcodeScanner from 'react-native-barcodescanner';
import {Actions} from 'react-native-router-flux';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as RActions from '../actions'
function mapStateToProps(state) {return {
    wallet:state.walletReducers.wallet}}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(RActions, dispatch)
}

class BarcodeScannerApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            torchMode: 'off',
            cameraType: 'back'
        };
        this.barcodeReceived = this.barcodeReceived.bind(this);

    }

    barcodeReceived(e) {
        console.log('Barcode: ' + e.data);
        console.log('Type: ' + e.type);
        Actions.pop();
        this.props.scanQr(e, this.props.wallet);
    }
    

    render() {
        return (
            <View style={styles.container}>
                <BarcodeScanner
                    onBarCodeRead={this.barcodeReceived}
                    style={{ flex: 1 }}
                    torchMode={this.state.torchMode}
                    cameraType={this.state.cameraType} />
            </View>
        );
    }
}
var styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: 'red'}});

export default connect(mapStateToProps, mapDispatchToProps)(BarcodeScannerApp)

//module.exports = BarcodeScannerApp;