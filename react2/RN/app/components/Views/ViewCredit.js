'use strict';
import React, {Component} from 'react';
import {View, StyleSheet, Text, TextInput} from 'react-native';
import ViewContainer from './ViewContainer'
import * as t from '../../module/constants';
import Wallet from '../../module/accountMSClient'  ;
import  QRCode from 'react-native-qrcode';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as RActions from '../../actions'
import NotBar from './NotBar'
import Button from 'react-native-button'


function mapStateToProps(state) {return {
    wallet:state.walletReducers.wallet,
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(RActions, dispatch)
}

class SendFunds extends Component {
    constructor(props) {
        super(props);
        this.askCredit = this.askCredit.bind(this);
        this.state= {
            amount: '0.01'
        }
    }

    askCredit() {
        this.props.askCredit(this.props.wallet, this.state.amount)
    }

    render() {

        return (
            <ViewContainer style={style.containerCol}>
                <NotBar/>
                <TextInput
                    keyboardType="numeric"
                    style={style.txInput}
                    onChangeText={(amount) => this.setState({amount})}
                    value={this.state.amount}/>
                <View style={{flex:1}}/>
                <Button onPress={this.askCredit} containerStyle={style.btnBigCont} style={style.btnBig}> ASK CREDIT </Button>
            </ViewContainer>
        );
    }
}

const style = StyleSheet.create({
    infoText: {
        color:'white',
        textAlign: 'center',
        fontSize: 16,
        fontWeight: 'bold'
    },
    infobar: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        alignSelf: 'stretch',
        backgroundColor: 'orange'
    },
    btnBigCont: {
        padding: 20,
        alignSelf: 'stretch',
        backgroundColor: 'lightgrey'
    },
    btnBig: {
        fontSize:20,
        fontWeight: 'bold',
        color: 'black'
    },
    containerCol: {
        marginTop: 50,
        alignItems: 'center'
    },
    txInput: {
        paddingLeft: 10,
        paddingRight: 10,
        alignSelf: 'stretch',
        height: 40,
        borderColor: 'gray',
        borderWidth: 1
    },
    text: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(SendFunds)
//module.exports = ReceiveFundsView;