'use strict';
import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import ViewContainer from './ViewContainer'
var Wallet = require('../../module/accountMSClient');
import * as t from '../../module/constants';
var QRCode = require('react-native-qrcode');


import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as RActions from '../../actions'
function mapStateToProps(state) {return {
    wallet:state.walletReducers.wallet}
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(RActions, dispatch)
}

class ReceiveFundsView extends Component {
    constructor(props) {
        super(props);
        console.log(this.props.wallet);
        this.state = {
            loading: true,
            address: ''
        }
    }

    componentDidMount() {
        var w = new Wallet(this.props.wallet);
        var that = this;
        w.sendMessageToAllEncrypted({msgType: t.messageTypes.MSW_GETCURRENTADDRESS}, {}, function(err, data) {
            console.log('Address', data)
            that.setState({address: data.uri, loading: false});
        })
    }

    render() {
        var r;
        if (this.state.loading) {
            r = (<Text> {this.props.wallet.mnemonic}</Text>)
        } else {
            r =  (<QRCode
                value={this.state.address}
                size={200}
                bgColor='black'
                fgColor='white'
            />)
        }
        return (
            <ViewContainer style={{marginTop:70, alignItems: 'center'}}>
                <Text>Receiving Address</Text>
                {r}
            </ViewContainer>
        );
    }
}

var styles = StyleSheet.create({

})

export default connect(mapStateToProps, mapDispatchToProps)(ReceiveFundsView)
//module.exports = ReceiveFundsView;