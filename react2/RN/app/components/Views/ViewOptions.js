'use strict';
import React, {Component} from 'react';
import {View, StyleSheet, Text, ListView, Clipboard}   from 'react-native';
import Button from 'react-native-button'
import ScrollableTabView from 'react-native-scrollable-tab-view';
import WalletHome from '../AppHome'
import ViewContainer from './ViewContainer'
import StatusBar from '../StatusBar';
import Wallet from '../../module/accountMSClient';
import t from '../../module/constants';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as RActions from '../../actions'
function mapStateToProps(state) {
    return {
        wallet: state.walletReducers.wallet
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(RActions, dispatch)
}

class ViewNotifications extends Component {
    constructor(props) {
        super(props);
        var w = new Wallet(props.wallet);
        console.log('wallet', w);
        this.state = {
            xpubKeyMsg: w.getMessage("ACCOUNT_REG_CLIENT_MSC_HDPRIVGET"),
            wordList: w._mnemonic.toString(),
            commKey: w.getAuthPub()
        }
    }

    copy(toCopy) {
        Clipboard.setString(JSON.stringify(toCopy));
    }

    reset() {

    }

    render() {
        return (
            <ViewContainer style={{marginTop:50}}>
                <Text style={[style.margin, {fontWeight:'bold', marginTop:20}]}>Communication Key:   </Text>
                <Text style={[style.margin]}>{JSON.stringify(this.state.commKey)}</Text>
                <Button style={style.padding} onPress={() => this.copy(this.state.commKey)}>COPY</Button>
                <View style={style.devider}></View>
                <Text style={[style.margin, {fontWeight:'bold', marginTop:20}]}>Register Private Key with other device:   </Text>
                <Text style={[style.margin]}>{JSON.stringify(this.state.xpubKeyMsg)}</Text>
                <Button style={style.padding} onPress={() => this.copy(this.state.xpubKeyMsg)}>COPY</Button>
                <View style={style.devider}></View>
                <Text style={[style.margin, {fontWeight:'bold', marginTop:20}]}>Backup Word List:   </Text>
                <Text style={[style.margin]}>{JSON.stringify(this.state.wordList)}</Text>
                <Button style={style.padding} onPress={() => this.copy(this.state.wordList)}>COPY</Button>
                <View style={style.devider}></View>
                <View style={{flex:1}}></View>
                <Button style={[{marginTop:20, marginBottom:20},{color: '#F44336'}]} onPress={() => this.reset()}>RESET WALLET</Button>
                <View style={style.devider}></View>

            </ViewContainer>
        );
    }
}

const style = StyleSheet.create({
    margin: {
      marginLeft: 20,
        marginRight:20
    },
    devider: {
        backgroundColor: '#EFEFF2',
        height: 1
    },
    padding: {
        padding: 10
    },
    box: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        height: 60
    },
    boxAction: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'red',
        paddingLeft: 15,
        paddingRight: 15
    },
    boxActionText: {
        fontSize: 12,
        color: 'white',
        fontWeight: 'bold'
    },
    btnBigCont: {
        padding: 20,
        alignSelf: 'stretch',
        backgroundColor: 'lightgrey'
    },
    btnBig: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black'
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(ViewNotifications);