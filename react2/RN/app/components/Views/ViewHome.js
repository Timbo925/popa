'use strict';
import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import ViewContainer from './ViewContainer';
import Button from 'react-native-button'
import {Actions} from 'react-native-router-flux';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as RActions from '../../actions'
function mapStateToProps(state) {
    return {
        balance: state.walletReducers.balance,
        wallet: state.walletReducers.wallet,
        credit: state.walletReducers.credit
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(RActions, dispatch)
}

class Home extends Component {
    constructor(props) {
        super(props);
    }

    update() {
        this.props.updateBalance(this.props.wallet);
        this.props.updateCredit(this.props.wallet);
    }

    render() {
        return (
            <ViewContainer style={{marginTop:50}}>
                <ViewContainer style={[{flex:0, backgroundColor: '#EFEFF2',paddingTop: 20, paddingBottom:20, borderBottomWidth: 0.5, borderBottomColor: '#828287'}]}>
                    <View style={styles.row}>
                        <Text style={styles.txt}>Balance: </Text>
                        <Text style={styles.txt}>{this.props.balance/1e8}</Text>
                        <Text style={styles.txt}> BTC</Text>
                    </View>
                    <View style={[styles.row]}>
                        <Text style={styles.txt}>Device Credit: </Text>
                        <Text style={styles.txt}>{this.props.credit/1e8}</Text>
                        <Text style={styles.txt}> BTC</Text>
                    </View>
                </ViewContainer>

                <Button style={[styles.btn, {marginTop:20}]} onPress={Actions.send}>Send Funds</Button>
                <Button style={styles.btn} onPress={Actions.receive}>Receive Funds</Button>
                <Button style={styles.btn} onPress={Actions.credit}>Request Credit</Button>
                <Button style={styles.btn} onPress={Actions.notifications}>Notifications</Button>
                <Button style={styles.btn} onPress={Actions.scan}>Scan</Button>
                <Button style={styles.btn} onPress={Actions.options}>Options</Button>
                <Button style={styles.btn} onPress={() => this.update()}>Refresh</Button>


            </ViewContainer>
        );
    }
}

var styles = StyleSheet.create({
    txt: {
        color:'black',
        fontSize: 16
    },
    btn: {
        margin: 10
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center'
    }

});

export default connect(mapStateToProps, mapDispatchToProps)(Home)
