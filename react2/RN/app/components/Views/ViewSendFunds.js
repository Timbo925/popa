'use strict';
import React, {Component} from 'react';
import {View, StyleSheet, Text, TextInput} from 'react-native';
import ViewContainer from './ViewContainer'
import Button from 'react-native-button'
import * as t from '../../module/constants';
import Wallet from '../../module/accountMSClient'  ;
import  QRCode from 'react-native-qrcode';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as RActions from '../../actions'

function mapStateToProps(state) {
    return {
        wallet: state.walletReducers.wallet,
        status: state.walletReducers.status.send
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(RActions, dispatch)
}

class SendFunds extends Component {
    constructor(props) {
        super(props);
        this.sendView = this.sendView.bind(this);
        this.state = {
            address: '2MxKz9g5NV8jVa1dGbhUEVCyUivUA1Ud7AN',
            amount: '0.01',
            amountCC: 'BTC'
        };
        this.props.status.type = '';
    }

    sendView() {
        this.props.send(
            this.props.wallet,
            this.state.address,
            this.state.amount,
            this.state.amountCC
        )
    }

    render() {
        var infobar;
        if (this.props.status.type == 'SENDING') {
            infobar = (
                <View style={style.infobar}>
                    <Text style={style.infoText}>{this.props.status.msg}</Text>
                 </View>)
        } else if (this.props.status.type == "ERROR") {
            infobar = (<View style={[style.infobar, {backgroundColor:'orangered'}]}>
                <Text style={style.infoText}>{this.props.status.msg}</Text>
            </View>)
        } else if (this.props.status.type == "SUCCESS") {
            infobar = (
                <View style={[style.infobar, {backgroundColor:'forestgreen'}]}>
                    <Text style={[style.infoText]}>{this.props.status.msg}</Text>
                </View>
            )

        }

        return (
            <ViewContainer style={style.containerCol}>
                {infobar}
                <TextInput
                    style={style.txInput}
                    onChangeText={(address) => this.setState({address})}
                    value={this.state.address}/>
                <TextInput
                    keyboardType="numeric"
                    style={style.txInput}
                    onChangeText={(amount) => this.setState({amount})}
                    value={this.state.amount}/>
                <View style={{flex: 1}}/>
                <Button onPress={this.sendView} containerStyle={style.btnBigCont} style={style.btnBig}> SEND </Button>
            </ViewContainer>
        );
    }
}

const style = StyleSheet.create({
    infoText: {
        color:'white',
        textAlign: 'center',
        fontSize: 16,
        fontWeight: 'bold'
    },
    infobar: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        alignSelf: 'stretch',
        backgroundColor: 'orange'
    },
    btnBigCont: {
        padding: 20,
        alignSelf: 'stretch',
        backgroundColor: 'lightgrey'
    },
    btnBig: {
        fontSize:20,
        fontWeight: 'bold',
        color: 'black'
    },
    containerCol: {
        marginTop: 50,
        alignItems: 'center'
    },
    txInput: {
        paddingLeft: 10,
        paddingRight: 10,
        alignSelf: 'stretch',
        height: 40,
        borderColor: 'gray',
        borderWidth: 1
    },
    text: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(SendFunds)
//module.exports = ReceiveFundsView;