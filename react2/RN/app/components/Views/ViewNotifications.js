'use strict';
import React, {Component} from 'react';
import {View, StyleSheet, Text, ListView} from 'react-native';
import Button from 'react-native-button'
import ScrollableTabView from 'react-native-scrollable-tab-view';
import WalletHome from '../AppHome'
import ViewContainer from './ViewContainer'
import StatusBar from '../StatusBar';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as RActions from '../../actions'
function mapStateToProps(state) {
    return {
        notifications: state.walletReducers.notifications,
        wallet: state.walletReducers.wallet
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(RActions, dispatch)
}

class ViewNotifications extends Component {
    constructor(props) {
        super(props);
        this.updateNot = this.updateNot.bind(this);
        //this.deny = this.deny().bind(this);
        this.approve = this.approve.bind(this);
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        console.log('Porps', props);
        this.state = {
            dataSource: ds.cloneWithRows(props.notifications)
        }
    }

    updateNot() {
        this.props.updateNotifications(this.props.wallet)
    }

    componentWillReceiveProps(nextProps) {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.setState({
            dataSource: ds.cloneWithRows(nextProps.notifications)
        });
    }

    deny(not) {
        this.props.rejectNotification(this.props.wallet, not);
    }

    approve(not) {
        this.props.approveNotification(this.props.wallet, not);
    }


    render() {
        return (
            <ViewContainer style={{marginTop:50}}>
                <ListView
                    enableEmptySections={true}
                    dataSource={this.state.dataSource}
                    renderSeparator={(sectionId, rowId) => (
                        <View key={rowId} style={{backgroundColor: '#8E8E8E', height:1}}></View>
                    )}
                    renderRow={(rowData) => (
                        <View style={style.box}>
                            <View style={[style.boxAction, {flex:1, backgroundColor: 'lightgrey'}]}>
                                <Text style={[{color: 'black', fontSize: 14}]}>{rowData.data.msg}</Text>
                            </View>
                            <View style={style.boxAction}>
                                <Text onPress={() => this.deny(rowData)} style={style.boxActionText}>DENY</Text>
                            </View>
                            <View style={[style.boxAction, {backgroundColor: 'green'}]}>
                                <Text onPress={() => this.approve(rowData)} style={style.boxActionText}>APPROVE</Text>
                            </View>
                        </View>
                    )}
                />
                <Button onPress={this.updateNot} containerStyle={style.btnBigCont} style={style.btnBig}> Refresh </Button>
            </ViewContainer>
        );
    }
}

const style = StyleSheet.create({
    box: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        height: 60
    },
    boxAction: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'red',
        paddingLeft: 15,
        paddingRight: 15
    },
    boxActionText: {
        fontSize: 12,
        color: 'white',
        fontWeight: 'bold'
    },
    btnBigCont: {
        padding: 20,
        alignSelf: 'stretch',
        backgroundColor: 'lightgrey'
    },
    btnBig: {
        fontSize:20,
        fontWeight: 'bold',
        color: 'black'
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(ViewNotifications);