'use strict';
import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import ViewContainer from './ViewContainer'
import * as t from '../../module/constants';
import Wallet from '../../module/accountMSClient'  ;
import  QRCode from 'react-native-qrcode';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as RActions from '../../actions'

function mapStateToProps(state) {return {
    status:state.walletReducers.notbar}
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(RActions, dispatch)
}

class NotBAR extends Component {
    constructor(props) {
        super(props);
        console.log('notbar', this.props.status);
    }

    render() {
        var infobar;
        if (this.props.status.type == 'WARNING') {
            infobar = (
                <View style={style.infobar}>
                    <Text style={style.infoText}>{this.props.status.msg.toString()}</Text>
                </View>)
        } else if (this.props.status.type == "ERROR") {
            infobar = (<View style={[style.infobar, {backgroundColor:'orangered'}]}>
                <Text style={style.infoText}>{this.props.status.msg.toString()}</Text>
            </View>)
        } else if (this.props.status.type == "SUCCESS") {
            infobar = (
                <View style={[style.infobar, {backgroundColor:'forestgreen'}]}>
                    <Text style={[style.infoText]}>{this.props.status.msg.toString()}</Text>
                </View>
            )
        } else {
            infobar = <View />
        }
        return (
            <View style={{alignSelf: 'stretch'}}>
                {infobar}
            </View>
        );
    }
}

const style = StyleSheet.create({
    infoText: {
        color:'white',
        textAlign: 'center',
        fontSize: 16,
        fontWeight: 'bold'
    },
    infobar: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        alignSelf: 'stretch',
        backgroundColor: 'orange'
    },
    btnBigCont: {
        padding: 20,
        alignSelf: 'stretch',
        backgroundColor: 'lightgrey'
    },
    btnBig: {
        fontSize:20,
        fontWeight: 'bold',
        color: 'black'
    },
    containerCol: {
        marginTop: 50,
        alignItems: 'center'
    },
    txInput: {
        paddingLeft: 10,
        paddingRight: 10,
        alignSelf: 'stretch',
        height: 40,
        borderColor: 'gray',
        borderWidth: 1
    },
    text: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(NotBAR)
//module.exports = ReceiveFundsView;