'use strict';
import React, { Component } from 'react';
import {View, StyleSheet, Text} from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import WalletHome from './AppHome'
import ViewContainer from './Views/ViewContainer'

class StatusBar extends Component {

    render() {
        return (
            <View style={style.sb}></View>
        );
    }
}

const style = StyleSheet.create({
    sb: {
        height: 20,
        backgroundColor: 'black'
    }
});

module.exports = StatusBar;