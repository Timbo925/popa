'use strict';
require('../../shim');
import React, { Component } from 'react';
import {View, StyleSheet, Text} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions/index';
import ViewContainer from '../components/Views/ViewContainer'
import AppRouter from './AppRouter'
import Scan from './qrCodeScanner'


function mapStateToProps(state) {
    return {balance:state.walletReducers.balance}
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch)
}

// var PushNotification = require('react-native-push-notification');
//
// PushNotification.configure({
//     // (optional) Called when Token is generated (iOS and Android)
//     onRegister: function(token) {
//         console.log( 'TOKEN:', token );
//     },
//
//     // (required) Called when a remote or local notification is opened or received
//     onNotification: function(notification) {
//         console.log( 'NOTIFICATION:', notification );
//     },
//
//     senderID: "YOUR GCM SENDER ID", // ANDROID ONLY: (optional) GCM Sender ID.
//     permissions: {alert: true, badge: true, sound: true},
//     popInitialNotification: true,
//     requestPermissions: true,
// });

class WalletHome extends Component {
    render() {
        return (
            <ViewContainer>
                <AppRouter/>
            </ViewContainer>
        )
    }
}

//export default connect(mapStateToProps, mapDispatchToProps)(WalletHome)
export default WalletHome