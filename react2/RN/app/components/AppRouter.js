import React, {Component} from 'react';
import {AppRegistry, StyleSheet, Text, View, Alert} from 'react-native';
import {Scene, Router, Modal} from 'react-native-router-flux'
import Home from './Views/ViewHome'
import Settings from './Views/ViewSettings'
import Credit from './Views/ViewCredit'
import Notification from './Views/ViewNotifications'
import Options from './Views/ViewOptions'
import SendFunds from './Views/ViewSendFunds'
import ReceiveFunds from './Views/ViewReceiveFunds'
import Scan from './qrCodeScanner'




//Alert.alert('sync random bytes', require('crypto').randomBytes(32).toString('hex'));

class AppRouter extends Component {

    render() {
        return (
            <Router>
                <Scene key="modal" component={Modal}>
                    <Scene key="root" hideNavBar={false}>
                        <Scene key="home" component={Home} initial={true} title="Personal Wallet"/>
                        <Scene key="settings" component={Settings} title="Settings"/>
                        <Scene key="credit" component={Credit} title="Request Credit"/>
                        <Scene key="notifications" component={Notification} title="Notifications"/>
                        <Scene key="options" component={Options} title="Options"/>
                        <Scene key="send" component={SendFunds} title="Send Transaction"/>
                        <Scene key="receive" component={ReceiveFunds} title="Receive"/>
                        <Scene key="scan" component={Scan} title="Scan"/>
                    </Scene>
                </Scene>
            </Router>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

//export default connect(mapStateToProps, mapDispatchToProps)(AppRouter)

module.exports = AppRouter;