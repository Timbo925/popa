import * as a from '../actions/index'
import _ from 'lodash';
//var _ = require('lodash');
require('../../shim');
var Wallet = require('../module/accountMSClient');
import createEngine from 'redux-storage-engine-reactnativeasyncstorage';
const engine = createEngine('WalletStore');

let cloneObject = function(obj){
    return JSON.parse(JSON.stringify(obj))
};

let initialState = {
    balance: 0,
    credit: 0,
    notifications: [],
    wallet: new Wallet().toJSON(),
    notbar: {
        msg:'',
        type:''
    },
    status: {
        send: {msg: '', type: ''}
    }
};

let cleanState = {};

//!! Object reference need to change, so we need to clone !!!

export default function (state = initialState, action = {}) {
    switch(action.type) {
        case 'REDUX_STORAGE_LOAD':
            return Object.assign({},initialState,action.payload.walletReducers);

        case a.INC_BALANCE:
            var newState = cloneObject(state);
            newState.balance = state.balance + action.value;
            engine.save(newState);
            return newState;

        case a.SCAN_QR_S:
            return state;
        case a.SCAN_QR_R:
            return state;

        case a.SEND_START:
            var newState = cloneObject(state);
            newState.status.send.msg = 'Sending';
            newState.status.send.type = 'SENDING';
            return newState;

        case a.SEND_DONE:
            var newState = cloneObject(state);

            newState.status.send.msg = action.msg.decision.msg;
            if (action.msg.decicion == 'NO' && !action.msg.txid) {
                newState.status.send.type = 'ERROR';
            } else if (action.msg.txid) {
                newState.status.send.msg = 'Successful transaction';
                newState.status.send.type = 'SUCCESS';
            }
            newState.wallet = action.wallet;
            engine.save(newState);
            return newState;

        case a.SEND_ERROR:
            var newState = cloneObject(state);
            newState.status.send.msg = action.msg;
            newState.status.send.type = 'ERROR';
            return newState;

        case a.UPDATE_WALLET:
            var newState = cloneObject(state);
            newState.wallet = action.wallet;
            engine.save(newState);
            return newState;


        case a.CREDIT_START:
            var newState = cloneObject(state);
            newState.notbar.msg = 'Requesting';
            newState.notbar.type = 'WARNING';
            return newState;

        case a.CREDIT_ERROR:
            var newState = cloneObject(state);
            newState.notbar.msg = action.msg;
            newState.notbar.type = 'ERROR';
            return newState;

        case a.CREDIT_DONE:
            var newState = cloneObject(state);
            newState.notbar.msg = action.msg;
            newState.notbar.type = 'SUCCESS';
            return newState;

        case a.UPDATE_BALANCE:
            var newState = cloneObject(state);
            newState.balance = action.amount;
            return newState;

        case a.NOT_D:
            var newState = cloneObject(state);
            newState.notifications = action.notifications;
            return newState;

        case a.UPDATE_CREDIT:
            var newState = cloneObject(state);
            newState.credit = action.credit;
            return newState;
        
        default:
            return state || newState;
    }
}
