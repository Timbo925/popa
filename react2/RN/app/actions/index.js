import * as t from '../module/constants';

export const UPDATE_BALANCE = 'UPDATE_BALANCE';
export const UPDATE_CREDIT = 'UPDATE_CREDIT';
export const SCAN_QR_S = 'SCAN_QR_S';
export const SCAN_QR_E = 'SCAN_QR_E';
export const SCAN_QR_R = 'SCAN_QR_R';
export const UPDATE_WALLET = 'UPDATE_WALLET';
export const SEND_START = 'SEND_START';
export const SEND_ERROR = 'SEND_ERROR';
export const SEND_DONE = 'SEND_DONE';
export const CREDIT_START = 'CREDIT_START';
export const CREDIT_DONE = 'CREDIT_DONE';
export const CREDIT_ERROR = 'CREDIT_ERROR';
export const NOT_S = 'NOT_S';
export const NOT_D = 'NOT_D';
export const NOT_E = 'NOT_E';
export const NOT_APPROVE = 'NOT_APPROVE';
export const NOT_REJECT = 'NOT_REJECT';

export const scanQr = (data, wallet) => (dispatch) => _processQr(dispatch, data, wallet);
export const incBalance = (value) => (dispatch) => _incBalance(dispatch, value);
export const send = (wallet, address, amount, currency) => (dispatch) => _send(dispatch, wallet, address,amount,currency);
export const askCredit = (wallet, amount) => (dispatch) => _askCredit(dispatch, wallet, amount);
export const updateBalance = (wallet) => (dispatch) => _getBalance(dispatch, wallet);
export const updateCredit = (wallet) => (dispatch) => _getCredit(dispatch, wallet);
export const updateNotifications = (wallet) => (dispatch) => _updateNotifications(dispatch, wallet);
export const approveNotification = (wallet, notification) => (dispatch) => _approveNotification(dispatch, wallet, notification);
export const rejectNotification = (wallet, notification) => (dispatch) => _rejectNotification(dispatch, wallet, notification);


const _approveNotification = (dispatch, wallet, notification) => {
    dispatch(_notificationsUpdateStart(notification))
    var msg = {
        msgType: t.messageTypes.N_UPDATE,
        msgData: {
            id: notification.id,
            action: 'ACK'
        }
    };
    _answerNotification(dispatch, wallet, notification, msg)

};

const _rejectNotification = (dispatch, wallet, notification) => {
    dispatch(_notificationsUpdateStart(notification))
    var msg = {
        msgType: t.messageTypes.N_UPDATE,
        msgData: {
            id: notification.id,
            action: 'NACK'
        }
    };
    _answerNotification(dispatch, wallet, notification, msg)

};

const _answerNotification = (dispatch, wallet, notification, msg) => {
    var Wallet = require('../module/accountMSClient');
    var w = new Wallet(wallet);
    w.sendMessageToAllEncrypted(msg, {}, function(err, resp) {
        if (err) return dispatch(_notificationsUpdateError(err));
        console.log("Answered Notification", resp);
        _updateNotifications(dispatch, wallet);
    })
};

const _getBalance = (dispatch,wallet) =>  {
    var Wallet = require('../module/accountMSClient');
    var w = new Wallet(wallet);
    var msg = {msgType: t.messageTypes.MSW_GETBALANCE};
    w.sendMessageToAllEncrypted(msg, {testnet: true}, function(err, resp) {
        if (err) return dispatch(_error(err));
        dispatch(_updateBalance(resp.satoshis))
    })
};

const _getCredit = (dispatch,wallet) =>  {
    var Wallet = require('../module/accountMSClient');
    var w = new Wallet(wallet);
    var msg = {msgType: t.messageTypes.ACCOUNT_GETCREDIT};
    w.sendMessageToAllEncrypted(msg,{}, function(err, credit) {
        if (err) return dispatch(_error(err));
        dispatch(_updateCredit(credit));
    })
};

const  _updateNotifications = (dispatch, wallet) => {
    dispatch(_notificationsUpdateStart());
    var Wallet = require('../module/accountMSClient');
    var w = new Wallet(wallet);
    var msg = {msgType: 'ACCOUNT_GETNOT'}
    w.sendMessageToAllEncrypted(msg, {}, function(err, response) {
        if (err) {
            dispatch(_notificationsUpdateError(err));
            return null;
        } else {
            dispatch(_notificationsUpdateDone(response))
        }
    })
};

const _askCredit = (dispatch, wallet, amount) => {
    dispatch(_creditStart());
    var Wallet = require('../module/accountMSClient');
    var w = new Wallet(wallet);
    var msg = {
        msgType: t.messageTypes.MSW_CREDITREQUEST,
        msgData: {
            value: amount*1e8,
            value_CC: amount,
            CC: 'BTC'
        }
    };

    w.sendMessageToAllEncrypted(msg, {}, function(err, resp) {
        console.log('credit request::',msg,  err, resp);
        if (err) return dispatch(_creditError(err));
        dispatch(_creditDone(resp));
    })
};

const _send = (dispatch, wallet, address, amount, currency) => {
    dispatch(_sendStart());
    console.log(wallet, address,amount)
    //require('../../shim')
    var Wallet = require('../module/accountMSClient');
    var w = new Wallet(wallet);
    var request = {
        'msgType' : 'MSW_MAKETRANSACTION',
        'msgData' : {'uri' : 'bitcoin:' + address + '?amount=' + amount}
    }
    w.sendMessageToAllEncrypted(request,{}, function(err,tData) {
        console.log('transaction::', err, tData);
        var trans = w.signTransaction(tData)
        if (err) return dispatch(_sendError(err));
        var tRequest = {
            msgType: t.messageTypes.MSW_SUBMITTRANSACTION,
            msgData: trans
        };
        w.sendMessageToAllEncrypted(tRequest, {}, function(err, resp) {
            if (err) return dispatch(_sendError(err));
            dispatch(_sendDone(w.toJSON(), resp))
        })
    })
};

const  _processQr = (dispatch, data,wallet) => {
    var Wallet = require('../module/accountMSClient');

    console.log('ProcessQr', data, wallet);
    var msg = JSON.parse(data.data);
    console.log(msg);
    dispatch(_scanStarted(data));

    wallet = new Wallet(wallet);

    wallet.registerClient(msg.commData.senderPubkey, msg.commType, msg.commData);
    var retMsg = wallet.getMessage(t.messageTypes.ACCOUNT_REG_CLIENT);
    retMsg.msgData.challenge = msg.msgData.challenge; //Set the challenge from the MSW so he will accept
    retMsg.commType = "ANDROID";
    retMsg.commData = {senderPubkey: wallet.getAuthPub()};
    console.log('retMsg', retMsg);

    wallet.sendMessageEncrypted(msg.commData.senderPubkey, retMsg, {}, function (err, serverMsg) {
        if (err) return dispatch(_scanError(err));
        dispatch(_updateWallet(wallet.toJSON()))
    })

};


//Action Objects
const _error = (error) => ({type: 'ERROR', error});
const _incBalanceD = (value) => ({type: INC_BALANCE, value: value});
const _scanError = (data) => ({type: SCAN_QR_E});
const _scanStarted = (qrCode) => ({type: SCAN_QR_S, qrCode});
const _scanStoppedRegister = (qrCode, wallet) => ({type: SCAN_QR_R, qrCode, wallet});
const _updateWallet = (wallet) => ({type: UPDATE_WALLET, wallet});
const _sendDone = (wallet, msg) => ({type: SEND_DONE, wallet,msg});
const _sendStart = () => ({type: SEND_START});
const _sendError = (msg) => ({type: SEND_ERROR, msg});
const _creditDone = (msg) => ({type: CREDIT_DONE, msg});
const _creditError = (msg) => ({type: CREDIT_ERROR, msg});
const _creditStart = (msg) => ({type: CREDIT_START, msg});
const _updateBalance = (amount) => ({type: UPDATE_BALANCE, amount});
const _updateCredit = (credit) => ({type: UPDATE_CREDIT, credit});
const _notificationsUpdateStart = (info) => ({type: NOT_S, info});
const _notificationsUpdateDone = (notifications) => ({type: NOT_D, notifications});
const _notificationsUpdateError = (error) => ({type: NOT_E, error});