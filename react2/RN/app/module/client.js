var bitcore = require('bitcore-lib');
var enums = require("./constants");

function Client(options) {
    if (bitcore.PrivateKey.isValid(options.privateKey)) {
        this._privateKey = bitcore.PrivateKey(options.privateKey);
    } else {
        this._privateKey = "";
    }

    if (options.type &&!enums.deviceTypes[options.type]) throw new Error("Not a recognized device type:" + type);

    this.pubkey = options.publicKey || this._privateKey.publicKey;
    this.type = options.type || "SERVER";
    this.typeInfo = options.typeInfo || {}; //TODO check if typeInfo is correct
    //this.groups = options.groups = [];
}

Client.prototype.getPrivateKey = function () {
    return this._privateKey;
};

Client.prototype.getPublicKey = function () {
    return this.pubkey;
};

Client.prototype.toJSON = function () {
        return {
            privateKey: this._privateKey.toString(),
            publicKey: this.pubkey.toString(),
            type: this.type,
            typeInfo: this.typeInfo
        }
};

Client.prototype.toString = function () {
  return JSON.stringify(this.toJSON())
};

module.exports = Client;
