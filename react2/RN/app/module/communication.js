var bitcore = require('bitcore-lib');
var types = require('./constants');
var _ = require('lodash');
var uid = require('uid-safe');
var qs = require('querystring-es3')

exports.sendMessage = function (client, message, opt, cb) {
    console.log('opt', opt, qs.stringify(opt))

    opt = opt || {};
    var id = uid.sync(8);
    if (client.type == 'SERVER') {
        var url = client.typeInfo.url;
        if (!_.startsWith(url, 'http://')) {
            url = 'http://' + url
        }
        //url = _.replace(url, 'localhost', '192.168.1.25');
        url = _.replace(url, 'localhost', '10.0.0.3');
        url += '?' + qs.stringify(opt);
        console.log(id, 'Sending Message to client:', url);

        fetch(url, {
            method: 'POST',
            body: JSON.stringify(message),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                console.log('Received Message', response);
                return response
            })
            .then((response) => {
                console.log('Status', response.status);
                if (!response.ok) {
                    response
                        .json()
                        .then((data) => {cb(data)})
                } else {
                    response
                        .json()
                        .then((data) => {cb(null, data)})
                }
            })
            .catch((error) => {
                console.warn(error);
                cb(error)
            })
            .done()
    } else {
        console.log("Not SERVER");
        cb('not server');
    }
};

/**
 * Returns PubKey from the message Buffer
 * @param message {Buffer}
 * @return {String}
 */
exports.getPubKeyMessage = function (message) {
    return bitcore.PublicKey.fromDER(message.slice(0, 33)).toString();
};

//TODO parse could check if message is allowed...
/**
 *
 * @param accountManager {AccountManager}
 * @param policyManager {PolicyManager}
 * @param msgOriginal {{}}
 */
exports.parseMessage = function (accountManager, policyManager, msgOriginal) {

    //TODO check message is right format
    /*
     STEP 1
     Get the decrypted message based on the the
     */
    var msgBuffer = new Buffer(msgOriginal.data);
    var ret = {msgType: "PARSED_MESSAGE"};

    var msg = accountManager.decryptMessage(msgBuffer);
    //if (!msg) res.status(500).json({error: "Couldn't decrypt the message"});

    /*
     STEP 2
     Add Add standard info to the result
     */
    ret.senderPubkey = this.getPubKeyMessage(msgBuffer);
    ret.receivedMessage = msg;

    log.info("Parsing ", msg.msgType);

    /*
     STEP 3
     Parse the messages
     3.1 check if all required data from message is present
     3.2 (optional) ask for a decision to the policyManager
     3.2.1 Handle decision
     3.3 Parse the message
     */
    switch (msg.msgType) {
        case types.messageTypes.REG_NEW_CLIENT_R:
            // 1. Check if all required field are present
            if (!checkRequiredCommData(msg)) return {status: "failed"};
            if (!checkRequiredMsgData(msg)) return {status: "failed"};

            //TODO: 2. Check challenge OK
            //      3. Register client to admin account
            var account = accountManager.getAccountAdmin();
            var regClient = account.getMessenger().registerClient(ret.senderPubkey, msg.commType, msg.commData);
            console.log(ret.senderPubkey, "Added to AdminAccount");

            // 4. Register with Policy + Make Admin
            var resAddClient = policyManager.addClient(ret.senderPubkey);
            var resAddGroupRoot = policyManager.addGroupToClient(ret.senderPubkey, "ROOT");
            console.log(ret.senderPubkey, "Added to group: ROOT");
            console.log("RegClient:", regClient, "ResAddCleint", resAddClient, "ResAddGroupRoot", resAddGroupRoot)
            ret.status = "ok";
            break;

        case types.messageTypes.TEST_ROOT:
            //var groups = policyManager.getClient(ret.senderPubkey);
            //ret.groups = groups;
            //var allowed = policyManager.isAllowed(ret.senderPubkey, types.policyActions.SEND, {value: 1000})
            //ret.allowed = allowed;
            var decision = policyManager.decide(ret.senderPubkey, types.policyActions.ROOT, {});
            console.log("TEST_ROOT Decision", decision);
            break;

        case types.messageTypes.REG_CLIENT_ACCOUNT:
            if (!checkRequiredCommData(msg)) return {status: "failed"};
            if (!checkRequiredMsgData(msg)) return {status: "failed"};

            //Register with the account
            var account = accountManager.getAccountById(msg.commData.accountId);
            account.getMessenger().registerClient(ret.senderPubkey, msg.commType, msg.commData);

            //Register with policy
            policyManager.addClient(ret.senderPubkey)
            break;

        default:
            break;

    }
    return ret
};

function checkRequiredCommData(msg) {
    if (!_.every(types.commTypesData[msg.commType], function (o) {
            return _.has(msg, o)
        })) {
        console.log("Not all types present")
        return false;
        //return {status: "Failure", msg: msg, needs: types.commTypesData[msg.commType]};
    }
    console.log("All commData present");
    return true;
}
function checkRequiredMsgData(msg) {
    if (!_.every(types.messageData[msg.msgType], function (o) {
            return _.has(msg, o)
        })) {
        console.log("Not all MsgData present");
        return false;
        //return {status: "Failure", msg: msg, needs: types.commTypesData[msg.commType]};
    }
    console.log("All msgData present");
    return true;
}
