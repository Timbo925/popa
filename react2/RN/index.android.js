require('./shim');
import React, {Component} from 'react';
import {AppRegistry, StyleSheet, Text, View, Alert} from 'react-native';
import {Scene, Router, Modal} from 'react-native-router-flux'
import { createStore , combineReducers, applyMiddleware} from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk'
import createLogger from 'redux-logger'
import walletReducers from './app/reducers/wallet';
import  WalletHome from './app/components/AppHome'
import createEngine from 'redux-storage-engine-reactnativeasyncstorage';
import * as storage from 'redux-storage'

//Alert.alert('sync random bytes', require('crypto').randomBytes(32).toString('hex'));

var combReducers = combineReducers({walletReducers: walletReducers});
var reducer = storage.reducer(combReducers);
var logger = createLogger();

const engine = createEngine('WalletStore');

const middleware = storage.createMiddleware(engine);

var createStoreWithMiddleware = applyMiddleware(thunk,logger,middleware)(createStore);

let store = createStoreWithMiddleware(combReducers);

const load = storage.createLoader(engine);
load(store)
    .catch(() => console.log('Failed to load previous state'));

class MyApp extends Component {
    render () {
        return (
            <Provider store={store}>
                <WalletHome/>
            </Provider>
        )
    }
}

AppRegistry.registerComponent('RN', () => MyApp);
