var bitcore = require('bitcore-lib');
var Mnemonic = require('bitcore-mnemonic');
var log = require('log4js').getLogger("AccountMSW");
var uid = require('uid-safe');
var Account = require('./account');
var AccountMSC = require('./accountMSClient');
var t = require('./constants');
var _ = require('lodash');
var apiBtc = require('./apiBtcNetwork');


/**
 *
 * @param identifier
 * @constructor
 * @type{Account}
 */
function AccountMSWallet(identifier) {
    log.trace("AccountMSW Construct");
    Account.apply(this, [identifier]);
}

AccountMSWallet.prototype = new Account();
AccountMSWallet.prototype.constructor = AccountMSWallet;

/**
 * Init gets called by constructor Account
 * @param options
 */
AccountMSWallet.prototype.init = function (options) {
    Account.prototype.init.call(this, options);
    this.type = t.accountTypes.AccountMSW;

    this._client = options.client ? new AccountMSC(options.client) : new AccountMSC(this._hdPrivMaster.toString());

    this._treshold = options.treshold || 2;
    this._maxKeys = options.maxKeys || 2;
    this._index = options.index || 0;
    this._indexChange = options.indexChange || 0;
    this._xpubMasters = options.xpubMasters || [this._client.getXpubMaster()];
    this._challenge = options.challenge || '';

    this._xpubMasters = _.map(this._xpubMasters, function(x) {return new bitcore.HDPublicKey(x)});

};

//Override Functions
AccountMSWallet.prototype.toJSON =  function () {
    var json = Account.prototype.toJSON.call(this);
    json.index = this._index;
    json.indexChange = this._indexChange;
    json.treshold = this._treshold;
    json.maxKeys = this._maxKeys;
    json.client = this._client.toJSON();
    json.xpubMasters = this._xpubMasters;
    json.challenge = this._challenge;
    return json;
};

//Other Functions

/**
 * Adding a HDPublicKey if the maxKeys limit isn't reached for the MSW
 * @param hdXpub {HDPublicKey}
 */
AccountMSWallet.prototype.addXpubMasters = function (hdXpub) {
    if (bitcore.HDPublicKey(hdXpub)) {
        if (this._xpubMasters.length < this._maxKeys) {
            this._xpubMasters.push(new bitcore.HDPublicKey(hdXpub))
            return this;
        } else {
            throw "Max Keys reached for this wallet"
        }
    } else {
        throw "Not a valid xpub format"
    }
};


/**
 * Check if the challenge is given by the wallet
 * @param challenge {string}
 * @return {boolean}
 */
AccountMSWallet.prototype.isValidChallenge = function(challenge) {
    if (!challenge) return false;
    return (this._challenge == challenge)
};

AccountMSWallet.prototype.getMessage = function (type) {
    switch(type) {
        case t.messageTypes.ACCOUNT_REG_CLIENT:
            this._challenge = uid.sync(8);

            return {
                msgType: t.messageTypes.ACCOUNT_REG_CLIENT_MSW,
                msgData: {
                    accountId: this.id,
                    accountType: this.type,
                    senderPubkey: this.getAuthPub(),
                    challenge: this._challenge
                }
            };
        case t.messageTypes.ACCOUNT_REG_CLIENT_MSC:
            if (this._xpubMasters.length < this._maxKeys) {
                //All keys are present, newly registered device needs to get the Xpriv of other client
                return {msgType: 'SUCCESS'}
            } else {
                //Newly registered device needs to send over his Xpub to we can share. 
                return {msgType: t.messageTypes.ACCOUNT_REG_CLIENT_MSW_GETHDPRIV}
            }
            break;
        default:
            return Account.prototype.getMessage.call(this, type);
    }  
};

AccountMSWallet.prototype.getPubKeys = function (index, change) {
    var deriveIndex = change ? 1 : 0;
    var pubKeys = [];
    for (var i=0; i<this._xpubMasters.length; i++) {
        pubKeys.push(
            this._xpubMasters[i]
                .derive(deriveIndex)
                .derive(index)
                .publicKey)
    }
    return pubKeys;
};

AccountMSWallet.prototype.getAddressFromTo = function(from, to, change, testnet) {
  var list = [];
    for (var i = from; i<=to; i++ ) {
        list.push(this.getAddressIndex(i,change, testnet))
    }
    return list;
};


AccountMSWallet.prototype.getAddressCurrentIndex = function(change,testnet, next) {
    if (next) {change ? this._indexChange++ : this._index++}
    return this.getAddressIndex(change ? this._indexChange : this._index, change, testnet);
};

AccountMSWallet.prototype.getAddressNextIndex = function (change, testnet) {
    change ? this._indexChange++ : this._index++;
    return this.getAddressCurrentIndex(change, testnet);
};

AccountMSWallet.prototype.getAddressIndex = function(index,change,testnet) {
    var network = testnet ? 'testnet' : 'livenet';
    if (this._treshold <= this._xpubMasters.length) {
        var pubKeys = this.getPubKeys(index, change);
        //log.debug('pubKeys', pubKeys);
        return new bitcore.Address(pubKeys, this._treshold, network);
    } else {
        return null
    }
};

AccountMSWallet.prototype.getAllUsedAddresses = function(testnet) {
    var searchList = _.union(
        this.getAddressFromTo(0,this._index+1, false, testnet),
        this.getAddressFromTo(0,this._indexChange+1, true, testnet)
    );
    searchList = _.map(searchList, function (o) {
        return o.toString();
    });
    log.debug('Addresses to check', JSON.stringify(searchList));
    return searchList;
};

AccountMSWallet.prototype.getBalance = function (testnet, cb) {
    apiBtc.btcBalance(this.getAllUsedAddresses(testnet), function(err, satoshis) {
        if (err) return cb(err);
        cb(null, satoshis)
    })
};

AccountMSWallet.prototype.getPubKeysFromUtoxs = function (Utoxs, testnet) {
    var retObj = {};
    var that = this;
    var indexList = _.map(this.getAddressFromTo(0,this._index, false, testnet), toString);
    var changeList = _.map(this.getAddressFromTo(0,this._indexChange, true, testnet), toString);
    _.forEach(Utoxs, function(u) {
        var address =  u.address;
        var indexIndexList = indexList.indexOf(address);
        var indexChangeList = changeList.indexOf(address);
        if (indexIndexList> -1) {
            retObj[address] = {pubkeys: that.getPubKeys(indexIndexList, false), depth: 'm/0/' + indexIndexList}
        } else if (indexChangeList > -1) {
            retObj[address] = {pubkeys: that.getPubKeys(indexChangeList, true), depth: 'm/1/' + indexChangeList}
        }
    });
    return retObj;
};

function toString(s) {
    return s.toString()
}

function selectUtxos(utxos, targetBalance) {
    var sorted = _.sortBy(utxos, ['satoshis']);
    var selectedBalance = 0
    var toReturn = [];
    while (selectedBalance < targetBalance) {
        if (sorted.length == 0) return []; //Coudnt gatter enough outputs, select none
        var o = sorted.shift();
        selectedBalance += o.satoshis;
        toReturn.push(o);
    }
    return toReturn;
}

AccountMSWallet.prototype.makeTransaction = function (uriString, callback) {
    var that = this;

    //Check and build Bitcoin URI
    if (!bitcore.URI.isValid(uriString)) return callback('Not a valid URI' + uriString);
    var uri = new bitcore.URI(uriString);
    var testnet = (uri.address.network.toString() == 'testnet');
    log.debug('URI Make Transaction', uri);

    apiBtc.getUtxoSet(this.getAllUsedAddresses(testnet), function(err, utxosList) {
        if (err) return callback('Failed getting utxo set' + err);

        //Select UTXO
        var utxos = selectUtxos(utxosList, uri.amount);
        log.debug('#Utxos selected', utxos.length);
        if (utxos.length == 0) return callback(null, 'Not enough funds available');


        //Create Transaction
        var transaction = new bitcore.Transaction();
        transaction.change(that.getAddressNextIndex(true, testnet));
        transaction.to(uri.address, uri.amount);

        var infoList = that.getPubKeysFromUtoxs(utxos, testnet);
        var depthList = [];
        _.forEach(utxos, function(u) {
            transaction.from(u, infoList[u.address].pubkeys, 2);
            depthList.push(infoList[u.address].depth)
        });

        transaction.fee(transaction.getFee());

        log.debug('Transaction\n', transaction);
        callback(null, null,{
            depthList: depthList,
            satoshis: uri.amount, //value in satoshis
            transaction: transaction
        })

    })
};

/**
 * Sign a transaction in the tData format
 * @param tData {{transaction: String, depthList: [String]}
 * @return tData {Object}
 */
AccountMSWallet.prototype.signTransaction = function (tData) {
    log.debug('Signing:', JSON.stringify(tData));
    var that = this;
    var transaction = new bitcore.Transaction(tData.transaction);
    var depthList = tData.depthList || [];
    _.forEach(depthList, function (d) {
        transaction.sign(that._client._hdClientMaster.derive(d).privateKey);
        log.debug('Signed with: ', that._client._hdClientMaster.derive(d).privateKey.toString())
    });
    log.debug('Index before', this._indexChange)
    this._indexChange++;
    log.debug('Index after', this._indexChange)

    tData.transaction = transaction;
    tData.depthList = depthList;

    return tData;
};


module.exports = AccountMSWallet;