exports.deviceTypes = {
    SERVER: "SERVER",
    ANDROID: "ANDROID"
};

exports.accountTypes = {
    Account: "Account",
    AccountMSW: "AccountMSW",
    AccountMSC: "AccountMSC",
    AccountAdmin: "AccountAdmin"
};

exports.accountTypesDescriptions = {
    Account: "This is a normal basic account",
    AccountMSW: "Account Multisig Wallet for your payment hub",
    AccountMSC: "Account Multisig Client to connect to your hub"
};

exports.loggerConfig = {
    appenders: [
        {type: 'console'}
    ]
};

/**
 * Actions we want to police using the policy
 */
exports.policyActions = {
    SEND: "SEND",
    AUTH: "AUTH",
    ROOT: "ROOT",
    CREDIT: "CREDIT",
    RATELIMIT: 'RATELIMIT',
    STOP: "STOP"
};

/**
 * Needed variables to register a new policy
 */
exports.policyActionsVars = {
    SEND: ["upperBound", "lowerBound"],
    AUTH: ["upperBound", "lowerBound", "threshold", "groups"],
    ROOT: [],
    CREDIT: ["upperBound", 'threshold', 'groups', 'credit'],
    RATELIMIT: ['upperBound', 'timeAmount', 'timeScale', 'timeHistory'],
    STOP: ['fromDate', 'toDate', 'enable']
};

exports.policyActionsDefaults = {
    SEND: [0, 0],
    AUTH: [0, 0, 1, []],
    ROOT: [],
    CREDIT: [0,1,[], 0],
    RATELIMIT: [0, 1, 'days', []],
    STOP:[Date.now(), Date.now(), false]
};

/**
 * Needed variables when checking a policy with isAllowed
 */
exports.policyActionCommand = {
    SEND: ['value'],
    AUTH: ['value'],
    ROOT: [],
    CREDIT: ['value'],
    RATELIMIT: ['value'],
    STOP:[]
};

/**
 * Types of messages we can process
 */
exports.messageTypes = {
    REG_NEW_CLIENT_R     : "REG_NEW_CLIENT_R",
    NOT_SUPPORTED        : "NOT_SUPPORTED",
    ROOT_ACTION_TEST     : "ROOT_ACTION_TEST",

    REG_CLIENT_ACCOUNT_R :     "REG_CLIENT_ACCOUNT_R",
    REG_CLIENT_ACCOUNT   :     "REG_CLIENT_ACCOUNT",
    REG_CLIENT_ACCOUNT_MSW   : "REG_CLIENT_ACCOUNT_MSW",


    ACCOUNT_NEW : "ACCOUNT_NEW",
    ACCOUNT_REMOVE : "ACCOUNT_REMOVE",
    ACCOUNT_REG_CLIENT: "ACCOUNT_REG_CLIENT",
    ACCOUNT_REG_CLIENT_MSW: "ACCOUNT_REG_CLIENT_MSW",
    ACCOUNT_REG_CLIENT_MSC: "ACCOUNT_REG_CLIENT_MSC",
    ACCOUNT_REG_CLIENT_MSW_GETHDPRIV: "ACCOUNT_REG_CLIENT_MSW_GETHDPRIV",
    ACCOUNT_REG_CLIENT_MSC_HDPRIV: "ACCOUNT_REG_CLIENT_MSC_HDPRIV",
    ACCOUNT_REG_CLIENT_MSC_HDPRIVGET: "ACCOUNT_REG_CLIENT_MSC_HDPRIVGET",

    MSW_GETCURRENTADDRESS:  "MSW_GETCURRENTADDRESS",
    MSW_MAKETRANSACTION:    "MSW_MAKETRANSACTION",
    MSW_GETBALANCE:         "MSW_GETBALANCE",
    MSW_SUBMITTRANSACTION:  "MSW_SUBMITTRANSACTION",
    MSW_CREDITREQUEST: "MSW_CREDITREQUEST",
    
    N_NEW: "N_NEW",
    N_UPDATE: "N_UPDATE",

    TEST_ROOT : "TEST_ROOT"
};


/**
 * Required data provided with these messages
 */
exports.messageData = {
    REG_CLIENT_ACCOUNT   : ['msgType', 'msgData.accountId'],
    REG_CLIENT_ACCOUNT_MSW   : ['msgType', 'msgData.accountId', 'msgData.xpub'],

    ACCOUNT_NEW:                       ['msgData.name', 'msgData.accountType'],
    ACCOUNT_REMOVE:                    ['msgType', 'msgData.pubkey'],
    ACCOUNT_REG_CLIENT:                ['msgType', 'msgData.senderPubkey'],
    ACCOUNT_REG_CLIENT_MSW:            ['msgType', 'msgData.senderPubkey'],
    ACCOUNT_REG_CLIENT_MSC:            ['msgType', 'msgData.senderPubkey', 'msgData.hdPubkey'],
    ACCOUNT_REG_CLIENT_MSW_GETHDXPRIV: ['msgType'],
    ACCOUNT_REG_CLIENT_MSC_HDPRIV:     ['msgType', 'msgData.hdPrivkey'],

    MSW_CREDITREQUEST: ['msgType', 'msgData.value']
};



/**
 * Types of communication partners
 */
exports.commTypes = {
    SERVER: "SERVER",
    ANDROID: "ANDROID",
    IOS: "IOS"
};

/**
 * Identifying communication data provided to connect to certain commTypes
 */
exports.commTypesData = {
    SERVER: ['commType', 'commData.url','commData.senderPubkey']
};