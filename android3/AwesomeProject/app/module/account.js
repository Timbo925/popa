var bitcore = require('bitcore-lib');
var Mnemonic = require('bitcore-mnemonic');
var log = require('log4js').getLogger("Account");
var uid = require('uid-safe');
var Messenger = require('./messenger');
var types = require('./constants');
var Client = require('./client');

function Account(identifier) {
    this._options = {};
    this.type = types.accountTypes.Account;
    log.trace("Account Construct");

    //Check if we are creating with Mnemonic and/or Private key as identifier
    if (Mnemonic.isValid(identifier)) {
        this._options = {mnemonic: new Mnemonic(identifier).toString()}
    } else if (bitcore.HDPrivateKey.isValidSerialized(identifier)) {
        this._options = {hdPrivMaster: identifier}
    } else if (identifier) {
        this._options = identifier;
    } else {
        this._options = {mnemonic: new Mnemonic().toString()}
    }

    this.init(this._options);
}

Account.prototype.init = function (options) {
    //log.debug("Creating Account with options: ", JSON.stringify(options))
    log.trace("Account Init", options);
    this.version = 1;
    this.id = options.id || uid.sync(18);
    this.name = options.name || "";
    this._mnemonic = options.mnemonic ? new Mnemonic(options.mnemonic) : new Mnemonic();
    this._hdPrivMaster = options.hdPrivMaster ? new bitcore.HDPrivateKey(options.hdPrivMaster) : this._mnemonic.toHDPrivateKey()

    //Loading Messenger
    var messengerOptions = options.messenger || {client: {privateKey: this._hdPrivMaster.derive("m/0/0'").privateKey}};
    //log.debug("Messenger Options", messengerOptions);
    this._messenger = new Messenger(messengerOptions);
    return this
};

/**
 * Override this to make sure important inherited objects are also saved correctly
 * @returns JSON critical info of the object
 */
Account.prototype.toJSON = function () {
    return {
        type: this.type,
        version: this.version,
        name: this.name,
        id: this.id,
        mnemonic: this._mnemonic.toString(),
        hdPrivMaster: this._hdPrivMaster.toString(),
        messenger: this._messenger.toJSON()
    }
};

/**
 * Return string used for backup
 * @return {String}
 */
Account.prototype.getBackupString = function () {
    return this._mnemonic.toString()
};

Account.prototype.getType = function () {
    return this.type
};

Account.prototype.toString = function () {
    return JSON.stringify(this.toJSON());
};

Account.prototype.getId = function () {
    return this.id;
};

Account.prototype.getAuthPub = function () {
    return this._messenger.getClient().getPublicKey().toString()
};

/**
 *
 * @param a1 {Account}
 * @returns {boolean}
 */
Account.prototype.isEqualTo = function (a1) {
    var a2 = this;
    return a1.type == a2.type &&
        a1.id == a2.id &&
        a1._hdPrivMaster.toString() == a2._hdPrivMaster.toString();
};

Account.prototype.getMessage = function (type) {
    switch (type) {
        // case types.messageTypes.REG_NEW_CLIENT_R:
        //     return {msgType: types.messageTypes.REG_NEW_CLIENT_R};
        case types.messageTypes.ACCOUNT_REG_CLIENT:
            return {
                msgType: types.messageTypes.ACCOUNT_REG_CLIENT,
                msgData: {accountId: this.id, accountType:types.accountTypes}};
        default:
            return {
                msgType: 'Not supported by account or unknown type'
            }
    }
};

Account.prototype.registerClient = function(pubkey, commType, commData) {
    this._messenger.registerClient(pubkey,commType,commData);
    return this;
};

/**
 *
 * @returns {Messenger}
 */
Account.prototype.getMessenger = function () {
    return this._messenger;
};

Account.prototype.signMessageJson = function (toPubKey, message) {
    return  this._messenger.encryptJson(message,toPubKey)
};

Account.prototype.sendMessageToAllEncrypted = function(msg, options, cb) {
    return this._messenger.sendMessageToAllEncrypted(msg, options, cb);
};

Account.prototype.sendMessageEncrypted = function(toPubKey, message, options, cb) {
    return this._messenger.sendMessageEncrypted(toPubKey, message, options ,cb);
};

Account.prototype.decryptMessage = function(message) {
  return this._messenger.decryptJson(message);  
};

Account.prototype.getBalance = function () {
    return 0;
};

module.exports = Account;