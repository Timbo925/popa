var bitcore = require('bitcore-lib');
var Mnemonic = require('bitcore-mnemonic');
var log = require('log4js').getLogger("AccountAdmin");
var uid = require('uid-safe');
var Account = require('./account');
var types = require('./constants');


/**
 *
 * @param identifier
 * @constructor
 * @type{Account}
 */
function AccountAdmin(identifier) {
    Account.apply(this, [identifier]);
    this.type = types.accountTypes.AccountAdmin;
}

AccountAdmin.prototype = new Account();
AccountAdmin.prototype.constructor = AccountAdmin;


/**
 * Init gets called by constructor Account
 * @param options
 */
AccountAdmin.prototype.init = function (options) {
    Account.prototype.init.call(this, options);
};

//Override Functions
AccountAdmin.prototype.toJSON =  function () {
    return Account.prototype.toJSON.call(this);
};


module.exports = AccountAdmin;