var types = require('./constants');
var log = require('log4js').getLogger("AccountManager");
var Account = require('./account');
var jf = require('jsonfile');
var fs = require('fs');
var AccountMSW = require('./accountMSWallet');
var AccountMSC = require('./accountMSClient');
var AccountAdmin = require('./accountAdmin');
var constants = require('./constants');
var comm = require('./communication');
var _ = require('lodash');

function AccountManager(options) {
    this.id = "";
    this.accountList = [];
    this.init(options)
}

AccountManager.prototype.init = function (options) {
    if (!options) return this;
    this.id = options.id || this.id;
    this.accountList = [];
    if (options.accountList) {
        for (var i = 0; i < options.accountList.length; i++) {
            var con = this.getAccountConstructor(options.accountList[i].type);
            var account = new con(options.accountList[i]);
            //log.debug("Con:", con)
            this.accountList.push(account);
        }
    }
    // if (!_.find(this.accountList, function (account) {
    //         return account.type == constants.accountTypes.AccountAdmin
    //     })) {
    //     //No AccountAdmin in the accountManager, create one
    //     this.createAccount(constants.accountTypes.AccountAdmin, {name: "Account Admin"})
    // }
    return this;
};

/**
 * @returns {AccountAdmin}
 */
AccountManager.prototype.getAccountAdmin = function () {
    return _.find(this.accountList, function (o) {
        return o.type == types.accountTypes.AccountAdmin;
    })
};

AccountManager.prototype.toString = function () {
    return JSON.stringify(this.toJSON())
};

AccountManager.prototype.toJSON = function () {
    return {
        id: this.id,
        accountList: this.accountList
    }
};

AccountManager.prototype.saveFs = function () {
    try {
        jf.writeFileSync(this.id + "_AccountManager.json", this.getAccountListFlat());
        for (var i = 0; i < this.accountList.length; i++) {
            log.trace("Saving Account: ", accountList[i]);
            this.accountList[i].saveAccount()
        }
    } catch (e) {
        log.error("Saving AccountManger", e);
        return false;
    }
    return true;
};

AccountManager.prototype.removeFs = function () {
    for (var i = 0; i < accountList.length; i++) {
        this.accountList[i].removeAccount()
    }
    fs.unlinkSync(this.id + "_AccountManager.json");
};


//TODO Load id
AccountManager.prototype.loadFs = function () {
    try {
        accountList = [];
        var idAccountList = jf.readFileSync(this.id + "_AccountManager.json");
        //log.debug("Loaded Account List: ", JSON.stringify(idAccountList));
        for (var i = 0; i < idAccountList.length; i++) {
            var con = getAccountConstructor(idAccountList[i].type);
            var account = new con(idAccountList[i].id);
            //log.debug("Con:", con)
            accountList.push(account);
        }
        this.accountList = accountList;
    } catch (e) {
        log.debug("No accountManager found: " + this.id + "_AccountManager.json")
        return false;
    }
    return true;
};

AccountManager.prototype.getAccountConstructor = function (type) {
    var con;
    switch (type) {
        case types.accountTypes.Account:
            con = Account;
            break;
        case types.accountTypes.AccountMSC:
            con = AccountMSC;
            break;
        case types.accountTypes.AccountMSW:
            con = AccountMSW;
            break;
        case types.accountTypes.AccountAdmin:
            con = AccountAdmin;
            break;
        default:
            log.error("No type matched:", type, types.accountTypes.AccountMSC);
            break;
    }
    return con;
};

/**
 *
 * @return {[{id: string, type: string, pubkey: string, name: string}]}
 */
AccountManager.prototype.getAccountListFlat = function () {
    var idList = [];
    for (var i = 0; i < this.accountList.length; i++) {
        idList.push({
            id: this.accountList[i].getId(),
            type: this.accountList[i].getType(),
            pubkey: this.accountList[i].getAuthPub(),
            name: this.accountList[i].name
        })
    }
    return idList
};

AccountManager.prototype.getAccountHash = function () {
    var accountHash = {};
    for (var i = 0; i < this.accountList.length; i++) {
        accountHash[this.accountList[i].getAuthPub()] = {
            id: this.accountList[i].getId(),
            type: this.accountList[i].getType(),
            pubkey: this.accountList[i].getAuthPub(),
            name: this.accountList[i].name
        }
    }
    return accountHash;
};

/**
 *
 * @param message {Buffer}
 * @return {*}
 */
AccountManager.prototype.decryptMessage = function (message) {
    var pubkey = comm.getPubKeyMessage(message);
    var that = this;

    log.trace("Message", message);
    log.trace("Pubkey From:", pubkey);
    log.trace("AccountList", this.getAccountListFlat());

    var decrypted = null;
    _.forEach(this.accountList, function (account) {
        try {
            decrypted = account.getMessenger().decryptJson(message);
            return false;
        } catch (e) {
            log.debug("Message not for me", e)
        }
    });
    return decrypted;
};

AccountManager.prototype.getAccountList = function () {
    return this.accountList;
};

/**
 *
 * @param id
 * @return {Account}
 */
AccountManager.prototype.getAccountById = function (id) {
    var account;
    for (var i = 0; i < this.accountList.length; i++) {
        if (this.accountList[i].getId() == id) account = this.accountList[i];
    }
    return account;
};

AccountManager.prototype.getAccountByPubKey = function (pubkey) {
    var account;
    for (var i = 0; i < this.accountList.length; i++) {
        if (this.accountList[i].getAuthPub() == pubkey.toString()) {
            account = this.accountList[i];
        }
    }
    log.debug("Found Account", account)
    return account;
};

AccountManager.prototype.removeAll = function () {
    this.accountList = [];
    return this;
};

/**
 * Create account with type and options
 * @param type
 * @param options
 * @return {Account}
 */
AccountManager.prototype.createAccount = function (type, options) {
    options = options || {};
    var con = this.getAccountConstructor(type);
    var account = new con(options);
    this.accountList.push(account);
    return account;
};

/**
 * Delete account based on the pubkey
 * @param pubkey
 */
AccountManager.prototype.deleteAccountByPubkey = function(pubkey) {
    var ac = this.getAccountByPubKey(pubkey);
    if (!ac) throw new Error('Account not fund with pubkey' + pubkey);
    _.remove(this.accountList, function(a) {return ac.id == a.id});
    return true;
};

AccountManager.prototype.getInstance = function (id) {
    return new AccountManager(id);
};

module.exports = AccountManager;