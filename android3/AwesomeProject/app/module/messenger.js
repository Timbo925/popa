var bitcore = require('bitcore-lib');
var ECIES = require('bitcore-ecies');
var Mnemonic = require('bitcore-mnemonic');
var log = require('log4js').getLogger("Messenger");
var uid = require('uid-safe');
var enums = require('./constants');
var comm = require('./communication');
var Client = require('./client');
var _ = require('lodash');
var async = require('async');
var NotificationCenter = require('./NotificationCenter');

/**
 *
 * @param options
 * @constructor
 */
function Messenger(options) {
    log.trace("Creating Messenger with: ", options);
    this.client = new Client(options.client);
    this._clientList = options.clientList ? toClientList(options.clientList) : [];
    this._cipher = ECIES().privateKey(this.client.getPrivateKey())
    this._notificationCenter = new NotificationCenter(options.notificationCenter);
}

Messenger.prototype.toJSON = function () {
    return {
        client: this.client.toJSON(),
        clientList: clientListToJson(this._clientList),
        notificationCenter: this._notificationCenter.toJSON()
    }
};

Messenger.prototype.toString = function () {
    return JSON.stringify(this.toJSON())
};

Messenger.prototype.getClientList = function () {
    return this._clientList;
};

//  #######################
//  ##### Client Code #####
//  #######################
//Todo check message that it includes the challange
/**
 *
 * @param pubkey {bitcore.PublicKey|String}
 * @param type: {String}
 * @param typeInfo
 * @returns {Boolean} Returns false if no action was taken
 */
Messenger.prototype.registerClient = function (pubkey, type, typeInfo) {
    if (bitcore.PublicKey.isValid(pubkey)) {
        var client = new Client({publicKey: pubkey, type: type, typeInfo: typeInfo});
        if (_.find(this._clientList, {pubkey: pubkey})) return false;
        this._clientList.push(client)
    } else {
        throw "Not a valid communication pubkey"
    }
    return true;
};

/**
 *
 * @returns {Client}
 */
Messenger.prototype.getClient = function () {
    return this.client;
};

/**
 * @param pubkey {String|bitcore.PublicKey}
 * @returns {Client}
 */
Messenger.prototype.getClientByPubKey = function (pubkey) {
    var rClient = null;
    log.trace("ClientList", this._clientList)
    for (var i = 0; i < this._clientList.length; i++) {
        log.trace(this._clientList[i].getPublicKey().toString(), pubkey);
        if (this._clientList[i].getPublicKey() == pubkey) rClient = this._clientList[i];
    }
    return rClient;
};

Messenger.prototype.getClientByMessage = function (message) {
    return this.getClientByPubKey(this.getPubKeyMessage(message))
};

//  ##########################
//  ##### Messaging Code #####
//  ##########################

Messenger.prototype.encrypt = function (message, pubkey) {
    pubkey = new bitcore.PublicKey(pubkey);
    var cipher = ECIES().privateKey(this.client.getPrivateKey()).publicKey(pubkey);
    return cipher.encrypt(message);
};

Messenger.prototype.decrypt = function (encrypted) {
    var enc = encryptedToBuffer(encrypted);
    var cipher = this._cipher;
    log.debug("Decrypting: ", JSON.stringify(encrypted));
    log.trace("isBuffer", Buffer.isBuffer(encrypted));
    try {
        var ret = cipher.decrypt(enc);
    } catch (e) {
        log.error(e)
    }
    return ret;
};

Messenger.prototype.encryptJson = function (json, pubkey) {
    return this.encrypt(JSON.stringify(json), pubkey);
};

Messenger.prototype.decryptJson = function (encrypted) {
    return JSON.parse(this.decrypt(encrypted));
};

/**
 * Send message encrypted to all other connected clients
 * @param message {Object} message to be encrypted
 * @param options {Object} options for params/query
 * @param cb {function} callback
 */
Messenger.prototype.sendMessageToAllEncrypted = function (message, options, cb) {
    var that = this;
    var msg = [];
    log.debug('sendMessageToAllEncrypted --', 'Start');
    async.each(this._clientList, function (client, callback) {
        that.sendMessageEncrypted(client.pubkey, message, options, function (err, retMsg) {
            if (!err) msg.push(retMsg.msg);
            callback(err);
        })
    }, function (err) {
        log.debug('sendMessageToAllEncrypted --', 'End');
        if (msg.length == 1) msg = msg[0];
        if (err) return cb(err.msg);
        return cb(null, msg);
    });
};


Messenger.prototype.sendMessageEncryptedToClients = function (clients, message, options, cb) {
    var that = this;
    var msg = [];
    async.eachSeries(clients, function (client, callback) {
        that.sendMessageEncrypted(client, message, options, function (err, retMsg) {
            if (!err) msg.push(retMsg.msg);
            callback(err);
        });
    }, function (err) {
        if (msg.length == 1) msg = msg[0];
        if (err) return cb(err.msg);
        return cb(null, msg);
    })
};

Messenger.prototype.sendMessageEncrypted = function (pubkey, message, options, cb) {
    var client = this.getClientByPubKey(pubkey);
    //if (!client) throw new Error("Client not found with pubkey: ", client);
    if (!client) cb("Client not found with pubkey: " + client);
    var encrypted;
    if (typeof message == 'string') {
        log.debug("Encrypting string");
        encrypted = this.encrypt(message, client.pubkey)
    } else {
        log.debug("Encrypting JSON", JSON.stringify(message));
        encrypted = this.encryptJson(message, client.pubkey)
    }
    return comm.sendMessage(client, encrypted, options, cb);
};

Messenger.prototype.getPubKeyMessage = function (message) {
    var msg = encryptedToBuffer(message);
    return bitcore.PublicKey.fromDER(msg.slice(0, 33));
};

Messenger.prototype.receiveMessageEncrypted = function (message) {
    try {
        var decrypted = this.decrypt(message); //Throws error when message is not for this client
        var fromClient = this.getClientByMessage(message);
        if (isJson(decrypted.toString())) {
            return {encrypted: true, message: JSON.parse(decrypted.toString()), client: fromClient}
        } else {
            return {encrypted: true, message: decrypted.toString(), client: fromClient}
        }
    } catch (err) {
        log.error(err);
        return {encrypted: false, message: message, client: null}
    }
};

//  ###################################
//  ##### NotificationCenter Code #####
//  ###################################


/**
 *
 * @param type {String}
 * @param groups {[String|PublicKey]}
 * @param data {Object}
 * @param cb {function}
 */
Messenger.prototype.newNotification = function (type, groups, data, cb) {
    var that = this;
    var filteredGroups = _.filter(groups, function (g) {
        return that.getClientByPubKey(g);
    });
    log.debug('Groups to Notify', groups, filteredGroups);
    var notification = this._notificationCenter.newNotification(type, data);
    var msg = {
        msgType: enums.messageTypes.N_NEW,
        msgData: notification
    };
    this.sendMessageEncryptedToClients(filteredGroups, msg, {}, cb);
};

/**
 *
 * @param notification {Object}
 * @return {Messenger}
 */
Messenger.prototype.receiveNotification = function (notification) {
    this._notificationCenter.receiveNotification(notification);
    return this;
};

Messenger.prototype.getNotifications = function() {
    return this._notificationCenter.getReceiveNotification();
};

Messenger.prototype.getNotificationsSend = function () {
    return this._notificationCenter.getSendNotification();
};

Messenger.prototype.updateNotification = function (notification) {
    return this._notificationCenter.updateNotification(notification)
};

//  ############################
//  ##### Helper Functions #####
//  ############################

function encryptedToBuffer(encrypted) {
    if (!Buffer.isBuffer(encrypted)) {
        if (encrypted.type == 'Buffer') {
            return new Buffer(encrypted.data)
        } else {
            log.error('Message to encrypt not Buffer/JSON');
            throw 'Encrypted not a Buffer or not in JSON Buffer Format'
        }
    } else {
        return encrypted;
    }
}

function isJson(str) {
    try {
        JSON.parse(str)
    } catch (err) {
        return false;
    }
    return true;
}

function toClientList(json) {
    var ret = []
    for (var i = 0; i < json.length; i++) {
        ret.push(new Client(json[i]))
    }
    return ret;
}

function clientListToJson(list) {
    var ret = [];
    for (var i = 0; i < list.length; i++) {
        ret.push(list[i].toJSON())
    }
    return ret;
}

module.exports = Messenger;