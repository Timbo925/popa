/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
require('./shim');
import React, {Component} from 'react';
import {AppRegistry, StyleSheet, Text, View} from 'react-native';
import ViewContainer from './app/components/ViewContainer';
import WalletView from './app/components/WalletView';
//global.Buffer = require('buffer').Buffer;
//glabal.crypto

class AwesomeProject extends Component {

    render() {
        var x = new Buffer([1,2]);
        console.log(x);

        return (
            <WalletView></WalletView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

AppRegistry.registerComponent('AwesomeProject', () => AwesomeProject);
