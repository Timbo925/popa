# PoPa: Policy Based Payment Hubs for Digital Payments and Beyond


In this project we propose a system called PoPa, which will allow for easy,
and extensive customization and multi device management. PoPa is built in
an open way, to be used with Bitcoin, but easily extensible to other platforms.
Device management is made easy for the user, and each device can be
configured separately with their own policies and limitations such as, daily
limits and maximum transaction amounts. Multi-devcie features are also
supported on the system allowing, for instance, a multi-device authorization
scheme for high valued payments. PoPa also offers easy ways for both the
user, and the developer, to extend and tune the policy system to their specific
needs. Even doing this on a group or device specific level, allowing a
multitude of interesting setups and configurations. Finally, PoPa offers a
multitude of features, trying to improve over existing solutions in the mobile
payment space, while keeping the system open and secure, for easy additional
development.